import { Component, OnInit, ElementRef, ChangeDetectorRef,ViewChild, AfterViewInit } from '@angular/core';
import { HttpClient, HttpEventType } from '@angular/common/http';
import { UploadFileService, dynamodbConfig, awsCognitoUtil, CSVRecord, validate, EncrDecrService } from '../_models';
import { HeaderComponent } from '../header/header.component';
import { UploadResult } from "../upload.service";
import { UploadService } from "../upload.service";
import { CookieService } from 'ngx-cookie-service';
import * as elasticsearch from 'elasticsearch-browser';
import * as aws from 'aws-sdk';



@Component({
  selector: 'app-uploadfile',
  templateUrl: './uploadfile.component.html',
  styleUrls: ['./uploadfile.component.scss']
})
export class UploadfileComponent implements OnInit, AfterViewInit {

  pageTitle: string;
  msgStr: string;
  @ViewChild('alert') alert: ElementRef;
  @ViewChild(HeaderComponent) objHeaderComp: HeaderComponent;

  _UserType: any;
  _AccountMapping: accountMapping[] = [];
  headAssignmentElements = ["Account Number", "Agency Code"];
  sessionKey :string = sessionStorage.getItem("sessionkey");



  public uploads: UploadResult[];

  private uploadService: UploadService;



  constructor(private http: HttpClient, private _awsCognitoUtil: awsCognitoUtil,
    private EncrDecr: EncrDecrService,private cdr: ChangeDetectorRef) {


  }

  ngOnInit() {
    this.objHeaderComp.flg = false;
  }

  ngAfterViewInit() {

    var type = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
    var userid = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
    var agencycode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agentcode"));

    this.pageTitle = "Check Account Mapping";

    if (type == "Agent" || type == "Agency") {

      /*  this._AgencyAssignmentFilter.nativeElement.style.display = 'none';
       this.txtAgencyDiv.nativeElement.style.display = 'none'; */

      // this.pageTitle = "Check Agency Assignment";

    }
    else if (type == "BU") {
      // this.IsDeletionRequired = true;
      // this.readItem()
    }

    this.cdr.detectChanges();
  }



  closeAlert() {

    //this.alert.nativeElement.classList.remove('show');
    this.alert.nativeElement.classList.remove('show');
    this.alert.nativeElement.style.display = 'none';

  }

  showAlert(){
    this.alert.nativeElement.classList.add('show');
    this.alert.nativeElement.style.display = 'block';

  }

  FindAccountDetailsInAmazonES(accountNumber: string = "") {
    var _this = this;

    this.closeAlert();
    if (accountNumber.trim().length == 8) {

      this.searchAccountNumberInAWSES(accountNumber).then(response => {

        try {

          this._AccountMapping = [];

          console.log(response);
          if (response["hits"].total > 0) {

            console.log(response["hits"].total);
           
            

            this._UserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
            
            var DecryptAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));

            if (_this._UserType == "Agency" || _this._UserType == "Agent") {

              if (response.hits.hits[0]._source["agency"] == DecryptAgencyCode) {

                for (var i = 0; i < response.hits.hits.length; i++) {
                  var accMapping = new accountMapping();
                  accMapping.Account = response.hits.hits[i]._source.account;
                  accMapping.Agency = response.hits.hits[i]._source["agency"];
                  this._AccountMapping.push(accMapping);

                }

               
                

              } else {

                //_this.alert.nativeElement.classList.add('show');
                this.showAlert();
                this.msgStr = "Account Number " + accountNumber + " is not mapped with agency -" + DecryptAgencyCode;
              }


            } else {



              for (var i = 0; i < response.hits.hits.length; i++) {

                var accMapping = new accountMapping();
                accMapping.Account = response.hits.hits[i]._source.account;
                accMapping.Agency = response.hits.hits[i]._source["agency"];
                this._AccountMapping.push(accMapping);

              }
            }
          }
          else
          {
            _this.alert.nativeElement.classList.add('show');
            
            this.showAlert();
            this.msgStr = "Account Number " + accountNumber + " is not mapped with agency ";
          }


        } catch (err) {
          this.showAlert();
          alert("Technical error :- " + err);
        }
      });
    } else {
      this.showAlert();
      //_this.alert.nativeElement.classList.add('show');

      this.msgStr = "Invalid account number, Please enter 8 digit account number";

    }


  }


  searchAccountNumberInAWSES(account_no: string) {

   this.closeAlert();
    var index = 'access-logs-dca';
    var type = 'doc';

    var domain = 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com'
    var region = 'ap-southeast-1'



    var elasticSearchClient = new elasticsearch.Client({
      host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com',
      /*   log:'trace', */
      /*  connectionClass: HttpAmazonESConnector, */
      amazonES: {

        region: this._awsCognitoUtil._REGION
      },
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    var result;

    return elasticSearchClient.search({
      index: 'access-logs-dca',
      type: 'doc',
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        query: {
          match: { "account": account_no }
        },
      },
      '_source': ['account', 'agency']
    });





  }



}

class accountMapping {

  Agency: string
  Account: string

}
