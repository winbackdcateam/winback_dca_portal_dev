import { Injectable, OnInit } from '@angular/core';
import { FLAGS } from '@angular/core/src/render3/interfaces/view';
import { DeviceDetectorService } from 'ngx-device-detector';

@Injectable({
  providedIn: 'root'
})
export class ApplicationStateServiceService implements OnInit {

  private isMobileResolution: boolean;
  public loadngflg: boolean = false
  _deviceInfo = null;

  constructor(private deviceService: DeviceDetectorService) {

  }


  ngOnInit() { }

  public getIsMobileResolution(): boolean {
    //console.log('device is being recognized.. ')
    return this.epicFunction();
  }


  public startSplashScreen(): boolean {
    return this.loadngflg;
  }


  epicFunction(): boolean {

    let flg = true;
  
    this._deviceInfo = this.deviceService.getDeviceInfo();
    const isMobile = this.deviceService.isMobile();
    const isTablet = this.deviceService.isTablet();
    const isDesktopDevice = this.deviceService.isDesktop();

    if (!isDesktopDevice) {
      
      if (isMobile) {
      // console.log('device is mobile.. ')
        flg = false;
      }
      else if (isTablet) {
      //  console.log('device is tablet.. ')
        flg = false;
      }

    }
    else {

      if (window.innerWidth < 768) {
       // console.log('device is not desktop.. ')
        flg = false;
      } else {
       // console.log('device is desktop.. ' + window.innerWidth)
        flg = true;
      }


    }
   // console.log('Application access is .. ' + flg)
    return flg;
  }


}
