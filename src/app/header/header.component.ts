

import {
  Component,
  OnInit,
  ViewChild,
  Input,
  EventEmitter,
  Output,
  AfterViewInit,
  AfterViewChecked,
  Renderer2,
  ElementRef,
  HostListener
} from "@angular/core";
import { formatDate, DatePipe } from "@angular/common";
import { CustomerInformation } from "../_models/customer";
import { HttpClient } from "@angular/common/http";
import { CognitoIdentityServiceProvider } from "aws-sdk/clients/all";
import { DataService } from "../data.service";
import { AccountInformation } from "../_models/account";
import { PaymentInformation } from "../_models/payment";
import { SMCInformation } from "../_models/smc";
import { WinBack } from "../_models/winback";
import { AmplifyService } from "aws-amplify-angular";
import { Router } from "@angular/router";
import { Auth } from "aws-amplify";
import { Observable } from "rxjs";
import { CookieService } from 'ngx-cookie-service';
import AWS, { DynamoDB } from 'aws-sdk';
import * as aws from 'aws-sdk';
import {
  AccountMapping, dynamodbConfig,
  Common,
  AccountDESMapping,
  SessionUserIdle,
  validate,
  awsCognitoUtil,
  CSVRecord,
  EncrDecrService
} from "../_models";
import { forEach } from "@angular/router/src/utils/collection";
import { Papa } from "ngx-papaparse";
import { AppComponent } from "../app.component";
import { AuthComponent } from "../auth/auth.component"
import { Alert } from "selenium-webdriver";
import { ApplicationStateServiceService } from '../application-state-service.service';
import * as elasticsearch from 'elasticsearch-browser'
import { logawses } from '../_models/logawses';
import { isNumber } from 'util';
import * as AWSGlobal from "aws-sdk/global";




@Component({
  selector: "app-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.scss"]
})
export class HeaderComponent
  implements OnInit, AfterViewInit, AfterViewChecked {

  @ViewChild(AuthComponent) objAuhtComp: AuthComponent;
  @Input() state: boolean;
  @Input() errorstate: boolean;
  @Output() errored = new EventEmitter();
  @Output() toggle = new EventEmitter();
   sessionKey :string = sessionStorage.getItem("sessionkey");
  isErrorFound: boolean = false;
  today = new Date();
  jstoday = "";
  awsPersonalCreds: any;
  flg: boolean = true;
  IsUploadLinkVisible: boolean = false;
  IsUserManagementLinkVisible: boolean = false;
  IsAuditLogsVisible: boolean = false;
  _account: string;
  _accountholdername: string;
  _accountStatus: string;
  hidemenutab: boolean = true;
  _cmobject = new Common();
  _AccountDESMapping: AccountDESMapping[];
  _username: string = "";
  _userFullName: string = "";
  _userType: string;
  _agencyName: string;
  user: any;
  _ValidateStatus: validate;
  _ReadAssignmentData: CSVRecord[] = [];
  _RecordFound: CSVRecord[] = [];
  _msg: string;
  _mappingTitle: string = "Check Mapping";
  disabledState: boolean = false;
  pk: string;
  app :string;

  constructor(
    private _data: DataService,
    private amplifyService: AmplifyService,
    private router: Router,
    private http: HttpClient,
    private datePipe: DatePipe,
    private papa: Papa,
    private cookieService: CookieService,
    private _validator: validate,
    private _AwsCongnitoUtil: awsCognitoUtil,
    private _state: ApplicationStateServiceService,
    private EncrDecr: EncrDecrService,
    private logawsesobj: logawses,
    private renderer: Renderer2,
    private el: ElementRef
  ) {
    this.jstoday = formatDate(
      this.today,
      "dd-MM-yyyy hh:mm:ss a",
      "en-US",
      "+0530"
    );

    this.checkLoginState();
  }

  @HostListener('click', ['$event']) onclick(event: Event) {

    
    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));
    this.IsUserActive(decryptedUserId);
    
  }


  ngOnInit() {

    if (localStorage.length == 0) {      
      this.router.navigateByUrl('/auth');
      console.log("logging out");
     }
     
     if (sessionStorage.length == 0) {      
      this.router.navigateByUrl('/auth');
      console.log("logging out");
     }

     if(sessionStorage.getItem("token")=="")
     {
      this.router.navigateByUrl('/auth');
      console.log("logging out");

     }
     
    
    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

    var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));

    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

    var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
    
    

    this._username =decryptedUserId ;
    this._userFullName =decryptedFullName ;

    if(this._userFullName==""|| this._userFullName=="none")
    {
      alert("Try again due to unknown error occured");
      this.router.navigateByUrl("/auth?msg=cookieexpired");
      Auth.signOut({ global: true })
        .then(data => { })
        .catch(err => {
          this.isErrorFound = true;
          this.state = false;
          this.toggle.emit(this.state);
          console.log(err.message)
        });

    }

    var type = decryptedUserType;
    this._userType = decryptedUserType;
    this._agencyName = decryptedAgencyName;

    if (type == "BU" || type == "SA" ) {
      this.IsUploadLinkVisible = true;
      this.IsUserManagementLinkVisible = true;
      this.IsAuditLogsVisible = true;
    }
    else if (type == "Agency") {
      this.IsUserManagementLinkVisible = true;
      this.IsUploadLinkVisible = true;
      this._mappingTitle = "Check Assignment";
    } else {
      
      this.IsUploadLinkVisible = false;
      this.IsUserManagementLinkVisible = false;
      this.IsUploadLinkVisible = false;
    }
  }

  ngAfterViewInit() {
    this.checkLoginState();
  }

  public ngAfterViewChecked(): void {
    /* need _canScrollDown because it triggers even if you enter text in the textarea */
  }

  ngOnDestroy() {
  }

  customersData$: CustomerInformation[];
  accountData$: AccountInformation[];
  paymentData$: PaymentInformation[];
  smcData$: SMCInformation[];
  _winbackobj = new WinBack();

  //------------------------------------------------
  //-- Load Winback data here
  //------------------------------------------------

  @Output() Clicked = new EventEmitter<any>();
  @Output() getSearchStatusChange = new EventEmitter<boolean>();

  LoadWinbackData(XID: string) {
    var numbers = new RegExp(/^[0-9]+$/);
    this._account = XID;

    if (XID.trim() == "") {
      alert("Please input account number");

    } else if (!numbers.test(XID.trim())) {
      alert("Please input valid account number");
      
    } else {

      var _this = this;
      console.log("checking account");

 
   this.pk  = this.EncrDecr.get(this.sessionKey + '$#@$^@1DPK', localStorage.getItem('dcaPartnerKey'));
    this. app = this.EncrDecr.get(this.sessionKey + '$#@$^@1DAPP', localStorage.getItem('dcaapipartnerpassword'));
     
   

      var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

      var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));
  
      var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
  
      var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
      
      var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
    
      if(decryptedUserType =='BU')
      {
        decryptedAgencyCode = 'Admin'
      }

      console.log("Audit log to be saved");
      _this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", "Account Search", decryptedFullName, decryptedUserType, decryptedUserId + " searched account no -" + this._account, new Date().toISOString(),decryptedAgencyName ,decryptedAgencyCode);
      console.log("Audit log saved");


      this._username = decryptedUserId;

      if (this._username == "") {
        
        this.router.navigateByUrl("/auth?msg=cookieexpired");
        Auth.signOut({ global: true })
          .then(data => { })
          .catch(err => {
            _this.isErrorFound = true;
            _this.state = false;
            _this.toggle.emit(_this.state);
            console.log(err.message)
          });

        return false;
      }

     
      Auth.currentSession()
            .then(data => {
              console.log("checking current session is valid")
              
             
              AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId: this._AwsCongnitoUtil._IDENTITY_POOL_ID,
                Logins: {
                  'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': data.getIdToken().getJwtToken()
                }
              });
          
              AWS.config.update({
                region: 'ap-southeast-1',
                credentials: AWS.config.credentials
              }); 
          
              AWSGlobal.config.update({region: 'ap-southeast-1',
              credentials: AWS.config.credentials});

             //-------------------
        
     
           var _this = this;
           var awsparams = {
               UserPoolId: this._AwsCongnitoUtil.USER_POOL_ID,
               Username: this._username
           };

      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();

      cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
        if (err) {
            console.log('failed reload -'+ err.message );
            alert('technical error failed - ' + err.message)
            }
        else {
                  
            let IsTimeExpired = data.UserAttributes.find((a) => a.Name === 'custom:logintime').Value
            let IsUserEnabled = data.Enabled;

           
        if (IsUserEnabled == true) {
          

        

          if (IsTimeExpired != "00:00-00:00") {

            if (!_this._validator.validatelogintimeDate(IsTimeExpired)) {
              _this.router.navigateByUrl("/auth?msg=loginexpired");
              alert("Your Login Time has been expired for the day");
              Auth.signOut({ global: true })
                .then(data => console.log(data))
                .catch(err => console.log(err));
              return false;
            } else {

              //---------------------------------------------------------------------------
  
           
  
              _this.state = true;
              _this.toggle.emit(_this.state);
  
  
              let status = false;

            
              
  
              if (_this._userType == "Agency" || _this._userType == "Agent") {

                _this.state = true;
                _this.toggle.emit(_this.state);

                console.log("Agency is searching " + XID + " details");
                
                const resultES = _this.searchAccountNumberInAWSES(_this._account);
                
                resultES.then(response => {   
                  
                try 
                  {
                    
                    if (response["hits"].total > 0) {              
                   
                      var decryptedAgencyCode = _this.EncrDecr.get(_this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));

                      if (response.hits.hits[0]._source["agency"] == decryptedAgencyCode) {
                        
                        _this.disabledState = true;

                        _this._data.requestCustomerData(XID,_this.pk,_this.app).subscribe((data: any[]) => {
  
                          try {

                             
                            console.log("checking account inside api call " + XID);
                 
                            _this._state.loadngflg = true;
                            _this._winbackobj.customerInformation =
                              data[0]["body"]["CustomerInformation"];
                            _this._winbackobj.accountInformation =
                              data[1]["body"]["AccountInformation"];
                            _this._winbackobj.addressInformation =
                              data[2]["body"]["AddressInformation"];
                            _this._winbackobj.collectionBucket =
                              data[3]["body"]["CollectionBucket"];
                            _this._winbackobj.financialInformation =
                              data[4]["body"]["FinancialInformation"];
                            _this._winbackobj.paymentInformation =
                              data[5]["body"]["PaymentInformation"];
                            _this._winbackobj.smcInformation =
                              data[6]["body"]["SMCInformation"];
                            _this.Clicked.emit(_this._winbackobj);
  
                            _this.state = false;
                            _this.toggle.emit(_this.state);
                            _this.disabledState = false;

                          } catch (errmsg) {
                            _this.state = false;
                            _this.toggle.emit(_this.state);
                            alert("Error :- ODS API is not working, Please contact to application owner")
                            console.log(errmsg.message);
                            _this.errorstate = true;
                            _this.errored.emit(_this.errorstate);
                            _this.disabledState = false;
                          }
  
  
                        });
  
  


                      } else {
                        _this.state = false;
                        _this.toggle.emit(_this.state);
                        _this.errorstate = true;
                        _this.errored.emit(_this.errorstate);
                        alert("You are not authorized to search account number :" + XID);
                        _this.disabledState = false;
                      }

                   
                  }else {
                    
                     _this.state = false;
                      _this.toggle.emit(_this.state);
                      _this.errorstate = true;
                      _this.errored.emit(_this.errorstate);
                
                      if (XID.length == 8) {

                        alert("Account number :" + XID + " is not mapped.");

                      } else {
                        
                        alert("Account number :" + XID + " is not mapped. \n Please enter correct 8 digit account number");

                      }
                      _this.disabledState = false;
                  }
                
            
                } catch (e) {
                    
                  _this.state = false;
                    _this.toggle.emit(_this.state);
                    _this.errorstate = true;
                    _this.errored.emit(_this.errorstate);
              
                  alert("Technical error :" + e.message);
                  _this.disabledState = false;
                }

                    
              });

                
  
              } else {
  
                _this.state = true;
                _this.toggle.emit(_this.state);
                _this.disabledState = true;
                _this._data.requestCustomerData(XID,_this.pk,_this.app).subscribe((data: any[]) => {
  
                  try {

                    console.log("checking account inside api call " + XID);
                 
                    _this._state.loadngflg = true;
                    _this._winbackobj.customerInformation =
                      data[0]["body"]["CustomerInformation"];
                    _this._winbackobj.accountInformation =
                      data[1]["body"]["AccountInformation"];
                    _this._winbackobj.addressInformation =
                      data[2]["body"]["AddressInformation"];
                    _this._winbackobj.collectionBucket =
                      data[3]["body"]["CollectionBucket"];
                    _this._winbackobj.financialInformation =
                      data[4]["body"]["FinancialInformation"];
                    _this._winbackobj.paymentInformation =
                      data[5]["body"]["PaymentInformation"];
                    _this._winbackobj.smcInformation =
                      data[6]["body"]["SMCInformation"];
                    _this.Clicked.emit(_this._winbackobj);
  
                    _this.state = false;
                    _this.toggle.emit(_this.state);
                    
                    _this.disabledState = false;

                  } catch (errmsg)
                  {
                    _this.errorstate = true;
                    _this.errored.emit(_this.errorstate);
                    _this.state = false;
                    _this.toggle.emit(_this.state);
                    alert("Error :- ODS API is not working, Please contact to application owner")
                    console.log(errmsg.message);
                    _this.disabledState = false;
                   }
  
  
                });
  
  
              }
  
          
  
  
              //---------------------------------------------------------------------------
  
            }
          } else{


            try
            {

            _this.state = true;
            _this.toggle.emit(_this.state);
            _this.disabledState = true;
            _this._data.requestCustomerData(XID,_this.pk,_this.app).subscribe((data: any[]) => {

              try
              {

              console.log(data);
              console.log("checking account inside api call " + XID);
         

              _this._state.loadngflg = true;

              _this._winbackobj.customerInformation =
                data[0]["body"]["CustomerInformation"];
              _this._winbackobj.accountInformation =
                data[1]["body"]["AccountInformation"];
              _this._winbackobj.addressInformation =
                data[2]["body"]["AddressInformation"];
              _this._winbackobj.collectionBucket =
                data[3]["body"]["CollectionBucket"];
              _this._winbackobj.financialInformation =
                data[4]["body"]["FinancialInformation"];
              _this._winbackobj.paymentInformation =
                data[5]["body"]["PaymentInformation"];
              _this._winbackobj.smcInformation =
                data[6]["body"]["SMCInformation"];
              _this.Clicked.emit(_this._winbackobj);

              _this.state = false;
                _this.toggle.emit(_this.state);
                _this.disabledState = false;

              } catch (err) {
                console.log(err.message);
                _this.state = false;
                _this.toggle.emit(_this.state);
                _this.errorstate = true;
                _this.errored.emit(_this.errorstate);
                _this.disabledState = false;
            }
            });


          

      
            } catch (er) {
              
              _this.state = false;
              _this.toggle.emit(_this.state);
              alert("Error :- ODS API is not working, Please contact to application owner")
              console.log(er.message);
              _this.errorstate = true;
              _this.errored.emit(_this.errorstate);
              _this.disabledState = false;
          }

          //---------------------------------------------------------------------------

          }

         } else {

          Auth.signOut({ global: true })
            .then(data => console.log(data))
            .catch(err => console.log(err));

            localStorage.clear();
          sessionStorage.clear();
          
          _this.router.navigateByUrl("/auth?msg=userdisabled");

          return false;
        }
      }
    });

});
 

    /*   }, function (rejectOutput) {
        Auth.signOut({ global: true })
          .then(data => console.log(data))
          .catch(err => console.log(err));

        console.log(JSON.stringify(rejectOutput));
          localStorage.clear();
          sessionStorage.clear();
        _this.router.navigateByUrl("/auth?msg=usernotfound");
        return false;
      }); */




    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  _signoutobj = new SessionUserIdle(this.router);

  SignOut() {



   
    Auth.signOut()
      .then(data => {
        //console.log(data)
      })
      .catch(err => {
        this.state = false;
        this.toggle.emit(this.state);
        console.log(err.message)

      });
  }

 

  username: HTMLElement;
  checkLoginState() {
    var _this = this;
    this.amplifyService.authStateChange$.subscribe(authState => {
      if (!authState.user) {
        this.router.navigateByUrl("/auth");
      } else {

        this.user = authState.user;
        this._username = this.user.username;
      }
    });


  }

  getUserName() {

    var _this = this;
    var userid;
    this.amplifyService.authStateChange$.subscribe(authState => {
      if (!authState.user) {
        // this.router.navigateByUrl("/auth");
        //return this._username;
      } else {
        this.user = authState.user;
        this._username = this.user.username;
        // console.log("username" + this._username);

      }
    });



  }

  cleartoken(userid: string) {
    var _this = this;
    let user = Auth.currentAuthenticatedUser();

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var params = {
      UserAttributes: [
        /* required */
        {
          Name: "custom:jwttoken" /* required */,
          Value: "nosession"
        }
        /* more items */
      ],
      UserPoolId: this._AwsCongnitoUtil.USER_POOL_ID, /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (
      err,
      data
    ) {
      if (err) {
        _this.state = false;
        _this.toggle.emit(_this.state);
        _this.isErrorFound = true;
        console.log(err.message, err.stack);
      } // an error occurred
      else {
        //console.log(data);
      } // successful response
    });

    var awsparams = {
      UserPoolId: this._AwsCongnitoUtil.USER_POOL_ID  /* required */,
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
      if (err) {
        _this.isErrorFound = true;
      } //console.log(err, err.stack); // an error occurred
      else {
        //console.log("After signout token value is - " +  data.UserAttributes.find((a) => a.Name === 'custom:jwttoken').Value  )

        Auth.signOut()
          .then(data => {
            //console.log(data)
            _this.router.navigateByUrl("/auth");
            //location.reload();
          })
          .catch(err => console.log(err));
      } // successful response
    });
  }


  CheckMapping(filepath: string, account: string, agencyname: string) {
    let status = "false";

    if (this._userType == "Agency" || this._userType == "Agent") {
      let reordcnt = 0;
      this._data.readCSVfile(filepath).subscribe(
        csvData => {
          let allTextLines = csvData.split(/\r|\n|\r/);
          let headers = allTextLines[0].split(",");
          let csvDatalines = [];
          let filteredstring = "";

          let accountid: string, AssignedDate: string, Expirydate: string;
          let DCA: string;
          let cnt = 0;

          for (let i = 0; i < allTextLines.length; i++) {
            // split content based on comma
            let data = allTextLines[i].split(",");
            if (data.length === headers.length) {
              let tarr = [];
              for (let j = 0; j < headers.length; j++) {
                tarr.push(data[j]);
              }

              csvDatalines.push(tarr);
            }
          }

          let filteredvalues = [];

          var i = 0,
            k = 0,
            indx = [],
            msg;
          for (i = 0; i < csvDatalines.length; i++) {
            for (k = 0; k < csvDatalines[i].length; k++) {
              if (csvDatalines[i][k] === account) {
                indx = [i, k];

                if (cnt == 0) {
                  filteredstring =
                    "{'AccountId': '" +
                    csvDatalines[i][0] +
                    "','AssignedDate':'" +
                    csvDatalines[i][1] +
                    "','Expirydate':'" +
                    csvDatalines[i][2] +
                    "','DCA':'" +
                    csvDatalines[i][3] +
                    "'}";
                } else {
                  filteredstring +=
                    "," +
                    "{'AccountId': '" +
                    csvDatalines[i][0] +
                    "','AssignedDate':'" +
                    csvDatalines[i][1] +
                    "','Expirydate':'" +
                    csvDatalines[i][2] +
                    "','DCA':'" +
                    csvDatalines[i][3] +
                    "'}";
                }

                cnt++;
                reordcnt++;
              }
            }
          }

          if (cnt == 0) {
            this.state = false;
            this.toggle.emit(this.state);
            alert(
              "your agency is not authorized to search details of account no : " +
              this._account
            );

          }

          filteredstring = "[" + filteredstring + "]";

          var myObject = eval("(" + filteredstring + ")");
          var dates = [];
          for (let i in myObject) {
            dates.push(new Date(myObject[i]["AssignedDate"]));
          }

          var maxDate = new Date(Math.max.apply(null, dates));
          var minDate = new Date(Math.min.apply(null, dates));
          let maxdatefound = this.datePipe.transform(maxDate, "dd-MMM-yy");

          for (let i in myObject) {
            if (
              myObject[i]["AssignedDate"].toUpperCase() ===
              maxdatefound.toUpperCase()
            ) {
              if (myObject[i]["DCA"].toUpperCase() === agencyname.toUpperCase()) {
                status = "true";

                //this._data.getWinback(this._account).subscribe((data: any[]) => {
                this._data.requestCustomerData(this._account,this.pk,this.app).subscribe((data: any[]) => {

                  this._state.loadngflg = true;

                  this._winbackobj.customerInformation =
                    data[0]["body"]["CustomerInformation"];
                  this._winbackobj.accountInformation =
                    data[1]["body"]["AccountInformation"];
                  this._winbackobj.addressInformation =
                    data[2]["body"]["AddressInformation"];
                  this._winbackobj.collectionBucket =
                    data[3]["body"]["CollectionBucket"];
                  this._winbackobj.financialInformation =
                    data[4]["body"]["FinancialInformation"];
                  this._winbackobj.paymentInformation =
                    data[5]["body"]["PaymentInformation"];
                  this._winbackobj.smcInformation =
                    data[6]["body"]["SMCInformation"];

                  this.state = false;
                  this.toggle.emit(this.state);
                 

                  this.Clicked.emit(this._winbackobj);
                });
              } else {
                this.state = false;
                this.toggle.emit(this.state);

                alert(
                  "your agency is not authorized to search details of account no : " +
                  this._account
                );
              }
            }
          }
        },
        error => {

          this.state = false;
          this.toggle.emit(this.state);
          console.log("csv data loaded failed-" + error);
          this.isErrorFound = true;
          status = "false";
        }
      );

      if (reordcnt < 0) {
        this.state = false;
        this.toggle.emit(this.state);
        alert(
          "your agency is not authorized to search details of account no : " +
          this._account
        );
      }
    }
    else {

      this.state = true;
      this.toggle.emit(this.state);
      console.log("state is " + this.state);
      this.disabledState = true;
      this._data.requestCustomerData(this._account,this.pk,this.app).subscribe((data: any[]) => {

        this._state.loadngflg = true;
        console.log(data);
        this._winbackobj.customerInformation =
          data[0]["body"]["CustomerInformation"];
        this._winbackobj.accountInformation =
          data[1]["body"]["AccountInformation"];
        this._winbackobj.addressInformation =
          data[2]["body"]["AddressInformation"];
        this._winbackobj.collectionBucket =
          data[3]["body"]["CollectionBucket"];
        this._winbackobj.financialInformation =
          data[4]["body"]["FinancialInformation"];
        this._winbackobj.paymentInformation =
          data[5]["body"]["PaymentInformation"];
        this._winbackobj.smcInformation =
          data[6]["body"]["SMCInformation"];
        this.Clicked.emit(this._winbackobj);

        this.state = false;
        this.toggle.emit(this.state);
        console.log("state is " + this.state);
        this.disabledState = false;
      });

    }
  }

  getColor(flg: string) {
    switch (flg) {
      case "Active":
        return "green";
      case "InActive":
        return "red";
      default: {
        return "white";
      }
    }
  }

  compare(a, b) {
    if (new Date(a.AssignedDate) < new Date(b.AssignedDate)) return -1;
    if (new Date(a.AssignedDate) > new Date(b.AssignedDate)) return 1;
    return 0;
  }


  FindAccountNumber(accountNumber: string) {




    var _this = this;
    // Create DynamoDB service object
    var dynamodb = new DynamoDB({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
    });

    var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey +  '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));

    
    var params = {

      ExpressionAttributeNames: {
        "#AN": "Account_Number",
        "#AD": "Assigned_Date",
        "#ED": "Expiry_Date",
        "#AG": "Agency"
      },
      ExpressionAttributeValues: {
        ":v1": {
          S: decryptedAgencyCode
        }, ":v2": {
          N: accountNumber
        }
      },
      KeyConditionExpression: "Agency = :v1 AND Account_Number = :v2",
      ProjectionExpression: "#AN, #AD, #ED, #AG",
      TableName: "dca_ces9_assignment"
    };





    dynamodb.query(params, function (err, data) {
      if (err) {
        _this.isErrorFound = true;
        _this.state = false;
        _this.toggle.emit(_this.state);
        console.log(err.message, err.stack); // an error occurred
      }
      else {
        _this._ReadAssignmentData = [];
        data.Items.forEach(function (element, index, array) {
          let itemAssignment = new CSVRecord();
          itemAssignment.AccountNumber = element.Account_Number.N;
          itemAssignment.AssignedDate = _this.datePipe.transform(element.Assigned_Date.S, "dd-MMM-yy");
          itemAssignment.ExpiryDate = _this.datePipe.transform(element.Expiry_Date.S, "dd-MMM-yy");
          itemAssignment.AgencyName = element.Agency.S;

          _this._ReadAssignmentData.push(itemAssignment);
        });

        console.log(JSON.stringify(_this._ReadAssignmentData))
        
       

        _this._RecordFound = _this._ReadAssignmentData.filter(item => item.AgencyName == decryptedAgencyCode);
        console.log('Total ' + _this._ReadAssignmentData.length +' records found - ' + decryptedAgencyCode);
        console.log(JSON.stringify(_this._RecordFound))
        if (_this._RecordFound.length > 0) {
          console.log('making date array');
          var dates = [];
          for (let i in _this._ReadAssignmentData) {
            dates.push(new Date(_this._ReadAssignmentData[i]["AssignedDate"]));
          }


          var maxDate = new Date(Math.max.apply(null, dates));
          //var minDate = new Date(Math.min.apply(null, dates));
          let maxdatefound = _this.datePipe.transform(maxDate, "dd-MMM-yy");

          _this._RecordFound = _this._ReadAssignmentData.filter(item => item.AssignedDate == maxdatefound)
          console.log(maxdatefound + ' finding record - ' + JSON.stringify(_this._RecordFound));

          //localStorage.getItem("agencycode")
          if (_this._RecordFound.length > 0) {
            if (_this._RecordFound[0]["AgencyName"] == decryptedAgencyCode) {

             // console.log(localStorage.getItem("agencycode") + 'is record equal- ' + JSON.stringify(_this._RecordFound[0]["AgencyName"]));

              if (_this.validateExpiryDate(_this._RecordFound[0]["ExpiryDate"])) {

                _this.state = true;
                _this.toggle.emit(_this.state);
                _this.disabledState = true;
                console.log('finding details of this number');
                _this._data.requestCustomerData(accountNumber,this.pk,this.app).subscribe((data: any[]) => {

                  try {
                    
                 
                    _this._state.loadngflg = true;
                  
                    _this._winbackobj.customerInformation =
                      data[0]["body"]["CustomerInformation"];
                    _this._winbackobj.accountInformation =
                      data[1]["body"]["AccountInformation"];
                    _this._winbackobj.addressInformation =
                      data[2]["body"]["AddressInformation"];
                    _this._winbackobj.collectionBucket =
                      data[3]["body"]["CollectionBucket"];
                    _this._winbackobj.financialInformation =
                      data[4]["body"]["FinancialInformation"];
                    _this._winbackobj.paymentInformation =
                      data[5]["body"]["PaymentInformation"];
                    _this._winbackobj.smcInformation =
                      data[6]["body"]["SMCInformation"];
                    _this.Clicked.emit(_this._winbackobj);
                    _this.disabledState = false;
                    
                  } catch (ex) {

                    alert('Due to some technical issue details are not populating, error details - ' + ex.message);
                    _this.isErrorFound = true;
                    console.log(ex.message)
                  } finally {

                    _this.state = false;
                    _this.toggle.emit(_this.state);
                    _this.disabledState = false;
                  }


                },
                  (err) => {
                    _this.state = false;
                    _this.isErrorFound = true;
                    _this.toggle.emit(_this.state);
                    console.log(err.message)
                    _this.disabledState = false;
                  }
                );
                

              } else {


                alert(
                  "your agency is not authorized to search details of account no : " +
                  accountNumber
                );
                _this.state = false;
                _this.toggle.emit(_this.state);
                _this.disabledState = false;

              }
            } else {

              alert(
                "your agency is not authorized to search details of account no - " +
                accountNumber
              );
              _this.state = false;
              _this.toggle.emit(_this.state);
              _this.disabledState = false;

            }
          } else {

            alert("your agency is not authorized to search details of account no - " +
              accountNumber
            );
            _this.state = false;
            _this.toggle.emit(_this.state);
            _this.disabledState = false;
          }

        } else {

          alert("Check Assignment Mapping It seems your agency is not authorized to search details of account no - " +
            accountNumber
          );

          _this.state = false;
          _this.toggle.emit(_this.state);
          _this.disabledState = false;

        }

      }        // successful response
    });




  }


  validateExpiryDate(expirydate: string): boolean {

    console.log('validating expiry date');

    var q = new Date();
    var m = q.getMonth();
    var d = q.getDay();
    var y = q.getFullYear();

    var today = new Date();
    var todaydate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    var tdate = new Date(y, m, today.getDate());

    var expirtdt = new Date(this.datePipe.transform(new Date(expirydate), 'yyyy') + "-" +
      this.datePipe.transform(new Date(expirydate), 'MM') + '-' + this.datePipe.transform(new Date(expirydate), 'dd'));


    if (expirtdt < tdate) {
      console.log(' expiry date is not valid');
      return false;

    } else {

      console.log(' expiry date is valid');

      return true;
    }

  }


  searchAccountNumberInAWSES(account_no: string) {

    var isAccountNumberFound = false;

  var resultES = this.logawsesobj.getCampaignLogs(true);
  //console.log(resultES);
 return resultES.then(response => {   
                  
    try {

     

      if (response["hits"].total > 0) { 
        console.log ("Campaign status is " + response.hits.hits[0]._source["isactive"]) 
        var isCampaignActive = response.hits.hits[0]._source["isactive"];
        if(isCampaignActive)
        {
        console.log("Account number is being searched into campaign database")


        if(!isAccountNumberFound){
          console.log("Account number is not available in campaign now it is being searched into ods data")

          return this.logawsesobj.CheckAccountExistsInODS(account_no);
        
        }
        

        }else{

          console.log("Account number is being searched into ods data")

            return this.logawsesobj.CheckAccountExistsInODS(account_no);
          
          
          
        }

       }else{

        console.log("Account number is being searched into ods data")

          return this.logawsesobj.CheckAccountExistsInODS(account_no);
        
        
        

       }

    }catch{


    }
    });
   
    // campign-active-dca
  

    
    
    
  
  
  
  }
  
  clearContent() {

    this._winbackobj = null;

  }

    
 
  IsUserActive(userid: string) {

    var _this = this;

    console.log("User status is being checked..");

    var _csp = new CognitoIdentityServiceProvider();

    var params = {
      UserPoolId: this._AwsCongnitoUtil.USER_POOL_ID, // 'us-east-1_rWWNFO1Xi', /* required */
      Username: userid /* required */
    };
    _csp.adminGetUser(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred   

      }
      else {

       var  IsActive = data.Enabled;
       if(!IsActive){
        var agencyName = "";
        var agencyCode ="";
        try{
       
        agencyName = data.UserAttributes.find((a) => a.Name === "custom:agencyname").Value
         agencyCode = data.UserAttributes.find((a) => a.Name === "custom:agencycode").Value
        }catch{}
        
        _this.logawsesobj.logIntoAWSES(userid, "DCA", "User is Disabled", "", "", userid + " got disabled Please contact to admin", new Date().toISOString(),agencyName,agencyCode);
      //  console.log("Audit log saved"); 
      //  _this.alert.nativeElement.classList.add('show');
      //  _this.msgStr = _this.msgStr +  "<br> It seems that your login id has been blocked or disabled after exceeding failed login attempts. ";
      
      _this.router.navigateByUrl('/auth');
      console.log("logging out");
    
       }
      }

    });

  }



}
