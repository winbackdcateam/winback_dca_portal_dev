import { AmplifyService } from 'aws-amplify-angular';
import * as AWS from 'aws-sdk';


export class globals
{
 
  sessionTimeout: number = 100;
  sessionIdle: number = 300; // 5 mins
  sessionPing: number = 120;

  
  constructor() { }
  
  sendMail() {

    var _this = this;
    
    AWS.config.update({ region: 'us-west-2' });
    var ses = new AWS.SES();
    ses.config.update({ region: 'us-west-2' });
    
   
    
    var params = {
      Destination: {
      //  CcAddresses: [
      //     "naveen_kumar@astro.com.my"
      //  ], 
       ToAddresses: [
          "naveen_kumar@astro.com.my"
       ]
      }, 
      Message: {
       Body: {
        Html: {
         Charset: "UTF-8", 
         Data: "This message body contains HTML formatting. It can, for example, contain links like this one: <a class=\"ulink\" href=\"http://docs.aws.amazon.com/ses/latest/DeveloperGuide\" target=\"_blank\">Amazon SES Developer Guide</a>."
          }
        //   , 
        // Text: {
        //  Charset: "UTF-8", 
        //  Data: "This is the message body in text format."
        // }
       }, 
       Subject: {
        Charset: "UTF-8", 
        Data: "Test email"
       }
      },
    /*   , 
      ReplyToAddresses: [
      ], 
      ReturnPath: "", 
      ReturnPathArn: "",  */
      Source: "archnav26@gmail.com", 
      SourceArn: "arn:aws:ses:us-west-2:651805201906:identity/archnav26@gmail.com"
     };
     ses.sendEmail(params, function(err, data) {
       if (err) {
         AWS.config.update({ region: 'ap-southeast-1' });
         console.log(err.message, err.stack); // an error occurred
       }
       else {
         console.log(data); 
         AWS.config.update({ region: 'ap-southeast-1' });
       }     // successful response
       /*
       data = {
        MessageId: "EXAMPLE78603177f-7a5433e7-8edb-42ae-af10-f0181f34d6ee-000000"
       }
       */
     });
    
     AWS.config.update({ region: 'ap-southeast-1' });

  }


  sendTemporaryCredntials(userId: string, Password: string, toMail: string, ccMail: string) {
  
  AWS.config.update({ region: 'us-west-2' });
  var ses = new AWS.SES();
  ses.config.update({ region: 'us-west-2' });

var emailBody =    "Hi,<table> " +
"<tr> "+
"<td align='center'></td> "+
"</tr> "+
"<tr> "+
"<td>Please find your login details below:- <br><p> Login Id - "+  userId +" <br> Temporary Password - "+  Password +" </p></td> "+
"</tr> "+
"<tr> "+
"<td >Click Here for <a href='http://dca.astro.com.my'>Astro DCA Login</a></td>"+
"</tr><tr><td>Note - First Time Login will force you to reset password</td></tr>"+
"</table> <br> Thanks, <br> Auto Generated Mail <br> <img src='https://s3-ap-southeast-1.amazonaws.com/ce-prepaid-assets/Content/images/logo.png'>";


    var params = {
      Destination: { /* required */
        CcAddresses: [
         
          /* more items ccMail */
        ],
        ToAddresses: [
          toMail
          /* more items */
        ]
      },
      Message: { /* required */
        Body: { /* required */
          Html: {
           Charset: "UTF-8",
           Data: emailBody
          },
          Text: {
           Charset: "UTF-8",
           Data: "TEXT_FORMAT_BODY"
          }
         },
         Subject: {
          Charset: 'UTF-8',
          Data: 'Astro DCA Portal Temporary Credentials'
         }
        },
      Source: 'no-reply-dca@astro.com.my', /* required  no-reply@astro.com.my */
      ReplyToAddresses: [
         /*'naveen_kumar@astro.com.my',*/
        /* more items */
      ],
    };
    
    // Create the promise and SES service object
    var sendPromise = new AWS.SES().sendEmail(params).promise();
    

// Handle promise's fulfilled/rejected states
sendPromise.then(
  function(data) {
    console.log(data.MessageId);
  }).catch(
    function (err) {
      alert(err.message);
    console.error(err, err.stack);
    });
    AWS.config.update({ region: 'ap-southeast-1' });
  }


  autoVerifyEmailAddress()
  {
    var _this = this;
    AWS.config.update({ region: 'us-west-2' });
    var ses = new AWS.SES();
    

    var params = {
      IdentityType: "EmailAddress", 
      MaxItems: 123, 
      NextToken: ""
     };
     ses.listIdentities(params, function(err, data) {
       if (err) console.log(err, err.stack); // an error occurred
       else {
         console.log(JSON.stringify(data));
       
         data["Identities"].forEach(element => {
           _this.verifyEmail(ses,element);
         });
       
       }       // successful response
       /*
       data = {
        Identities: [
           "user@example.com"
        ], 
        NextToken: ""
       }
       */
         
     });

    /*
    if (lids.contains(address)) {
        //the address is verified so            
          return true;
    }
    VerifyEmailAddressResult res = ses
                .verifyEmailAddress(new VerifyEmailAddressRequest().withEmailAddress(address));
    System.out.println("Please check the email address " + address + " to verify it");
    */

  }
  
  verifyEmail(ses: any,  emailId: string){

    var params = {
      EmailAddress: emailId
     };
     ses.verifyEmailAddress(params, function(err, data) {
       if (err) console.log(err, err.stack); // an error occurred
       else {
         console.log(data);
         console.log("Please check the email address " + emailId + " to verify it");
       }    // successful response
     });
  }
 
}