import { DateTime } from 'aws-sdk/clients/transcribeservice';

export class DisconnectionDetails{

    SMC : string;
    OrderType : string;
    Date : DateTime;   

}