export class SMCInformation
{

    SMC : string;
    STATUS : string;
    Status_Date: string;
    OfferName: string;
    ARPU_BT: string;
    ARPU_AT: string;
    MinSubFee: string;
    CommStartDate: string;
    CommEndDate: string;
    Paired: string;
    DMT: string;

}