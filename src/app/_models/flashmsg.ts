import { DateTime } from 'aws-sdk/clients/ram';

export class FlashMessage
{

    "Title" : string;
    "StartDate" : DateTime;
    "EndDate" : DateTime;
    "Status" : string;
    "Text" :string;

}