export class BillInformation 
{   

    BA_ACCOUNT_NO: string;
    InvoiceDate: string;
    InvoiceNumber: string;
    PayBy: string;
    CYCLE_SEQ_NO: string;
    Cycle_code: string;
    prev_balance: string;
    Payment: string;
    OverDue_Charges: string;
    New_Charges: string;
    Total_Amt_due: string;
    //_BillDetails : BillDetails[];
}

export class BillDetails{
    SMC: string;
    ChargeCode: string;
    DESCRIPTION: string;
    Period: string;
    AMOUNT: string;
    GST: string;
    Total: string;
}