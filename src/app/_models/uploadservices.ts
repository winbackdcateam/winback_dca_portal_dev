import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import AWS, { S3 } from 'aws-sdk';
import * as fs from 'file-system';

import { ContainerEvents, FileObject, User } from './types';
import { S3Factory } from './utils/s3';
import { s3Config } from './config/awsconfig';
import { _iterableDiffersFactory } from '@angular/core/src/application_module';
import { awsCognitoUtil } from './awscognitoutil';
import { Auth } from 'aws-amplify';





@Injectable()
export class UploadFileService {
   
   s3 : any;
  bucketName : string = "dcaportalfiles";
  FOLDER: string = 'dcamappingfiles/';
  _filelist: string;
  private _listOfFile: Observable<string>;
  constructor( private _awsCognitoUtil:  awsCognitoUtil) {
    Auth.currentSession()
    .then(data => {
      console.log("current session")

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: _awsCognitoUtil._IDENTITY_POOL_ID,
        Logins: {
          'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': data.getIdToken().getJwtToken()
        }
      });
  
      AWS.config.update({
        region: 'ap-southeast-1',
        credentials: AWS.config.credentials
      }); 
  
      /* AWSGlobal.config.update({region: 'ap-southeast-1',
      credentials: AWS.config.credentials});
     */


    this.s3 = new S3(
      {
        region: _awsCognitoUtil._REGION //
      }
    );
    //---------------------------------
})
.catch(err => console.log(err)); 
    //-------


   }

  uploadfile(file:any)  {

    var _this = this;
    var msg = "";
  
    const params = {
      Bucket: "dcaportalfiles",
      Key: this.FOLDER + file.name.toString(),
      Body: file
    };

    this.s3.upload(params, function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err);
        msg = err.message;
        return true;
      }
      else {
        ///console.log('Successfully uploaded file.', data);
        msg = 'Successfully uploaded file.' + JSON.stringify(data);
        _this.readFileFromS3(file.name)
        return false;
       
      }
    });


    //this.s3.upload.maxPartSize(20971520);


   


    
  }


  listofFiles(): Observable<string> {

    let _list = " No mapping file found";
    var _this = this;
    var observable: any;
    var params = {
      Bucket: "dcaportalfiles", 
      Delimiter: '/',
      Prefix: this.FOLDER,  
      //Key: ,
      MaxKeys: 300
    };


    return Observable.create(function (observer) {
      _this.s3.listObjectsV2(params, (err, data) => {
        if (err) {
          console.log(err.message, err.stack); // an error occurred
          // return _list  + " - " + err;
        } else {
          var contents = data.Contents;
          //console.log(JSON.stringify(data.Contents));
          _list = "<ul>";
          for (let index = 1; index < data['Contents'].length; index++) {
            var filename = data['Contents'][index]['Key'].split("/");
            _list += "<li onclick='downloadFile('" + data['Contents'][index]['Key'].toString() + "','" + _this.bucketName + "')' >" + filename[1].toString() + "</li>";

        }
        _list += "</ul>";
          // console.log(data.Contents);
          // _list = "<ul>";
          // contents.forEach(function (content) {
          //   _list += "<li onclick='downloadFile('" + content.Key.toString() + "','" + _this.bucketName + "')' >" + content.Key.toString() + "</li>";
          // });

          // _list += "</ul>";
         // console.log(_list)
          observer.next(_list);
          observer.complete();





        }
      });
    });



  }

  readFileFromS3(filename: string) {
    
    const dynamoS3Bucket = new S3();
    // const stream = dynamoS3Bucket.getObject({ Bucket: 'dcaportalfiles', Key: this.FOLDER + filename })
    //   .createReadStream();
   

      var params = {Bucket: 'dcaportalfiles', Key: this.FOLDER + filename };
      dynamoS3Bucket.getObject(params, function(err, json_data)
      {
        if (!err) {
            
         // console.log("s3 object loaded" + JSON.stringify(json_data["Body"]));
          //console.log(json_data);
          // var attach = json_data.Body.toString('base64');

          // console.log(attach)

          // var json = JSON.parse(new Buffer(json_data.Body.toString()).toString("utf8"));
  
          // console.log(json);

          // var attachment, file;
          // file = new Buffer(json_data.Body.toString(), 'binary');
          // attachment = file.toString('base64');
             
        } else {
          
          console.log("S3 file buffering error - " + err.message);
       }
     });
    
    
    
  

    
}





  loadfile(fileName: string, _bucket: string) {

    var s3 = new S3({ apiVersion: '2006-03-01' });
    var params = { Bucket: _bucket, Key: fileName };


    // -> Do download Files
    s3.getObject(params)
      .createReadStream()
      .on('error', function (err) {
        // res.status(500).json({error:"Error -> " + err});
      });


    //var file = fs.createWriteStream('../assets/' + fileName);
    //var file = require('fs').createWriteStream('../assets/' + fileName);
    //s3.getObject(params).createReadStream().pipe(file);

    // const string = new TextDecoder('utf-8').decode(data.Body);
    // console.log(string);
  }

}
