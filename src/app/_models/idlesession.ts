import { Auth } from 'aws-amplify';
import { IdleTimeoutServiceService } from '../idle-timeout-service.service';
import { UserIdleService } from 'angular-user-idle';
import { CognitoIdentityServiceProvider } from 'aws-sdk/clients/all';
import { awsCognitoUtil } from './awscognitoutil';
import { Router } from "@angular/router";
import { EncrDecrService } from './EncrDecrService';
import { logawses } from './logawses';

export class SessionUserIdle {

  counter: string;
  private userIdle: UserIdleService;
  _IsSessionAlive: boolean = true;
  _AwsCognitoUtil = new awsCognitoUtil;
  //router: Router;
  sessionKey: string = sessionStorage.getItem("sessionkey");
  private EncrDecr: EncrDecrService;
  logawsesobj: logawses;

  constructor(private router: Router) {}

  CheckPageIdle(userid: string) {
    //Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      this.counter = count.toString();
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => console.log('Time is up!' + this.signout(userid)));

    return this.counter;
  }


  public signout(userid: string = "") {
       
    this._IsSessionAlive = false;

    try {
    
      console.log("Audit log to be saved");
      
      var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

      var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));

      var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

      var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
      var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
     
    
      this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", "Signout", decryptedFullName, decryptedUserType, decryptedUserId + " was logged out ", new Date().toISOString(), decryptedAgencyName,decryptedAgencyCode);
   
    } catch (er) { console.log(er.message); } finally {
      console.log("Audit log saved");
      this.cleartoken(userid)
      sessionStorage.clear();
      localStorage.clear();
    }
  }


  cleartoken(userid: string) {
    
    var _this = this;

    if (userid == "") {
      sessionStorage.clear();
      localStorage.clear();
    Auth.signOut({ global: true })
    .then(data => console.log(data))
      .catch(err => {
        _this.router.navigateByUrl("/auth");
        console.log(err.message)
      });

    } else {

      let user = Auth.currentAuthenticatedUser();

      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var params = {
        UserAttributes: [ /* required */
          {
            Name: 'custom:jwttoken', /* required */
            Value: 'nosession'
          },
          /* more items */
        ],
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, /* required */
        Username: userid /* required */
      };
      cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
        if (err) {
          _this.router.navigateByUrl("/auth");
          console.log(err, err.stack);
        }// an error occurred}
        else { console.log(data); }      // successful response
      });

      var awsparams = {
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, /* required */
        Username: userid /* required */
      };
      cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
        if (err) {
          
          console.log(err.message);
          _this.router.navigateByUrl("/auth");
          console.log(err, err.stack);
        }// an error occurred
        else {
          // console.log("After signout token value is - " +  data.UserAttributes.find((a) => a.Name === 'custom:jwttoken').Value  )

          Auth.signOut({ global: true })
            .then(data => {
              console.log("logged out")
              sessionStorage.clear();
              localStorage.clear();
            })
            .catch(err => {
              _this.router.navigateByUrl("/auth");
              console.log(err)
            });

        }    // successful response
      });
    }

  }

  
  stop() {

    this.userIdle.stopTimer();
    this.startWatching();
    return false;
  }

  stopWatching() {
    this.userIdle.stopWatching();
  }

  startWatching() {
    this.userIdle.startWatching();
  }

  restart() {
    this.userIdle.resetTimer();
  }



}