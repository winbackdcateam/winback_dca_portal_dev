import { ConfigService } from '../../../src/app/config.service';
import { environment } from 'src/environments/environment.prod';

export class awsCognitoUtil
{


     USER_POOL_ID: string;
    _REGION: string;
    _IDENTITY_POOL_ID: string;
    _CLIENT_ID: string;  
    
    CAPTCHA_SITE_KEY: string;  
    CLIENT_IP: string;
     partnerkey: string;
    apipartnerpassword: string; 

    confenv = new ConfigService();

    constructor() {
    
      let envobj = this.confenv.getConfig();        
      this.USER_POOL_ID = environment.dcauserpoolsid;
      this._REGION = environment.region
      this._IDENTITY_POOL_ID = environment.dcapoolid;
      this._CLIENT_ID = environment.dcawebclientid;  
      this.CAPTCHA_SITE_KEY = environment.CAPTCHA_SITE_KEY;  
       
  
      
    }
    
    
}