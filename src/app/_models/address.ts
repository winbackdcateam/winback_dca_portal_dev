export class AddressInformation
{

    Account_ID    : string;
    ServiceAddress: string;
    BillingAddress: string;
    MailingAddress: string;

}