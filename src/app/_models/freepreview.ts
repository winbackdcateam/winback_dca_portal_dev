import { DateTime } from 'aws-sdk/clients/rekognition';

export class FreePreview
{
    "SMC" : string;
    "Status" : string;
    "ChannelName" : string;
    "ChannelCode" : string;
    "StartDate" : DateTime;
    "EndDate" : DateTime;
}