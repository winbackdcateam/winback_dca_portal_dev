import { DateTime } from 'aws-sdk/clients/ec2';

export class Users
{

    constructor() { }
    
    Id : string;
    FullName: string;
    UserName: string;
    UserType : string;
    UserStatus:string;
    email :string;
    Winback :boolean;
    Winlead :boolean;
    logintime:string;
    expirydate : string;
    loginendtime:string;
    is_email_verified:boolean;
    IsEnabled:boolean;
    DateOfCreation: DateTime;
    LastModifiedDate: DateTime;
    LastPasswordModifiedDate: DateTime;
    concurrentvalue: string;
    failedattemptdate: string;
    failedattemptcount: string;
    Phone :string;
    agencyname : string;
    agentId :string;
    isDeactivtionValid :boolean;
    isLoginTimeEditable: boolean;
    isAgentLimitEditable: boolean;
    agencyCode : string;
    agencyFullName: string;
    agentLimit: string;
}