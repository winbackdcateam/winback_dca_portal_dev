import * as elasticsearch from 'elasticsearch-browser'
import {
    
    awsCognitoUtil
   
} from "../_models";
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { async } from '@angular/core/testing';
import { ɵConsole } from '@angular/core';


export class logawses{

  

  _AwsCongnitoUtil = new awsCognitoUtil();
   
  

  constructor() {
 // this.ipaddress = sessionStorage.getItem("ClientIP");
    //console.log("log captured - " + this.ipaddress);
  }

  logIntoAWSES( 
     userid : any,     
     project : any,
     event : any,
     loggedUser: any,
     role : any,
     description: any,
     time: any,agencyName : string,agencycode: string) {
   

    var index = 'audit-logs-dca';
    var type = 'doc';

    var domain = 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com'
    var region = 'ap-southeast-1'

    

    var elasticSearchClient =  new elasticsearch.Client({
      host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com', 
    /*   log:'trace', */
     /*  connectionClass: HttpAmazonESConnector, */
      amazonES: {
       
        region: this._AwsCongnitoUtil._REGION
      },
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });
    let curr_dt = new Date();

    let publishdate = curr_dt.toISOString().substring(0,10);  
  
    (async ()=>{

      // Let's start by indexing some data
    await  elasticSearchClient.index({
        index: 'audit-logs-dca',
        // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
        body: {
          loggeduser: userid,
          projectname: project,
          eventname: event,
          username: loggedUser ,
          roletype: role,
          agnecycode:agencycode,
          agency:agencyName,
          desc: description,
          eventtime: time,
          clientip:  sessionStorage.getItem("ClientIP"),
          published: publishdate
        }
      })
    })();
     
      
   
  
  
  
  }


 public flgCampignAWSES( 
    userid : any,     
    project : any,
    event : any,
    loggedUser: any,
    role : any,
    description: any,
    time: any,startdate : string,enddate: string, flg :any) {
  

   var _index = 'campign-active-dca';
   var type = 'doc';

   var domain = 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com'
   var region = 'ap-southeast-1'

   

   var elasticSearchClient =  new elasticsearch.Client({
     host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com', 
   /*   log:'trace', */
    /*  connectionClass: HttpAmazonESConnector, */
     amazonES: {
      
       region: this._AwsCongnitoUtil._REGION
     },
     headers: {
       'Accept': 'application/json',
       'Content-Type': 'application/json'
     }
   });
   let curr_dt = new Date();

   let publishdate = curr_dt.toISOString().substring(0,10);  
 if (flg){
   (async ()=>{

     // Let's start by indexing some data
   await  elasticSearchClient.index({
       index: _index,
       // type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
       body: {
         loggeduser: userid,
         projectname: project,
         eventname: event,
         username: loggedUser ,
         roletype: role,
         startdate:startdate,
         enddate:enddate,
         isactive: flg,
         desc: description,
         eventtime: time,
         clientip:  sessionStorage.getItem("ClientIP"),
         published: publishdate
       }
     })
   })();
  }else
  {

      elasticSearchClient.index({
      index: _index,
      type: '_doc', // uncomment this line if you are using Elasticsearch ≤ 6
      body: {
        loggeduser: userid,
        projectname: project,
        eventname: event,
        username: loggedUser ,
        roletype: role,
        
        isactive: flg,
        desc: description,
        eventtime: time,
        clientip:  sessionStorage.getItem("ClientIP"),
        published: publishdate
      }
    })
  


    /* elasticSearchClient.delete({
      index: _index,
      type: '_doc',
      id:'PYGonHEBxPbPZG91FnV8' 
     }, function (error, response) {
      console.log(error);
     }); */

  }
    
     
  
 console.log("Campaign status changed")
 
 
 }

 getCampaignLogs(isActive:boolean){


  var _index = 'campign-active-dca';
  var type = '_doc';
  var es =  new elasticsearch.Client({
    host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com', 
  /*   log:'trace', */
   /*  connectionClass: HttpAmazonESConnector, */
    amazonES: {
     
      region: this._AwsCongnitoUtil._REGION
    },
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
  let curr_dt = new Date();

if(isActive){

  return es.search({
    index:_index,
    type: type,
    filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
    body:{
     query:{
      match: { "projectname": 'dca' }
     },"size": "1",
     "sort": [
       {
         "eventtime": {
           "order": "desc"
         }
       }
     ]
    }
  });

}else
{
  console.log("full campaign logs");
  return es.search({
    index:_index,
    type: type,
    size:100,
    filterPath: ['hits.hits._source', 'hits.total'],
    body:{
     query:{
      "match_all": {}
     },"sort": [
      {
        "eventtime": {
          "order": "desc"
        }
      }
    ]
    }
  });
 }
}

 async findUserPosition() {
    if (navigator.geolocation) {
   await navigator.geolocation.getCurrentPosition((position) => {
     sessionStorage.setItem("position", JSON.stringify(position));
     alert("position captured - "+ sessionStorage.getItem("position"));
      });
    } else {
      alert("Geolocation is not supported by this browser.");
    }
  }

checkAccountExistsIntoBusinessDatabase(_AccountNo:any){}
  //https://api.ipify.org?format=jsonp&callback=getip
  
  
CheckAccountExistsInODS(account_no:any){

  var index = 'access-logs-dca';
  var type = 'doc';

  var domain = 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com'
  var region = 'ap-southeast-1'

  

  var elasticSearchClient =  new elasticsearch.Client({
    host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com', 
  /*   log:'trace', */
   /*  connectionClass: HttpAmazonESConnector, */
    amazonES: {
      region: this._AwsCongnitoUtil._REGION
    },
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  });
 
  var result;
  
  return elasticSearchClient.search({
    index: 'access-logs-dca',
    type: 'doc',
    filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
    body: {
      query: {
        match: { "account": account_no }
      },
    },
     '_source': ['account', 'agency']
  });

}

}