export class IntrxnInformation
{
    START_TIME :string;
    Title      :string;
    Media      :string;
    Direction  :string;
    Text       :string;
}