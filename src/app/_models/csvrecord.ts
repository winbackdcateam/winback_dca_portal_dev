export class CSVRecord{
    Id: string;
    AccountNumber: string;
    AssignedDate: string;
    ExpiryDate: string;
    AgencyName: string;
    "InsertDate": string;
    "InsertedBy":string;
    "ModifiedBy": string;
    "ModifyDate": string;
}