import { integer } from 'aws-sdk/clients/cloudfront';

export class AccountMapping
{
    user : string;
    name : string; 
    account : string[];
}

export class AccountDESMapping
{

    AccountId :string
    AssignedDate: string
    Expirydate : string 
    DCA :string

}