import { Auth } from 'aws-amplify';
import { CognitoIdentityServiceProvider } from 'aws-sdk/clients/all';
import { stringType } from 'aws-sdk/clients/iam';
import { awsCognitoUtil } from './awscognitoutil';
import { DatePipe } from '@angular/common';
import { Users } from './users';
import AWS from 'aws-sdk';
import * as AWSGlobal from "aws-sdk/global";

export class validate {

    UserCurrentStatus = new Users();

    constructor() { }


    public RequiredFields(val: string): boolean {

        val = val.trim()

        if (val == "") {
            return false;
        }

        return true;
    }

    public IsNumber(val: number): boolean {

        val = Math.sign(val)

        if (val == -1 || val == -0) {
            return false;
        } else if (isNaN(val)) {

            return false;
        }

        return true;
    }


    public validateSelection(val: string): boolean {
        if (val == "") {
            return false;
        }
        return true;
    }


    public validateLoginId(val: string): boolean {
        if (val == "") {
            return false;
        }
        if (val.indexOf(' ') >= 0) {
            return false;
        }

        if (/\s/.test(val)) {
            return false;
        }


        return true;
    }

    public PasswordFormatValidation(val: string): boolean {

        var strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        if (val.length < 8) {
            return false;
        }

        if (val.indexOf(' ') >= 0) {
            return false;
        }

        if (/\s/.test(val)) {
            return false;
        }


        return strongRegex.test(String(val).toLowerCase());


    }


    validateEmail(search: string): boolean {
        var serchfind: boolean;

        var regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

        serchfind = regexp.test(search);

        // alert(serchfind)
        return serchfind
    }

    public PasswordCheck(value: string) {
        let hasNumber = /\d/.test(value);
        let hasUpper = /[A-Z]/.test(value);
        let hasLower = /[a-z]/.test(value);
        let hasSpecial = /[!@#$%^&*(),.?":{}|<>]/.test(value);
        let hasNoSpace = /^\S*$/.test(value);
        // console.log('Num, Upp, Low', hasNumber, hasUpper, hasLower);
        const valid = hasNumber && hasUpper && hasLower && hasSpecial && hasNoSpace;
        // alert(valid)

        if (!valid) {

            // return what´s not valid
            return false;
        } else {
            return true;
        }
    }

    CheckCurrentUserStatus() {

        Auth.currentAuthenticatedUser({
            bypassCache: false  // Optional, By default is false. If set to true, this call will send a request to Cognito to get the latest user data
        }).then(user => console.log(user))
            .catch(err => console.log(err));

    }

    getSession() {

        Auth.currentSession()
            .then(data => console.log(data))
            .catch(err => console.log(err));
    }

    remeberDevice() {
        // cognitoUser.setDeviceStatusRemembered({
        //     onSuccess: function (result) {
        //         console.log('call result: ' + result);
        //     },
        //     onFailure: function(err) {
        //         alert(err);
        //     }
        // });

    }

    getUserStatus(_UserId: stringType, _AwsCognitoUtil: awsCognitoUtil) {


        

        let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();


        
        var _this = this;
        var awsparams = {
            UserPoolId: _AwsCognitoUtil.USER_POOL_ID,
            Username: _UserId,
            region: 'ap-southeast-1' 
        };

        return new Promise((resolve, reject) => {

           
            cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
                if (err) {
                    console.log(err.message + "User ID" + _UserId + "Pool Info -" + _AwsCognitoUtil.USER_POOL_ID);
                    reject(err.message); // an error occurred
                }
                else {


                    let IsTimeExpired = data.UserAttributes.find((a) => a.Name === 'custom:logintime').Value
                    let IsUserEnabled = data.Enabled;


                    _this.UserCurrentStatus.IsEnabled = IsUserEnabled
                    _this.UserCurrentStatus.logintime = IsTimeExpired;
                    resolve(_this.UserCurrentStatus);
            
                  
                }
            });


        });
    
}


    validateExpiryDate(expirydate: string, datePipe: DatePipe): boolean {

        //console.log('validating expiry date');

        var q = new Date();
        var m = q.getMonth();
        var d = q.getDay();
        var y = q.getFullYear();

        var today = new Date();
        var todaydate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

        var tdate = new Date(y, m, today.getDate());

        var expirtdt = new Date(datePipe.transform(new Date(expirydate), 'yyyy') + "-" +
            datePipe.transform(new Date(expirydate), 'MM') + '-' + datePipe.transform(new Date(expirydate), 'dd'));


        if (expirtdt < tdate) {
          //  console.log(' expiry date is not valid');
            return false;

        } else {

           // console.log(' expiry date is valid');

            return true;
        }

    }

    validatelogintimeDate(logintimestr: string): boolean {


        var arr = logintimestr.split('-')

        var a = arr[0] + ":00";
        var b = arr[1] + ":00";

        var aa1 = a.split(":");
        var aa2 = b.split(":");

        var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
        var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
        var dd1 = d1.valueOf();
        var dd2 = d2.valueOf();

        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var ttm = time.split(":");
        var todaytimepars = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(ttm[0], 10), parseInt(ttm[1], 10), parseInt(ttm[2], 10));
        var todaytime = todaytimepars.valueOf();

        //console.log("starttime -" + dd1 + "endtime  -" + dd2 + "Today time - " + todaytime)
        if (todaytime < dd1 || todaytime > dd2) {

            return false;
        }

        return true;

    }

}