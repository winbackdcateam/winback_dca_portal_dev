export class CustomerInformation
{

    Full_name   : string;
    Account_ID	: string;
    Balance	    : string;
    SubType	: string;
    Type	: string;
    Race	: string;
    ID	: string;
    ID_Type	: string;
    COUNTRY	: string;
    Home_Phone	: string;
    MOBILE_PHONE	: string;
    Office_Phone	: string;
    e_mail	: string;
    VIP	: string;
    collection_indicator	: string;    

}