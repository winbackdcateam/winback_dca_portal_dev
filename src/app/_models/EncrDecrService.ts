import { Injectable } from '@angular/core';
import * as CryptoJS from 'crypto-js';

@Injectable({
    providedIn: 'root'
})

export class EncrDecrService {
    constructor() { }
 
  
    
  set(keys: any, value: any) {
    // Encrypt
    var ciphertext = CryptoJS.AES.encrypt(value, keys).toString();
    return ciphertext;
  }
// Decrypt
  get(keys: any, ciphertext: any) {

    var originalText = "none";
    try {
      var bytes = CryptoJS.AES.decrypt(ciphertext, keys);
      originalText = bytes.toString(CryptoJS.enc.Utf8);
      
    } catch (e)
    {
      console.log( keys + " Original Message :- " + originalText + "error found:-" + e.message); 

    } finally {
     // console.log(keys + " Original Message :- " + originalText);
     // console.log( "Session value :- " +  sessionStorage.getItem('sessionkey').toString()); 
    }

   // console.log( keys + " Original Message :- " + originalText); // 'my message'

    return originalText;
  }
  
  getRandomSessionKey(bytes: number) {
    const randomValues = new Uint8Array(bytes);
    window.crypto.getRandomValues(randomValues);
    return Array.from(randomValues).map(this.intToHex).join('');
  }
  
   intToHex(nr: number) {
    return nr.toString(16).padStart(2, '0');
  } 
}