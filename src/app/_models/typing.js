const dotenv = require('dotenv');
dotenv.config();

module.exports = function() {
    return dotenv;
};