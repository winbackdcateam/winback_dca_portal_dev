export const s3Config = {
    buckets: {
    'us-east-1':'s3://dcaportalfiles',
    'ap-south-1': 'http://dcaportalfiles.s3.amazonaws.com/',
    'us-west-1': 'uploads.us-west-1.tensult.com'
    },
    defaultRegion: 'ap-southeast-1'
  };