import {
  Component, OnInit, ElementRef, ViewChild,
  AfterViewInit, HostListener, ChangeDetectorRef
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { HeaderComponent } from '../header/header.component';
import { MdbTableService, MdbTablePaginationComponent, MdbTableDirective } from 'angular-bootstrap-md';
import * as S3 from 'aws-sdk/clients/s3';
import { Subject, Observable } from 'rxjs';
import { UploadFileService, dynamodbConfig, awsCognitoUtil, CSVRecord, validate, EncrDecrService } from '../_models';
import { Auth } from 'aws-amplify';
import Amplify, { Storage } from 'aws-amplify';
import { AmplifyService } from 'aws-amplify-angular';
import { Papa } from 'ngx-papaparse';
import { saveAs } from 'file-saver';
import { Router } from '@angular/router';
import { ContainerEvents, FileObject, FileObjectStatus } from '../_models/types';
import AWS, { DynamoDB } from 'aws-sdk';
import { stringType } from 'aws-sdk/clients/iam';
import { CookieService } from 'ngx-cookie-service';
import * as elasticsearch from 'elasticsearch-browser';
import { logawses } from '../_models/logawses';




@Component({
  selector: 'app-load-mapping-file',
  templateUrl: './load-mapping-file.component.html',
  styleUrls: ['./load-mapping-file.component.scss']
})
export class LoadMappingFileComponent implements OnInit, AfterViewInit {

  @ViewChild('xAgencyAssignmentFilter') _AgencyAssignmentFilter: ElementRef;
  searchCampaignLogsText: any;
  
  @HostListener('click', ['$event']) onclick(event: Event) {
    
    console.profile(); 
    console.profileEnd(); 
    if (console.clear) console.clear();
   
    
  }

  lblExpandList: string='Expand To Load List';
  // Pagination parameters.
  headAssignmentElements = ["Account Number", "Assigned Date", "Expiry Date", "Agency Code"]
  headCampaignElements =["Event Created By", "Campaign Status",	"Detail",	"Time",	"IP Address",	"Log Time"]
  
  //headCampaignElements =["Event Created By",	"Project Name",	"Event Name",	"User Name",	"Role",	"Start Date",	"End Date",	"Campaign Status",	"Detail",	"Time",	"IP Address",	"Log Time"]
  campaignLogsArray : campaignlogs[] =[];
  pageTitle: string;
  p: Number = 1;
  count: Number = 5;
  AssignDateUpdateMsg: string;
  ExpiryDateUpdateMsg: string;
  selectedFiles: FileList;
  _htmlstring: string;
  searchText: string = '';
  IsDeletionRequired: boolean = false;
  showprogressbar: boolean = false;
  recordFoundMessage: string;
  _userType: any;
  sessionKey :string = sessionStorage.getItem("sessionkey");
  previous: any = [];
  isFileUploading: boolean;
  s3: any;
  fileData: File = null;
  _ReadAssignmentData: CSVRecord[] = [];
  msgStr: string;
  output: string;
  _objValidate = new validate ();
  isCampaignActive :boolean =false;
  CampaignUploadPanel:boolean = false;
  

  @ViewChild('alert') alert: ElementRef;
  @ViewChild('txtAgencyDiv') txtAgencyDiv: ElementRef;
  @ViewChild('ChkboxCampaign')ChkBoxCompaign: ElementRef;
  @ViewChild(HeaderComponent) objHeaderComp: HeaderComponent;

  constructor(private authService: AmplifyService, private _awsCognitoUtil: awsCognitoUtil,private EncrDecr: EncrDecrService,
    private tableService: MdbTableService, private datePipe: DatePipe, private cookieService: CookieService,
    private router: Router, private papa: Papa, private el: ElementRef, private cdRef: ChangeDetectorRef,
    private logawsesobj: logawses,private uploadService: UploadFileService) {
    console.log('Application loaded. Initializing data.');
    this.cookieService.set('expires', new Date().getTime().toString());
    this.s3 = new S3(
      {
        region: _awsCognitoUtil._REGION 
      }
    );

  }

  ngOnInit() {
    this.objHeaderComp.flg = false;
    var _this = this;

    let itemToReturn = new Observable();
    const sequence = this.uploadService.listofFiles();

    sequence.subscribe(function (data) { _this._htmlstring = data });


  }

  ngAfterViewInit() {

    this.cdRef.detectChanges();

    var type = localStorage.getItem("type");
    var userid = localStorage.getItem("userid");
    var agencycode = localStorage.getItem("agencycode");

    this.pageTitle = "Manage Campaign Targeted List";

    if (type == "Agent" || type == "Agency") {

      this._AgencyAssignmentFilter.nativeElement.style.display = 'none';
      this.txtAgencyDiv.nativeElement.style.display = 'none';

      this.pageTitle = "Check Agency Assignment";

    }
    else if (type == "BU") {
      this.IsDeletionRequired = true;
     // this.readItem()
    }
    var resultCampaignES = this.logawsesobj.getCampaignLogs(false);
    resultCampaignES.then(response => {
                   
    try {

      if (response["hits"].total > 0) { 

       
        response.hits.hits.forEach(obj => {
          let objCampaign = new campaignlogs();
          objCampaign.loggeduser= obj["_source"].loggeduser;
          objCampaign.projectname= obj["_source"].projectname;
          objCampaign.eventname= obj["_source"].eventname;
          objCampaign.username= obj["_source"].username;
          objCampaign.roletype= obj["_source"].roletype;
          objCampaign.startdate= obj["_source"].startdate;
          objCampaign.enddate= obj["_source"].enddate;
          objCampaign.isactive= obj["_source"].isactive;
          objCampaign.desc= obj["_source"].desc;
          objCampaign.eventtime= obj["_source"].eventtime;
          objCampaign.clientip= obj["_source"].clientip;
          objCampaign.published= obj["_source"].published;
       
        this.campaignLogsArray.push(objCampaign);
       
       });
      }

    }
    catch{}

     });
  console.log("checking campaign");

  var resultES = this.logawsesobj.getCampaignLogs(true);
  //console.log(resultES);
  resultES.then(response => {   
                  
    try {

      if (response["hits"].total > 0) { 
        //console.log (response.hits.hits[0]._source["isactive"]) 
       this.isCampaignActive = response.hits.hits[0]._source["isactive"];
       if(this.isCampaignActive)
       {
        this.strCampaignStatus="Campaign is activated";
        
       }else{

        this.strCampaignStatus="Campaign is not activated";
       }
       console.log(this.isCampaignActive);
       }

    }catch{


    }
    });

 
  }

  expandelement: HTMLElement;

 ShowUploadPanel(flg:boolean){
  this.expandelement = document.getElementById('eid') as HTMLElement;
  
  if(flg)
  {
   this.CampaignUploadPanel = false; 
   this.expandelement.className  ='far fa-plus-square';
   this.lblExpandList ="Expand Load Panel";
   console.log("now panel is sending " + flg +" Panel must be collapsed");

  }else{
   


    this.CampaignUploadPanel = true; 
    this.expandelement.className = 'far fa-minus-square';
    this.lblExpandList ="Collapse Load Panel";
    console.log("now panel is sending " + flg +" Panel must be expanded");

  }
 }
  

  @HostListener('input') oninput() {

    this.searchText = this.el.nativeElement.querySelector('#search').value;
    this.searchItems();
    this.searchCampaignLogsText  = this.el.nativeElement.querySelector('#txtSearchCampaignLogs').value;

  }


  downloadFiles(filename: string, bucket: string) {

    this.uploadService.loadfile(filename, bucket);

  }

  strCampaignStatus:any;
activateCompaign(isChecked: boolean){

 var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));
   
 
 if(isChecked)
 {
  this.logawsesobj.flgCampignAWSES(decryptedUserId, "DCA", "Campaign Status", "", "", decryptedUserId + " activated campaign", new Date().toISOString(),'feature not added','feature not added',isChecked);

  this.msgStr ="Campaign has been activated";

  this.strCampaignStatus="Campaign is activated";

 }else{
  this.logawsesobj.flgCampignAWSES(decryptedUserId, "DCA", "Campaign Status", "", "", decryptedUserId + " deactivated campaign", new Date().toISOString(),'feature not added','feature not added',isChecked);

  this.msgStr ="Campaign has been deactivated";

  this.strCampaignStatus="Campaign is not activated";

 }
   
   this.alert.nativeElement.classList.add('show');
}


  _progressbar: HTMLElement;
  _LoadingPercentage: string

 
upload(event) {


    Auth.currentSession()
    .then(data => {
      console.log("current session")

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: this._awsCognitoUtil._IDENTITY_POOL_ID,
        Logins: {
          'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': data.getIdToken().getJwtToken()
        }
      });
  
      AWS.config.update({
        region: 'ap-southeast-1',
        credentials: AWS.config.credentials
      }); 
      this.s3 = new S3(
        {
          region: this._awsCognitoUtil._REGION //
        }
      );

    this.cookieService.set('expires', new Date().getTime().toString());

    this.selectedFiles = event.target.files;

    const file = this.selectedFiles.item(0);

    var _this = this;
    var msg = "";

    const params = {
      Bucket: "dcaportalfiles",
      Key: 'dcamappingfiles/' + file.name.toString(),
      Body: file
    };

    this.isFileUploading = true;

    this._progressbar = document.getElementById('fileprogressbar') as HTMLElement;
    _this.alert.nativeElement.classList.add('show');
    this.s3.upload(params).on('httpUploadProgress', function (evt) {
      console.log(evt.loaded + ' of ' + evt.total + ' Bytes');
      var percentage = (evt.loaded / evt.total) * 100;
      _this._LoadingPercentage = Math.round(percentage).toString();
      _this._progressbar.style.width = percentage.toString() + "%";


    }).send(function (err, data) {
      if (err) {
        console.log('There was an error uploading your file: ', err.message);
        _this.msgStr = 'There was an error uploading your file: ' + err.message;
        _this.isFileUploading = false;
        return false;
      } else {
        _this.isFileUploading = false;
        _this.BulkInsert(event);
        _this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>File has been uploaded successfully</b></strong>  ";
        let itemToReturn = new Observable();
        const sequence = _this.uploadService.listofFiles();

        sequence.subscribe(function (data) {
          _this._htmlstring = data
          _this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Please wait File is being read now </b> <span class='spinner-border spinner-border-sm'   role='status'  aria-hidden='true' ></span></strong>  ";


        });

      }
      return true;
    });


  })
  .catch(err => console.log(err)); 
      //-------



  }

  selectFile(event) {

    this.selectedFiles = event.target.files;

    this.fileChangeListener(event);

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
    this.cookieService.set('expires', new Date().getTime().toString());
  }

   toDate(dateStr) {
    var parts = dateStr.split("-")
    return new Date(parts[2], parts[1] - 1, parts[0])
  }
  //https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/DynamoDB.html
  InsertRecordIntoTable(FileData: CSVRecord[]) {
    var _this = this;
    this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b> Total " + FileData.length.toString() + " to be processed now</b></strong>  ";

    for (let i = 0; i <= FileData.length; i++) {

      var today = new Date();
      var todaydate = today.getFullYear().toString() + (today.getMonth() + 1).toString() + today.getDate().toString();
      var time = today.getHours().toString() + today.getMinutes().toString() + today.getSeconds().toString() + today.getMilliseconds().toString();

      console.log(FileData[i].AssignedDate)
      console.log(FileData[i].ExpiryDate)

      var assignDateArr = FileData[i].AssignedDate.split('-');
      var expiryDateArr = FileData[i].ExpiryDate.split('-');
      
      var assignDate = new Date( assignDateArr[2] +'/' + assignDateArr[1] +'/' + assignDateArr[0] )
      var expiryDate = new Date(expiryDateArr[2] +'/' + expiryDateArr[1] +'/' + expiryDateArr[0])

      var params = {
        RequestItems: {
          "dca_ces9_assignment": [
            {
              PutRequest: {
                Item: {
                  "Id": { N: todaydate.toString() + time.toString() },
                  "Agency": {
                    S: FileData[i].AgencyName
                  },
                  "Account_Number": {
                    N: FileData[i].AccountNumber.trim().toString()
                  },
                  "Assigned_Date": { S: this.datePipe.transform(assignDate, "dd-MMM-yy")  },
                  "Expiry_Date": { S: this.datePipe.transform(expiryDate, "dd-MMM-yy")},
                  "InsertDate": { S: FileData[i].InsertDate },
                  "InsertedBy": { S: FileData[i].InsertedBy },
                  "ModifiedBy": { S: FileData[i].ModifiedBy },
                  "ModifyDate": { S: FileData[i].ModifyDate }

                }
              }
            }
          ]
        }
      };
      var dynamodb = new DynamoDB({
        endpoint: dynamodbConfig.endpoint,
        region: dynamodbConfig.defaultRegion
      });


      

      dynamodb.batchWriteItem(params, function (err, data) {
        if (err) {
          console.log(err.message);
          //console.log(err.message, err.stack); 
        }// an error occurred
        else {
          console.log("record -" + todaydate.toString() + time.toString() + " saved in dynamodb");
          if (i == FileData.length) {
            _this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b></b></strong>  " + JSON.stringify(data);
            //_this.readItem();
          }

        }      // successful response

      });
    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  readFileFromS3(filename: string, folder: string) {
    var _this = this;
    const dynamoS3Bucket = new S3();
    var params = { Bucket: 'dcaportalfiles', Key: folder + filename };

    const stream = dynamoS3Bucket.getObject({ Bucket: 'dcaportalfiles', Key: folder + filename })


    dynamoS3Bucket.getObject(params, function (error, data) {
      var attachment = data.Body.toString('base64');

      _this.output = window.atob(attachment);

      var blob = new Blob([_this.b64DecodeUnicode(attachment)], { type: "text/plain;charset=utf-8" });
      saveAs(blob, "..//assets/uploads//CES9.csv");
    });



    this.cookieService.set('expires', new Date().getTime().toString());

  }

  b64DecodeUnicode(str): string {
    // Going backwards: from bytestream, to percent-encoding, to original string.
    return decodeURIComponent(atob(str).split('').map(function (c) {
      return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));
  }

  isCSVFile(file: any) {

    return file.name.endsWith(".csv");

  }

  isTXTFile(file: any) {

    return file.name.endsWith(".txt");

  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = csvRecordsArr[0].split(',');
    let headerArray = [];

    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }

  getDataRecordsArrayFromCSVFile(csvRecordsArray: any, headerLength: any): any[] {
    var dataArr = []
    this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Assignment  is being loaded into database now</b></strong>  ";
    for (let i = 1; i < csvRecordsArray.length; i++) {
      let data = csvRecordsArray[i].split(',');
      // FOR EACH ROW IN CSV FILE IF THE NUMBER OF COLUMNS         
      // ARE SAME AS NUMBER OF HEADER COLUMNS THEN PARSE THE DATA        
      var now = new Date();
      if (data.length == headerLength) {
        var csvRecord: CSVRecord = new CSVRecord();
        csvRecord.AccountNumber = data[0].trim();
        csvRecord.AssignedDate = data[1].trim();
        csvRecord.ExpiryDate = data[2].trim();
        csvRecord.AgencyName = data[3].trim();
        csvRecord.InsertDate = now.toISOString();
        var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));
    
        csvRecord.InsertedBy = decryptedUserId; //localStorage.getItem('userid');
        csvRecord.ModifiedBy = decryptedUserId;//localStorage.getItem('userid');
        csvRecord.ModifyDate = now.toISOString();
        dataArr.push(csvRecord);
      }
    }
    return dataArr;
  }

  fileChangeListener($event: any): void {
    var text = [];
    var files = $event.srcElement.files;

    if (this.isCSVFile(files[0]) || this.isTXTFile(files[0])) {
      var input = $event.target;
      var reader = new FileReader();
      reader.readAsText(input.files[0]);


      reader.onload = (data) => {
        let csvData = reader.result.toString();
        let csvRecordsArray = csvData.split(/\r\n|\n/);
        let headersRow = this.getHeaderArray(csvRecordsArray);
        let csvRecords =
          this.getDataRecordsArrayFromCSVFile(csvRecordsArray,
            headersRow.length);
        this.InsertRecordIntoTable(csvRecords)


      }

      reader.onerror = function () {
        alert('Unable to read ' + input.files[0]);
      };
    } else {
      alert('Please import valid .csv file.');
      // this.fileReset();      
    }
  }

  readItem() {
    var _this = this;
    // Create DynamoDB service object
    var dynamodb = new DynamoDB({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
    });

    var params = {
      TableName: 'dca_ces9_assignment'
    };

    dynamodb.scan(params, function (err, data) {
      if (err) {
        console.log("Error", err.message);
      } else {

        _this.loadData(data);

        // data.Items.forEach(function (element, index, array) {
        // let itemAssignment = new CSVRecord();
        // itemAssignment.AccountNumber = element.Account_Number.N;
        // itemAssignment.AssignedDate =  element.Assigned_Date.S;
        // itemAssignment.ExpiryDate =  element.Expiry_Date.S;
        // itemAssignment.AgencyName=  element.Agency.S;
        // itemAssignment.ModifyDate = element.ModifyDate.S;

        // _this._ReadAssignmentData.push(itemAssignment);
        // });


        // /// searching 
        // _this.tableService.setDataSource(_this._ReadAssignmentData);
        // _this._ReadAssignmentData = _this.tableService.getDataSource();
        // _this.previous = _this.tableService.getDataSource();

      }
    });

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  searchItems() {

    const prev = this.tableService.getDataSource();

    console.log(this.searchText);

    if (!this.searchText) {
      this.tableService.setDataSource(this.previous);
      this._ReadAssignmentData = this.tableService.getDataSource();
    }

    if (this.searchText) {
      console.log('Searching...');
      this._ReadAssignmentData = this.tableService.searchLocalDataBy(this.searchText);
      console.log(this._ReadAssignmentData)
      this.tableService.setDataSource(prev);
    }

    this.cookieService.set('expires', new Date().getTime().toString());

  }

  updateAssignmentItem(Id: string, accountNumber: string, agencyCode: string, Assigned_Date: string, Expiry_Date: string, item: string = "") {

    var now = new Date();


    Expiry_Date = this.datePipe.transform(Expiry_Date, "dd-MMM-yy")

    Assigned_Date = this.datePipe.transform(Assigned_Date, "dd-MMM-yy")

    var isValidDate = false;

    if (this.datePipe.transform(Assigned_Date, 'dd-MMM-yy') < this.datePipe.transform(Expiry_Date, 'dd-MMM-yy')) { isValidDate = true } else { isValidDate = false }

    if (this.datePipe.transform(Expiry_Date, 'dd-MMM-yy') > this.datePipe.transform(Assigned_Date, 'dd-MMM-yy')) { isValidDate = true } else { isValidDate = false }

    console.log(Assigned_Date < this.datePipe.transform(Expiry_Date, 'dd-MMM-yy') ? true : false);

    console.log(Assigned_Date + "--" + Expiry_Date + "--" + isValidDate)

    if (isValidDate) {




      var _this = this;
      // Create DynamoDB service object
      var dynamodb = new DynamoDB({
        endpoint: dynamodbConfig.endpoint,
        region: dynamodbConfig.defaultRegion
      });

      var params = {
        ExpressionAttributeNames: {

          "#AD": "Assigned_Date",
          "#ED": "Expiry_Date",
          "#MD": "ModifyDate",
          "#MB": "ModifiedBy"
        },
        ExpressionAttributeValues: {
          ":ADV": {
            S: Assigned_Date
          },
          ":EDV": {
            S: Expiry_Date
          },
          ":MDV": {
            S: now.toISOString()
          },
          ":MBV": {
            S: localStorage.getItem('userid').toString()
          }
        },
        Key: {
          "Account_Number": {
            N: accountNumber
          },
          "Agency": {
            S: agencyCode
          }
        },
        ReturnValues: "ALL_NEW",
        TableName: "dca_ces9_assignment",
        UpdateExpression: "SET #AD = :ADV, #ED = :EDV, #MD = :MDV, #MB = :MBV"
      };
      dynamodb.updateItem(params, function (err, data) {
        if (err) {


          if (item == "expiryDate") {

            var expiryDateEl = document.getElementById("expiryDateUpdated_" + Id) as HTMLElement;
            expiryDateEl.style.display = "block"
            expiryDateEl.className = "far fa-times-circle"

            _this.ExpiryDateUpdateMsg = "Invalid Expiry Date " + err.message

          } else if (item == "assignDate") {

            var assignDateEl = document.getElementById("assignDateUpdated_" + Id) as HTMLElement;
            assignDateEl.style.display = "block"
            assignDateEl.className = "far fa-times-circle"

            _this.AssignDateUpdateMsg = " Invalid Assign Date " + err.message
          }

          console.log(err, err.stack); // an error occurred
        }
        else {
          console.log(data);           // successful response

          if (item == "expiryDate") {

            var expiryDateEl = document.getElementById("expiryDateUpdated_" + Id) as HTMLElement;
            expiryDateEl.style.display = "block"
            expiryDateEl.className = "far fa-check-circle"

            _this.ExpiryDateUpdateMsg = "Expiry Date updated"
            var expiryElement = <HTMLElement>document.getElementById(item + '_spn_' + Id);
            expiryElement.innerText = Expiry_Date;
            _this.ShowEditableField(item, 'read', Id);

          } else if (item == "assignDate") {

            var assignDateEl = document.getElementById("assignDateUpdated_" + Id) as HTMLElement;
            assignDateEl.style.display = "block"
            assignDateEl.className = "far fa-check-circle"
            var assignElement = <HTMLElement>document.getElementById(item + '_spn_' + Id);
            assignElement.innerText = Assigned_Date;

            _this.ShowEditableField(item, 'read', Id);
            _this.AssignDateUpdateMsg = "Assign Date updated"
          }

        }
        /*
        data = {
         Attributes: {
          "AlbumTitle": {
            S: "Louder Than Ever"
           }, 
          "Artist": {
            S: "Acme Band"
           }, 
          "SongTitle": {
            S: "Happy Day"
           }, 
          "Year": {
            N: "2015"
           }
         }
        }
        */
      });
    }
    else if (!isValidDate) {

      if (item == "expiryDate") {
        var expiryDateEl = document.getElementById("expiryDateUpdated_" + Id) as HTMLElement;
        expiryDateEl.style.display = "block"
        expiryDateEl.className = "far fa-times-circle"

        this.ExpiryDateUpdateMsg = "Invalid Expiry Date - Expiry Date must be greater than expiry date"

      } else if (item == "assignDate") {

        var assignDateEl = document.getElementById("assignDateUpdated_" + Id) as HTMLElement;
        assignDateEl.style.display = "block"
        assignDateEl.className = "far fa-times-circle"

        this.AssignDateUpdateMsg = " Invalid Assign Date - Assign Date must be lesser than expiry date"
      }


    } else if (Assigned_Date == "mm/dd/yyyy" || Expiry_Date == "mm/dd/yyyy") {

      if (item == "expiryDate") {

        var expiryDateEl = document.getElementById("expiryDateUpdated_" + Id) as HTMLElement;
        expiryDateEl.style.display = "block"
        this.ShowEditableField(item, 'read', Id);

      } else if (item == "assignDate") {

        var assignDateEl = document.getElementById("assignDateUpdated_" + Id) as HTMLElement;
        assignDateEl.style.display = "block"


        this.ShowEditableField(item, 'read', Id);

      }

    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  removeAssignmentItem(accountNumber: string, agencyCode: string, id: string = "") {
    var _this = this;

    var deleteAccountNumber = confirm("Are you sure you want to delete " + accountNumber + "?");

    if (deleteAccountNumber == true) {

      if (id.length > 0) {
        (document.getElementById("row_" + id) as HTMLElement).className = "fas fa-trash-alt"

      }


      var dynamodb = new DynamoDB({
        endpoint: dynamodbConfig.endpoint,
        region: dynamodbConfig.defaultRegion
      });


      var params = {
        Key: {
          "Account_Number": {
            N: accountNumber
          },
          "Agency": {
            S: agencyCode
          }
        },
        TableName: "dca_ces9_assignment"
      };
      dynamodb.deleteItem(params, function (err, data) {
        if (err) {
          console.log(err, err.stack); // an error occurred
        }
        else {

          console.log('Item deleted' + JSON.stringify(data))

          if (id.length > 0) {
            (document.getElementById("row_" + id) as HTMLElement).style.display = 'none';
            (document.getElementById("row_" + id) as HTMLElement).className = "fas fa-trash-alt"

          }
        }


        // successful response
        /*
        data = {
         ConsumedCapacity: {
          CapacityUnits: 1, 
          TableName: "Music"
         }
        }
        */
      });
      this.cookieService.set('expires', new Date().getTime().toString());
    }
  }


 


  FilterAssignmentItem(accountNumber: string = "", agencyCode: string = "") {

    
    this.alert.nativeElement.classList.add('show');

    var type = localStorage.getItem("type");
    var _agencycode = localStorage.getItem("agencycode");

    if (type == "Agent" || type == "Agency") {

      if (accountNumber.trim().length > 7)
        {
        agencyCode = _agencycode;
        }

    }

    this.showprogressbar = true;

    var _this = this;
    // Create DynamoDB service object
    var dynamodb = new DynamoDB({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
    });

    

    var params: any;
    if (accountNumber.length > 0 && agencyCode.length < 1) {

      this.msgStr ="Please input agency code"

      console.log("Account Number " + accountNumber.length)
      console.log("Agency Code " + agencyCode.length)
      
      console.log("scaning using Account number filter");
      return false;

      params = {
        ExpressionAttributeValues: {
          ":v1": {
            N: accountNumber
          }
        },
        FilterExpression: "Account_Number = :v1",
        TableName: "dca_ces9_assignment"
      };
      dynamodb.scan(params, function (err, data) {
        if (err) {
          _this.showprogressbar = false;
          console.log(err, err.stack);
        }
        else {
          console.log(data);
          _this.loadData(data);
          _this.showprogressbar = false;
        }           // successful response

      });

    } else if (accountNumber.length < 1 && agencyCode.length > 0) {

      console.log("Account Number " + accountNumber.length)
      console.log("Agency Code " + agencyCode.length)
      
      console.log("scaning using Agency code filter");

      this.msgStr = "Please input account number"
      
      return false;

      params = {
        ExpressionAttributeValues: {
          ":v1": {
            S: agencyCode
          }
        },
        FilterExpression: "Agency = :v1",
        TableName: "dca_ces9_assignment"
      };

      dynamodb.scan(params, function (err, data) {
        if (err) {
          _this.showprogressbar = false;
          console.log(err, err.stack);
        }// an error occurred
        else { _this.loadData(data); _this.showprogressbar = false;  }           // successful response

      });

    } else if (accountNumber.length > 0 && agencyCode.length > 0) {

      console.log("Account Number " + accountNumber.length)
      console.log("Agency Code " + agencyCode.length)
      
      console.log("Triggered Query for fast search");
      this.showprogressbar = true

      params = {
        ExpressionAttributeValues: {
          ":v1": {
            S: agencyCode
          }, ":v2": {
            N: accountNumber
          }
        },
        KeyConditionExpression: "Agency = :v1 AND Account_Number = :v2",
        TableName: "dca_ces9_assignment"
      };

      dynamodb.query(params, function (err, data) {
        if (err) {
          _this.showprogressbar = false
          console.log(err.message); // an error occurred
        }
        else {

        

          _this.loadData(data);
          _this.showprogressbar = false;
          _this.closeAlert();
        }           // successful response

      });
    }

    if (accountNumber.length == 0 && agencyCode.length == 0) {

      alert('Entire data can not be accessed due to heavy volume')
      _this.showprogressbar = false;
      _this.closeAlert();

    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  loadData(data: any) {

    this._ReadAssignmentData = [];
    var _this = this;
    var type = localStorage.getItem("type");

    data.Items.forEach(function (element, index, array) {
      let itemAssignment = new CSVRecord();
      itemAssignment.AccountNumber = element.Account_Number.N;
      itemAssignment.AssignedDate = element.Assigned_Date.S;
      itemAssignment.ExpiryDate = element.Expiry_Date.S;
      itemAssignment.AgencyName = element.Agency.S;
      itemAssignment.ModifyDate = element.ModifyDate.S;
      itemAssignment.ModifiedBy = element.ModifiedBy.S;
      itemAssignment.Id = element.Id.N;
      itemAssignment.InsertDate = element.InsertDate.S;
      itemAssignment.InsertedBy = element.InsertedBy.S;

      var isexpired = _this._objValidate.validateExpiryDate(element.Expiry_Date.S, _this.datePipe);

      if (type == "Agent" || type == "Agency") {
        if (isexpired) {
          _this._ReadAssignmentData.push(itemAssignment);
        }
      }else{

        _this._ReadAssignmentData.push(itemAssignment);
      }
    });
    

  

    if (_this._ReadAssignmentData.length == 0) {

       this.recordFoundMessage = "Record not found..."

    } else {
      this.recordFoundMessage = "";
     }

      
     
      
    

    /// searching 
    _this.tableService.setDataSource(_this._ReadAssignmentData);
    _this._ReadAssignmentData = _this.tableService.getDataSource();
    _this.previous = _this.tableService.getDataSource();
    this.cookieService.set('expires', new Date().getTime().toString());
    _this.showprogressbar = false;
  }

  ShowEditableField(val: string, type: stringType, id: string) {

    var userType = localStorage.getItem("type");

    if (userType == "BU") {

      if (type == 'edit') {
        var elementSpan = <HTMLElement>document.getElementById(val + '_spn_' + id);
        elementSpan.style.display = 'none';

        var elementtxt = <HTMLInputElement>document.getElementById(val + '_' + id);
        elementtxt.style.display = 'block';

        var elementCancel = <HTMLInputElement>document.getElementById(val + '_Cancel_' + id);
        elementCancel.style.display = 'block';



      } else if (type == 'read') {

        var elementSpan = <HTMLElement>document.getElementById(val + '_spn_' + id);
        elementSpan.style.display = 'block';

        var elementtxt = <HTMLInputElement>document.getElementById(val + '_' + id);
        elementtxt.style.display = 'none';

        var elementCancel = <HTMLInputElement>document.getElementById(val + '_Cancel_' + id);
        elementCancel.style.display = 'none';


      }
    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  changebackground(type: boolean, item: string, id: string) {

    if (item == "delete") {
      if (type) {
        (document.getElementById("delete_" + id) as HTMLElement).setAttribute("style", " border-radius: 50%; width: 30px; height: 30px; background:#FFE9EC;text-align: center;");
      } else {

        (document.getElementById("delete_" + id) as HTMLElement).setAttribute("style", " border-radius: 50%; width: 30px; height: 30px; background:#FFF;text-align: center;");
        //(document.getElementById("delete_" + id) as HTMLElement).removeAttribute("style");
      }



    }
  }

  bulkDelete()
  {

    var documentclient = new DynamoDB.DocumentClient({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
});

var itemsArray = [];

var item1 = {
    DeleteRequest : {
        Key : {
            'Agency' : 'BMS'    
        }
    }
};

itemsArray.push(item1);

var item2 = {
    DeleteRequest : {
        Key : {
            'Agency' : 'BSM'    
        }
    }
};

itemsArray.push(item2);

var params = {
    RequestItems : {
        'dca_ces9_assignment' : itemsArray
    }
};
documentclient.batchWrite(params, function(err, data) {
    if (err) {
        console.log('Batch delete unsuccessful ...');
        console.log(err, err.stack); // an error occurred
    } else {
        console.log('Batch delete successful ...');
        console.log(data); // successful response
    }

});

  }
  

  deleteAllItems() {
    
    var documentclient = new DynamoDB.DocumentClient({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
});

    var _this = this;
    var hashKey = "Account_Number";
    var rangeKey = null;
    var tableName = "dca_ces9_assignment";
    var scanParams = {
      TableName: tableName,
    };
    
    documentclient.scan(scanParams, function(err, data) {
      if (err) console.log(err); // an error occurred
      else {
         data.Items.forEach(function(obj,i){
             console.log(i);
              console.log(obj);
              var params = {
                  TableName: scanParams.TableName,
                  Key:{"Account_Number":obj["Account_Number"],"Agency":obj["Agency"]} ,//_this.buildKey(obj,hashKey,rangeKey),
                  ReturnValues: 'NONE', // optional (NONE | ALL_OLD)
                  ReturnConsumedCapacity: 'NONE', // optional (NONE | TOTAL | INDEXES)
                  ReturnItemCollectionMetrics: 'NONE', // optional (NONE | SIZE)
              };
      
              documentclient.delete(params, function(err, data) {
                  if (err) console.log(err); // an error occurred
                  else console.log(data); // successful response
              });
          
         });
      }
  });
  
  

  }

  

  BulkInsert($event) {
    
    

    var _this = this;
    var documentclient = new DynamoDB.DocumentClient({
      endpoint: dynamodbConfig.endpoint,
      region: dynamodbConfig.defaultRegion
});

    var text = [];
    var files = $event.srcElement.files;

    if (this.isCSVFile(files[0]) || this.isTXTFile(files[0])) {
      var input = $event.target;
      var reader = new FileReader();
      reader.readAsText(input.files[0]);
    
      var split_arrays = [],
        size = 25;
      reader.onload = (data) => {
        let csvData = reader.result.toString();
        let csvRecordsArray = csvData.split(/\r\n|\n/);
        let headersRow = this.getHeaderArray(csvRecordsArray);
        let csvRecords =
          this.getDataRecordsArrayFromCSVFile(csvRecordsArray,
            headersRow.length);
        
            for(var i=0; i<csvRecords.length; i+=25){
              var sliced = csvRecords.slice(i, i + 25);
              split_arrays.push(sliced);
             // console.log(sliced);
            }
        
      // if( csvRecords.length > 0)
      //   {
      //       split_arrays.push(csvRecords.slice(0, size));
        
      //     }
      
        console.log(split_arrays.length);
        if (split_arrays.length > 4000) {
          alert('More than 100K records can not be loaded via DCA portal');
          return false;
        }

        var data_imported = false;
        var chunk_no = 1;
        var DYNAMODB_TABLENAME = "dca_ces9_assignment";

        split_arrays.forEach(function (element, index, array) {

          setTimeout( () => { 

          const params = {
            RequestItems: {}
          };
          params.RequestItems[DYNAMODB_TABLENAME] = [];

         // console.log(element);

          element.forEach(item => {
            // for (let key of Object.keys(item)) {
            //   // An AttributeValue may not contain an empty string
            //   if (item[key] === '')
            //     delete item[key];
            // }
            
            var today = new Date();
            var todaydate = today.getFullYear().toString() + (today.getMonth() + 1).toString() + today.getDate().toString();
            var time = today.getHours().toString() + today.getMinutes().toString() + today.getSeconds().toString() + today.getMilliseconds().toString();
    
            // console.log(item.AssignedDate)
            // console.log(item.ExpiryDate)
    
            var assignDateArr = item.AssignedDate.split('-');
            var expiryDateArr = item.ExpiryDate.split('-');
          
            var assignDate = new Date(assignDateArr[2] + '/' + assignDateArr[1] + '/' + assignDateArr[0])
            var expiryDate = new Date(expiryDateArr[2] + '/' + expiryDateArr[1] + '/' + expiryDateArr[0])

          

            params.RequestItems[DYNAMODB_TABLENAME].push({
              PutRequest: {
                Item: {
                  "Id": { N: todaydate.toString() + time.toString() },
                  "Agency": {
                    S: item.AgencyName
                  },
                  "Account_Number": {
                    N: item.AccountNumber.trim().toString()
                  },
                  "Assigned_Date": { S: _this.datePipe.transform(assignDate, "dd-MMM-yy") },
                  "Expiry_Date": { S: _this.datePipe.transform(expiryDate, "dd-MMM-yy") },
                  "InsertDate": { S: item.InsertDate },
                  "InsertedBy": { S: item.InsertedBy },
                  "ModifiedBy": { S: item.ModifiedBy },
                  "ModifyDate": { S: item.ModifyDate }

                }
              }
            });
           // console.log(DYNAMODB_TABLENAME + '=> ' +item.AccountNumber.trim().toString() + '--' + item.AgencyName);

          });


          var dynamodb = new DynamoDB({
            endpoint: dynamodbConfig.endpoint,
            region: dynamodbConfig.defaultRegion
          });


          dynamodb.batchWriteItem(params, function (err, data) {
           // console.log('done going next');
            if (err == null) {
             // console.log('Success chunk #' + chunk_no);
              data_imported = true;
            } else {
              console.log(err.message);
              console.log('Fail chunk #' + chunk_no);
              data_imported = false;
              return false;
            }

            _this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Record is being loaded now</b></strong> chunk " + chunk_no  + " loaded into system ( Total " + split_arrays.length + " chunks to be loaded) <hr> Do not close tab or browser otherwise you may lose data";
            
            if (chunk_no == split_arrays.length) {
              
              _this.showprogressbar = false;              
              _this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>File has been loaded into system successfully </b></strong>  ";
    
              let FileUploader = <HTMLInputElement>document.getElementById('txtFileUploader');
              FileUploader.value = "";

            }

            chunk_no++;


            
            
          
          });
    
        }, 2000 ); 
        });
      
     

      }


    }
  }
  







}

class campaignlogs{

   "loggeduser": string;
    "projectname": string;
    "eventname": string;
    "username": string;
    "roletype": string;
    "startdate": string;
    "enddate": string;
    "isactive": string;
    "desc": string;
    "eventtime": string;
    "clientip": string;
    "published": string
}
