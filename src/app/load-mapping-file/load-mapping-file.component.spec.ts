import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoadMappingFileComponent } from './load-mapping-file.component';

describe('LoadMappingFileComponent', () => {
  let component: LoadMappingFileComponent;
  let fixture: ComponentFixture<LoadMappingFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadMappingFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoadMappingFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
