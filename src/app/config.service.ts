import { Injectable } from '@angular/core';
import { config } from "../config";

@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private cfg = config;

  constructor() { }  

  getConfig() {
    return this.cfg;
  }
}
