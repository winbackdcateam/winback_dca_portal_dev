import { Component, OnInit,HostListener,ElementRef } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FilterPipe } from 'ngx-filter-pipe';
import { AppComponent } from '../app.component';

@Component({
  selector: 'app-error-page',
  templateUrl: './error-page.component.html',
  styleUrls: ['./error-page.component.scss']
})
export class ErrorPageComponent implements OnInit {

  users: any[] = [{ name: 'John' }, { name: 'Jane' }, { name: 'Mario' }];
  userFilter: any;


  @HostListener('input') oninput() {

    this.userFilter = this.el.nativeElement.querySelector('#userFilter').value;
   console.log(this.userFilter)
  }


message : string;
param1: string;
param2: string;
  constructor(private route: ActivatedRoute, private filterPipe: FilterPipe, private el: ElementRef,
    public myapp: AppComponent) {
    this.route.queryParams.subscribe(params => {
      this.param1 = params['msg'];

      if (this.param1 == "mobile") {
        this.message = "DCA Portal can not be accessed over mobile and tablet.Please try on laptop or desktop.."
        

      }
     
      console.log(params);
    });
    
  

   }

  ngOnInit() {

     this.myapp.userIdle.onTimerStart().subscribe(count => {

       this.myapp.stopWatching();

    });


    this.route.paramMap.subscribe(params => {
      

      if(params.get("msg")=="mobile")
      {
        alert(this.message);
      }
    })
  }




}
