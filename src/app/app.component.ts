import { Component, OnInit, AfterViewInit, HostListener,ChangeDetectorRef, NgZone,enableProdMode } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import { LocationStrategy } from '@angular/common';
import { CognitoUserPool } from "amazon-cognito-identity-js";
import * as AWS from "aws-sdk/global";
import { CookieService } from 'ngx-cookie-service';
import * as awsservice from "aws-sdk/lib/service";
import * as CognitoIdentity from "aws-sdk/clients/cognitoidentity";
import { awsCognitoUtil, SessionUserIdle, globals, EncrDecrService} from './_models';
import { Observable, Subject, Subscription, BehaviorSubject } from 'rxjs';
import { IdleTimeoutServiceService } from './idle-timeout-service.service';
import { UserIdleService } from 'angular-user-idle';
import { Auth } from 'aws-amplify';
import { Router, ActivatedRoute } from '@angular/router';
import { ApplicationStateServiceService } from './application-state-service.service';
import { ClipboardService } from 'ngx-clipboard';
import { FsService } from 'ngx-fs';
import { environment } from 'src/environments/environment';
import { ConfigService } from 'src/app/config.service';
import { HttpClient } from '@angular/common/http';

import * as AWSGlobal from "aws-sdk/global";

import SriPlugin from 'webpack-subresource-integrity';
import { logawses } from './_models/logawses';
 


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit, AfterViewInit {
  htmlSnippet: string = "<script>safeCode()</script>";
  title = 'Astro DCA Portal';
  user: any;
  _username: string;
  display = false;
  _timecountdown: string;
  _idleTimerSubscription: IdleTimeoutServiceService;
  public showtimeoutpopup: boolean = false;
  _session = new SessionUserIdle(this.router);
  _signouttype: string = "signout";
  cookieValue = 'UNKNOWN';
  sessionTimeLimit = new globals();
  sessionKey: any = sessionStorage.getItem("sessionkey");
  ipAddress:any;
  

  @HostListener('click', ['$event']) onclick(event: Event) {
    
    /*  console.profile();
    console.profileEnd();
    if (console.clear) console.clear(); */
    
    this.getIPInfo(); 

    if (localStorage.length == 0) 
    {
      this._signoutobj.signout();

    }else{

    this.validatelogintimeDate();
    }
    
  }

   @HostListener('mouseover') onMouseOver() {

  /*   console.profile();
    console.profileEnd();
    if (console.clear) console.clear(); */
    
  }
  
  @HostListener('document:keypress', ['$event']) onkeypress(event: KeyboardEvent) {
    
    if (event.keyCode == 44 || event.keyCode == 123) { this.copy('Oops! print screen is disabled') }
    
  }

  @HostListener('document:keyup', ['$event'])
  onKeyUp(event: KeyboardEvent) {
   
    var _this = this;
    
    if (event.keyCode == 44 || event.keyCode == 123) {
     
      this.copy('Oops! print screen is disabled')
     
    }
  }

  


  constructor(private amplifyService: AmplifyService, public userIdle: UserIdleService, private location: LocationStrategy, private cookieService: CookieService, private _clipboardService: ClipboardService,
    private cdr: ChangeDetectorRef, private http: HttpClient, private zone: NgZone, private EncrDecr: EncrDecrService, configService: ConfigService, private checkState: ApplicationStateServiceService, private router: Router, private route: ActivatedRoute, private _AwsCognitoUtil: awsCognitoUtil, private _fsService: FsService) {

      

      if(this.sessionKey == null)
        {
          this.sessionKey = localStorage.getItem("localsession");
          //alert(this.sessionKey);
        }

      
    if (!this.checkState.getIsMobileResolution()) {

      console.log('route to error page');
      this.router.navigateByUrl('/error?msg=mobile');
  
    } else {
   
      
      if (localStorage.length > 0) {
        var userid = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
        
        if (sessionStorage.getItem("sessionkey") == null) {
          sessionStorage.setItem("sessionkey", localStorage.getItem('lsk'));
        }
      }
      if (userid == "") { }
    
      window.onbeforeunload = function (e) {
        //alert('this one is working');
      };
    
      history.pushState(null, null, window.location.href);
      this.location.onPopState(() => {
        history.pushState(null, null, window.location.href);
      });


     
      
      
      Auth.configure({
        Auth: {
          // REQUIRED - Amazon Cognito Identity Pool ID
          identityPoolId: this._AwsCognitoUtil._IDENTITY_POOL_ID,
          // REQUIRED - Amazon Cognito Region
          region: this._AwsCognitoUtil._REGION,
          // OPTIONAL - Amazon Cognito User Pool ID
          userPoolId: this._AwsCognitoUtil.USER_POOL_ID,
          // OPTIONAL - Amazon Cognito Web Client ID
          userPoolWebClientId: this._AwsCognitoUtil._CLIENT_ID
        }
      });

      

        
      


      

      this.amplifyService.authStateChange$
        .subscribe(authState => {

          if (!authState.user) {
            this.router.navigateByUrl('/auth');
          } else {
            // console.log("user name - " + authState.user.username);
            this.user = authState.user;
            this._username = this.user.username;

            const cookieValue: string = this.cookieService.get('expires');
            var res = Math.abs(new Date().getTime() - parseInt(cookieValue)) / 1000;
            var min = Math.floor(res / 60) % 60;
            //  console.log('currentitme -' + new Date().getTime() + "| cokkie time - " + cookieValue)
            //  console.log("Difference (Minutes): " + min);
            if (min > 20 || isNaN(min) == true) {
              this._signoutobj.signout(this._username);
              //   console.log("Cookie expired : " + min);

            }


            

          }
        });

       

    
    }


    Auth.currentSession()
    .then(data => {
      console.log("reload - checking current session is valid")
      
      

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: this._AwsCognitoUtil._IDENTITY_POOL_ID,
        Logins: {
          'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': data.getIdToken().getJwtToken()
        }
      });
  
      AWS.config.update({
        region: 'ap-southeast-1',
        credentials: AWS.config.credentials
      }); 
  
      AWSGlobal.config.update({region: 'ap-southeast-1',
      credentials: AWS.config.credentials});

    });
  }


  getIPInfo() {
    
      
  this.http.get<{ip:string}>('https://api.ipify.org/?format=json')
  .subscribe( data => {
   
    this.ipAddress = JSON.stringify(data.ip);

    sessionStorage.setItem("ClientIP",this.ipAddress + " - " + this.getBrowserName());

  })
  }

  ngOnInit() {


    if (!this.checkState.getIsMobileResolution()) {

      //console.log('route to error page');

      this.router.navigateByUrl('/error?msg=mobile');

    } else {

      this.CheckPageIdle();
    }
  }

  ngAfterViewInit() {
    this.CheckPageIdle();
    this.cdr.detectChanges();
  }

  
  CheckPageIdle() {

    var _this = this;

    //console.log("session is being checked ")
    //Start watching for user inactivity.
    this.userIdle.startWatching();

    // Start watching when user idle is starting.
    this.userIdle.onTimerStart().subscribe(count => {
      this.showtimeoutpopup = true;
      this._timecountdown = String(this.sessionTimeLimit.sessionTimeout - count);
    });

    // Start watch when time is up.
    this.userIdle.onTimeout().subscribe(() => {

      this.showtimeoutpopup = false;
      console.log('Time is up!' + _this.signout())

    });


  }


  _signoutobj = new SessionUserIdle(this.router);
  signout() {

    this.showtimeoutpopup = false;
    this._signouttype = "sessionexpired"
    var userid = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
    //console.log("Session key -" + this.sessionKey +" stored key -" +localStorage.getItem("userid") + " user id -" + userid)
    this._signoutobj.signout(userid);

  }

  public stop() {
    this.userIdle.stopTimer();
    this.showtimeoutpopup = false;
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  public stopWatching() {
    this.showtimeoutpopup = false;
    this.userIdle.stopWatching();
    this.cookieService.set('expires', new Date().getTime().toString());
  }


  ForceToStop() {
    this.userIdle.stopTimer();
    this.showtimeoutpopup = false;
    this.CheckPageIdle();
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  restart() {
    this.userIdle.resetTimer();
  }

  disableTimer() {
    
    this.userIdle.stopTimer();
    this.showtimeoutpopup = false;
  }

  setAllCookies() {

    let dateTime = new Date()

    this.cookieService.set('expires', new Date().getTime().toString());

    
  }


  copy(text: string) {
    this._clipboardService.copyFromContent(text)
    
  }



  logawsesobj = new logawses();
  validatelogintimeDate(): boolean {

    var decryptedAgencyLoginTime = this.EncrDecr.get(this.sessionKey+'$#@$^@1ERFAGENCYTM',localStorage.getItem('agencylogintime') );
    
   //alert("From function session key -"+this.sessionKey);

    if (decryptedAgencyLoginTime != "00:00-00:00") {

    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));    

    
    //localStorage.clear();

    var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
      
    var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
  

    var arr = decryptedAgencyLoginTime.split('-')

    var a = arr[0] + ":00";
    var b = arr[1] + ":00";

    var aa1 = a.split(":");
    var aa2 = b.split(":");

    var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
    var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
    var dd1 = d1.valueOf();
    var dd2 = d2.valueOf();

    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var ttm = time.split(":");
    var todaytimepars = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(ttm[0], 10), parseInt(ttm[1], 10), parseInt(ttm[2], 10));
    var todaytime = todaytimepars.valueOf();

     console.log("Login Time -"+ decryptedAgencyLoginTime +" checking here starttime -" + dd1 + "endtime  -" + dd2 + "Today time - " + todaytime)
    if (todaytime < dd1 || todaytime > dd2) {

      alert("Your Login Time "+ decryptedAgencyLoginTime +" has been expired for the day");
    
     // this.alert.nativeElement.classList.add('show');
     // this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> you are only allowed to login during " + logintimestr;
      
    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", "Logged Out", "", "", decryptedUserId + " was forced to logged out due to odd time " + decryptedAgencyLoginTime , new Date().toISOString(),decryptedAgencyName,decryptedAgencyCode);
    console.log("Audit log saved"); 

              Auth.signOut({ global: true })
                .then(data => console.log(data))
                .catch(err => console.log(err));


    this.router.navigateByUrl("/auth?msg=loginexpired");
              
      
      return false;
    }
  }

    return true;

  }










  name = 'Angular';
  localIp = sessionStorage.getItem('LOCAL_IP');

  private ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);


  getClientIPAddress() {

     

  }
  private getRTCPeerConnection() {
    return window.RTCPeerConnection ||
      window.mozRTCPeerConnection ||
      window.webkitRTCPeerConnection;
  }

  getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }

  }
}

declare global {
  interface Window {
    RTCPeerConnection: RTCPeerConnection;
    mozRTCPeerConnection: RTCPeerConnection;
    webkitRTCPeerConnection: RTCPeerConnection;
  }




}
