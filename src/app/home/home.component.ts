import {
  Component, OnInit, EventEmitter, ElementRef, Output, Input,
  ViewChild, HostListener, AfterViewInit, ChangeDetectorRef,
  ChangeDetectionStrategy, DoCheck, OnChanges
} from '@angular/core';
import { CustomerInformation } from '../_models/customer';
import { Observable, Subscriber, throwError } from 'rxjs';
import { WinBack } from '../_models/winback';
import { BillInformation, BillDetails, IntrxnInformation, AccountInformation, AddressInformation, PaymentInformation, SMCInformation, CollectionBucket, FinancialInformation, DisconnectionDetails, FreePreview, FlashMessage, AccountMapping, EncrDecrService, awsCognitoUtil } from '../_models';
import { DataService } from '../data.service';
import { MdbTablePaginationComponent, MdbTableService, MdbTableDirective } from 'angular-bootstrap-md';
import { AmplifyService } from 'aws-amplify-angular';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

import { HeaderComponent } from '../header/header.component';
import { HttpClient } from '@angular/common/http';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpErrorResponse
} from '@angular/common/http';
import { Alert } from 'selenium-webdriver';
import { convertActionBinding } from '@angular/compiler/src/compiler_util/expression_converter';
import { ApplicationStateServiceService } from '../application-state-service.service';
import AWS from 'aws-sdk';
//import * as cdk from '@aws-cdk/aws-ssm';






@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewInit {

   @HostListener('click', ['$event']) onclick(event: Event) {
    
    console.profile(); 
    console.profileEnd(); 
    if (console.clear) console.clear();
     
    if (localStorage.length == 0) {      
      this.router.navigateByUrl('/auth');
      console.log("logging out");
     }
     
     
  }

 /*  @HostListener('mouseover') onMouseOver() { 
    console.profile(); 
    console.profileEnd(); 
    if (console.clear) console.clear();
    
  }  */

  @ViewChild(MdbTablePaginationComponent) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(HeaderComponent) objHeaderComp: HeaderComponent;


  @ViewChild('xdueamt') _DueAmtEl: ElementRef;
  @ViewChild('xdebtAge') _DebtAgeEl: ElementRef;
  @ViewChild('xinvoiceAge') _InvoiceAgeEl: ElementRef;
  @ViewChild('xbalance') _BalanceEl: ElementRef;
  

  showprogressbar: boolean = false;
  showbillprogressbar: boolean = false;
  showdisconnectprogressbar: boolean = false;
  showfreepreviewprogressbar = false;
  showflashmsgprogressbar = false;


  searchInteractionText: any;
  searchDisconnectionText: any
  searchPaymentText: any
  searchFinanceText: any


isInteractionDetailsLoading : boolean = false;
isDisconnectionDetailsLoading : boolean = false;
isBillingDetailsLoading : boolean = false;
isFreePreviewMsgLoading : boolean = false;
isFlashMsgLoading : boolean = false;


  customers$: CustomerInformation[];
  account$: AccountInformation[] = [];
  address$: AddressInformation[] = [];
  payment$: PaymentInformation[] = [];
  smc$: SMCInformation[] = [];
  collection$: CollectionBucket[] = [];
  finance$: FinancialInformation[] = [];
  intrxn$: IntrxnInformation[] = [];
  disconnectiondetails$: DisconnectionDetails[] = [];
  freepreview$: FreePreview[] = [];
  flashmsg$: FlashMessage[] = [];
  _isLoading: boolean;//=  this._state.loadngflg;
  _ShowTitle: boolean;
  _Curr_charges: string;
  _accountNumber: string;
  _smcNumber: string;
  _invoiceNumber: string;
  _payBydate: string;
  _ar_balance: string;
  _PasDueAmount: string;
  _DebtAge: string;
  _Invoice_Age: string;
  _Bill_Cycle: string;
  _Bill_Freq: string;
  _BillType: any;
  _smccnt: number;
  _creditclass: string;
  _accstatus: string;
  _isDataFound: boolean = false;
  isErrored: boolean = false;

    
  sessionKey :string = sessionStorage.getItem("sessionkey");

  pmtheadElements: any = ['Method', 'Deposit Date', 'Post Date', 'Amount', 'Source Id', 'Cheque No.', 'Bank Transaction Id', 'REV RSN'];
  finheadElements: any = ['Post Date', 'Description', 'Transaction Type', 'Amount', 'User Id', ' Transaction Code'];
  billheadElements: any = ['Previous Month Balance', 'Payment', 'OverDue Charges', 'New Charges', 'Total Amount Due'];
  billdetailsSMCElements: any = ['Charge Code', 'Description', 'Period', 'Amount', 'GST', 'Total']
  headDisconnectionElements: any = ['SMC', 'Order Type', 'Date']
  headFreepreviewElements: any = ['SMC', 'Status', 'Channel Name', 'Channel Code', 'Start Date', 'End Date']
  headFlashMsgElements: any = ['Title', 'Start Date', 'End Date', 'Status', 'Text']
  headInterationElements: any = ['START_TIME', 'Title', 'Media', 'Direction', 'Text'];
  headElements: any = ['SMC', 'STATUS'];
  headtestElements: any = ['ID', 'First', 'Last', 'Handle'];


  previous: any = [];
  elements: any = [];
  
  interationelements: IntrxnInformation[] = [];

  billinfromation: BillInformation[] = [];
  billinformationitem: BillInformation[] = [];
  billdetails: BillDetails[] = [];
  billsmcdetails: BillDetails[] = [];
  billdetailsitem: BillDetails[] = [];


  mapping: AccountMapping[];
  _MappedAccount: AccountMapping[];
  _username: string;
  user: any;
  firstItemIndex;
  lastItemIndex;

  @HostListener('input') oninput() {

    try{
    this.searchInteractionText = this.el.nativeElement.querySelector('#txtSearchInteraction').value;
    
    this.searchDisconnectionText = this.el.nativeElement.querySelector('#txtSearchDisconnection').value;

    this.searchPaymentText = this.el.nativeElement.querySelector('#txtSearchPayment').value;

    this.searchFinanceText = this.el.nativeElement.querySelector('#txtSearchFinance').value;
   
  }catch(er){}

  }


  constructor(private _data: DataService, private tableService: MdbTableService, private  _AwsCognitoUtil: awsCognitoUtil,
    private http: HttpClient, private cookieService: CookieService,private el: ElementRef,
    private _state: ApplicationStateServiceService, private cdRef: ChangeDetectorRef,
    private amplifyService: AmplifyService, private router: Router, private EncrDecr : EncrDecrService) {

      

    


    this.amplifyService.authStateChange$
      .subscribe(authState => {

        if (!authState.user) {
          this.router.navigateByUrl('/auth');
        } else {
          // console.log("Status - " + authState.user);
          this.user = authState.user;
          this._username = this.user.username;
          this.objHeaderComp._username = this._username;
        }
      });

    this.getJSON().subscribe(data => {
      this.mapping = data["Mapping"];
      // console.log("called -"+this.mapping);


    });


  }

  ngOnInit() {
  
    var decryptedUserId = this.EncrDecr.get(this.sessionKey +'$#@$^@1ERFUSRID', localStorage.getItem('userid'));
    this._username = decryptedUserId;
    
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  ngAfterViewInit() {

    this.objHeaderComp.flg = true;    

  }

  ngOnChanges() {
    if (this.isErrored) {this._isDataFound = false; }
    console.log("CHANGES")
  }
  /* ngDoCheck() {
    if (this.isErrored) {this._isDataFound = false; }
    console.log("DO CHECK")
  } */


  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  onNextDisconnectPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  onPreviousDisconnectPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //----------------------------------------
  //load mapping
  //---------------------------------------
  public getJSON(): Observable<any> {
    return this.http.get("../../assets/accountmapping.json");
  }

  //-------------------------
  // loading winback data
  //------------------------ 

  onClicked(value: WinBack) {

    
    var accountnumber = this.objHeaderComp._account;

    if (this.objHeaderComp.isErrorFound)
    {      
      return false;
    } 

    this.interationelements = [];
    this.billinfromation = [];
    this.billinformationitem = [];
    this.billdetails = [];
    this.billsmcdetails = [];
    this.billdetailsitem = [];
    this.collection$ = [];
    this.finance$ = [];
    this.intrxn$ = [];
    this.disconnectiondetails$ = [];
    this.freepreview$ = [];
    this.flashmsg$ = [];




    this.customers$ = value.customerInformation;
    this.account$ = value.accountInformation;
    this.address$ = value.addressInformation;
    this.payment$ = value.paymentInformation;
    this.smc$ = value.smcInformation;
    this.finance$ = value.financialInformation;
    this.collection$ = value.collectionBucket;
    this._smccnt = this.smc$.length;

    var objPymt = JSON.stringify(this.payment$);   

    var objfinance = JSON.stringify(this.finance$);    

    if (this.customers$.length == 1) {
      this._isDataFound = true;
      this.objHeaderComp._accountholdername = this.customers$.find(x => x.Account_ID == accountnumber).Full_name;    
    }
    else if (this.customers$.length == 0){
      this._isDataFound = false;
      this.objHeaderComp._accountholdername = "";
      this.objHeaderComp._accountStatus = "";
      alert('No record exists')

    }

    for (let itemAccount of this.account$) {
      this._DebtAge = itemAccount.DebtAge;
      this._Invoice_Age = itemAccount.Invoice_Age;
      this._Bill_Cycle = itemAccount.BILL_CYCLE;
      this._Bill_Freq = itemAccount.Bill_Freq;
      this._accountNumber = itemAccount.ACCOUNT_ID;
      this.objHeaderComp._accountStatus = itemAccount.AccStatus;
      this._creditclass = itemAccount.CREDIT_CLASS;
      this._accstatus = itemAccount.AccStatus;
      this._BillType = itemAccount.Bill_Type;

    }

    for (let itemCollection of value.collectionBucket) {
      this._PasDueAmount = itemCollection.PasDueAmount;
      this._ar_balance = itemCollection.ar_balance;
      this._Curr_charges = itemCollection.Curr_charges;
    }


   

   setTimeout(() => {
      this.HighlightRule();
    }, 1000);

   
  }


  HighlightRule() {

    if (parseInt(this._DebtAge) > 30 && parseInt(this._PasDueAmount) > 10 && this._creditclass == 'HI') {

      this._DueAmtEl.nativeElement.style.backgroundColor = "red";
      this._InvoiceAgeEl.nativeElement.style.backgroundColor = "red";
      this._DebtAgeEl.nativeElement.style.backgroundColor = "red";
      this._BalanceEl.nativeElement.style.backgroundColor = "red";

      this._DueAmtEl.nativeElement.style.color = "white";
      this._InvoiceAgeEl.nativeElement.style.color = "white";
      this._DebtAgeEl.nativeElement.style.color = "white";
      this._BalanceEl.nativeElement.style.color = "white";

    } else if (parseInt(this._DebtAge) > 45 && parseInt(this._PasDueAmount) > 20 && this._creditclass == 'ME') {

      this._DueAmtEl.nativeElement.style.backgroundColor = "red";
      this._InvoiceAgeEl.nativeElement.style.backgroundColor = "red";
      this._DebtAgeEl.nativeElement.style.backgroundColor = "red";
      this._BalanceEl.nativeElement.style.backgroundColor = "red";

      this._DueAmtEl.nativeElement.style.color = "white";
      this._InvoiceAgeEl.nativeElement.style.color = "white";
      this._DebtAgeEl.nativeElement.style.color = "white";
      this._BalanceEl.nativeElement.style.color = "white";

    } else if (parseInt(this._DebtAge) > 45 && parseInt(this._PasDueAmount) > 20 && this._creditclass == 'LO') {
      this._DueAmtEl.nativeElement.style.backgroundColor = "red";
      this._InvoiceAgeEl.nativeElement.style.backgroundColor = "red";
      this._DebtAgeEl.nativeElement.style.backgroundColor = "red";
      this._BalanceEl.nativeElement.style.backgroundColor = "red";

      this._DueAmtEl.nativeElement.style.color = "white";
      this._InvoiceAgeEl.nativeElement.style.color = "white";
      this._DebtAgeEl.nativeElement.style.color = "white";
      this._BalanceEl.nativeElement.style.color = "white";
    } else {

      this._DueAmtEl.nativeElement.style.backgroundColor = "white";
      this._InvoiceAgeEl.nativeElement.style.backgroundColor = "white";
      this._DebtAgeEl.nativeElement.style.backgroundColor = "white";
      this._BalanceEl.nativeElement.style.backgroundColor = "white";

      this._DueAmtEl.nativeElement.style.color = "black";
      this._InvoiceAgeEl.nativeElement.style.color = "black";
      this._DebtAgeEl.nativeElement.style.color = "black";
      this._BalanceEl.nativeElement.style.color = "black";
    }

  }

  
  //-------------------------
  // loading interaction data
  //------------------------ 
  LoadInteractionData(isReloadRequired :boolean = false) {

   
    
  var XID = this._accountNumber;
   if (this.interationelements.length < 1 || isReloadRequired) {
     this.showprogressbar = true;
     this.isInteractionDetailsLoading  = true;


      return this._data.requestInteractionDetails(XID).subscribe(
        (data: any[]) => {
          try
          {

            this.interationelements = data[0]["body"]["IntrxnInformation"];

            var obj = JSON.stringify(this.interationelements)

            this.showprogressbar = false;

            this.isInteractionDetailsLoading = false;
         
          } catch (e) {
            this.isInteractionDetailsLoading = false;
            this.showprogressbar = false;
            console.log(e.message);
            console.log(data);
          }
         
        });
    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //-------------------------
  // loading bill details
  //------------------------ 
  hyperelement: HTMLElement;
  divmenu: HTMLElement;
  LoadBillDetailsData(XID: string, Seq: string, InvoiceNumber: string, Paybydate: string) {

    this._invoiceNumber = InvoiceNumber;
    this._payBydate = Paybydate;
    this.showbillprogressbar = true;
    this.divmenu = document.getElementById("invdtmenu") as HTMLElement;
    var menuitem = this.divmenu.querySelectorAll('a');
    menuitem.forEach(function (mItem) {
      mItem.className = "list-group-item list-group-item-action flex-column align-items-start";
    });




    return this._data.requestBillDetails(XID, Seq).subscribe(
      (data: any[]) => {

        try {

          this.billdetails = data[0]["body"]["BillDetails"];

          var obj = JSON.stringify(this.billdetails)

          this.showbillprogressbar = false;

          if (this.billdetails.length > 0) {

            this.billdetailsitem = this.billdetails.filter((item) => item.SMC == "");


            this.billsmcdetails = this.billdetails.filter((item) => item.SMC != "");

            this.billsmcdetails.forEach(obj => {
              this._smcNumber = obj.SMC
            });
          }


          if (this.billinfromation.length > 0) {

            this.billinformationitem = this.billinfromation.filter((item) => item.CYCLE_SEQ_NO == Seq);
          }

          this.hyperelement = document.getElementById(Seq) as HTMLElement;
          this.hyperelement.className = "list-group-item list-group-item-action list-group-item-info flex-column align-items-start";
        } catch (e) {
         
          console.log(e.message);
          console.log(data);

        }

      });
    
      this.cookieService.set('expires', new Date().getTime().toString());
  }

  //-------------------------
  // loading bill information
  //------------------------ 
  LoadBillInformationData(isReloadRequired :boolean = false) {

    var XID = this._accountNumber;

    if (this.billinfromation.length < 1 || isReloadRequired) {

      this.showbillprogressbar = true;

      
      this.isBillingDetailsLoading = true;
     

      return this._data.requestBillInformation(XID).subscribe(
        (data: any[]) => {

          try{

          this.billinfromation = data[0]["body"]["BillInformation"];


          var obj = JSON.stringify(this.billinfromation)
          this.showbillprogressbar = false;
         


          for (let item of this.billinfromation) {
            this.LoadBillDetailsData(item.BA_ACCOUNT_NO, item.CYCLE_SEQ_NO, item.InvoiceNumber, item.PayBy);
            try {
              this.hyperelement = document.getElementById(item.CYCLE_SEQ_NO) as HTMLElement;
              this.hyperelement.className = "list-group-item list-group-item-action list-group-item-info flex-column align-items-start";
            } catch (ex) { } finally {
              
              this.isBillingDetailsLoading = false;
            }

            break;
            }

          } catch (e) {
            this.showbillprogressbar = false;
            this.isBillingDetailsLoading = false;
            console.log(e.message);
            console.log(data); }

        });
    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }


  //-------------------------
  // loading disconnection information
  //------------------------ 
  LoadDisconnectionInfoData(isReloadRequired :boolean = false) {

    var XID = this._accountNumber;

    if (this.disconnectiondetails$.length < 1 || isReloadRequired) {

      this.showdisconnectprogressbar = true;
  this.isDisconnectionDetailsLoading  = true;


      return this._data.requestDisconnectionDetails(XID).subscribe(
        (data: any[]) => {

          try{
          this.disconnectiondetails$ = data[0]["body"]["DisconnectInformation"];

          var obj = JSON.stringify(this.disconnectiondetails$)

          this.showdisconnectprogressbar = false;       
          } catch (e) {
          this.isDisconnectionDetailsLoading = false;
          this.showdisconnectprogressbar = false;
          console.log(e.message);
          console.log(data); 
          } finally {
            
            this.isDisconnectionDetailsLoading = false;
        }

        });
    }

    this.cookieService.set('expires', new Date().getTime().toString());

  }


  //-------------------------
  // loading free preview information
  //------------------------ 

  LoadFreePreviewInfoData(isReloadRequired :boolean = false) {

    var XID = this._accountNumber;
  

    if (this.freepreview$.length < 1 || isReloadRequired) {

      this.showfreepreviewprogressbar = true;
      this.isFreePreviewMsgLoading = true;

      return this._data.requestFreePreviewDetails(XID).subscribe(
        (data: any[]) => {

          try { 

          this.freepreview$ = data[0]["body"]["FreepreviewInformation"];
          var cnt = 0;
          this.showfreepreviewprogressbar = false;

          if (Object.keys(this.freepreview$).length === 0) {
            console.log("No properties")
          }

          var obj = JSON.stringify(this.freepreview$)
          } catch (e) {
          this.showfreepreviewprogressbar = false;
          this.isFreePreviewMsgLoading = false;
          console.log(e.message);
            console.log(data); 
            
          } finally { this.isFreePreviewMsgLoading = false; }
        });

    }

    this.cookieService.set('expires', new Date().getTime().toString());
   // this.logDAta("Free Preview Info : " + XID);
  }

  //-------------------------
  // loading flash message information
  //------------------------ 
  LoadFlashMessageData(isReloadRequired :boolean = false) {

    var XID = this._accountNumber;
    if (this.flashmsg$.length < 1 || isReloadRequired) {

      this.showflashmsgprogressbar = true;

      this.isFlashMsgLoading = true;
      
      return this._data.requestFlashDetails(XID).subscribe(
        (data: any[]) => {

          try
          {

          this.flashmsg$ = data[0]["body"]["FlashDetails"];

          var obj = JSON.stringify(this.flashmsg$)
          this.showflashmsgprogressbar = false;
          } catch (e) {
          this.isFlashMsgLoading = false;
            this.showflashmsgprogressbar = false;
            console.log(e.message);
            console.log(data); 
          } finally { this.isFlashMsgLoading = false; }

        });
    }
    this.cookieService.set('expires', new Date().getTime().toString());
   // this.logDAta("Flash Message : " + XID);
  }


  expandelement: HTMLElement;
  expand(eid: string) {
    this.expandelement = document.getElementById(eid) as HTMLElement;
    var classstr = this.expandelement.className;

    if (classstr.match(/minus/)) {

      this.expandelement.className = "far fa-plus-square";
    } else {

      this.expandelement.className = "far fa-minus-square";
    }

  }


  // getAccountInfoColor(flg: string) {
  //   switch (flg) {
  //     case 'Active':
  //       return 'green';
  //     case 'InActive':
  //       return 'red';
  //     default: {
  //       return 'white';
  //     }
  //   }
  // }


  // getCollectionBucketColor(flg: string) {
  //   switch (flg) {
  //       case 'LO':
  //       return 'green';
  //        case 'ME':
  //       return 'yellow';       
  //     default: {
  //       return 'red';
  //     }
  //   }
  // }

  getCollectionBucketColor(flg: string) {
    switch (flg) {
      case 'LO':
        return 'badge badge-success';
      case 'ME':
        return 'badge badge-warning';
      default: {
        return 'badge badge-danger';
      }
    }
  }

  getAccountInfoColor(flg: string) {
    switch (flg) {
      case 'Active':
        return 'badge badge-success';
      case 'InActive':
        return 'badge badge-danger';
      default: {
        return 'badge badge-secondary';
      }
    }
  }


}
