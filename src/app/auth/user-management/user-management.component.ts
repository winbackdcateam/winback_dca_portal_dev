import {
  Component, AfterViewInit, Directive, OnInit, ViewChild, HostListener,
  Renderer, ElementRef, ChangeDetectorRef, OnChanges, Input
} from '@angular/core';
import { Users, Common, PasswordValidator, validate, awsCognitoUtil, SessionUserIdle, globals, EncrDecrService } from '../../_models';
import { HeaderComponent } from '../../header/header.component';
import { MdbTableService, MdbTablePaginationComponent, MdbTableDirective } from 'angular-bootstrap-md';
import { FormsModule, FormControl, FormGroup, Validators } from '@angular/forms';
import { Auth } from 'aws-amplify';
import { AmplifyService } from 'aws-amplify-angular';
import { CognitoUserPool } from "amazon-cognito-identity-js";

import * as AWS from "aws-sdk/global";
import { CookieService } from 'ngx-cookie-service';
import * as awsservice from "aws-sdk/lib/service";
import * as CognitoIdentity from "aws-sdk/clients/cognitoidentity";
import { CognitoIdentityServiceProvider } from 'aws-sdk/clients/all';
import { resolve } from 'path';
import { reject, async } from 'q';
import { DatePipe } from '@angular/common';
import { forEach } from '@angular/router/src/utils/collection';
import { stringList } from 'aws-sdk/clients/datapipeline';
import { logawses } from 'src/app/_models/logawses';
import { Router } from '@angular/router';




let usersDetailsList: Array<Users> = [];

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})


export class UserManagementComponent implements OnInit, AfterViewInit {

  @HostListener('click', ['$event']) onclick(event: Event) {

    /* console.profile();
    console.profileEnd();
    if (console.clear) console.clear(); */

    if (localStorage.length == 0) {
      this._signoutobj.signout();

    }

  }

  /* @HostListener('mouseover') onMouseOver() {
    if (localStorage.length == 0) {
      this._signoutobj.signout();

    }
    console.profile();
    console.profileEnd();
    if (console.clear) console.clear();
  }  */

  @ViewChild(MdbTablePaginationComponent) mdbTablePagination: MdbTablePaginationComponent;
  @ViewChild(HeaderComponent) objHeaderComp: HeaderComponent;
  @ViewChild('xuid') _uid: ElementRef;
  @ViewChild('xpwd') _pwd: ElementRef;
  @ViewChild('alert') alert: ElementRef;
  @ViewChild('lblUserName') _lblUserName: ElementRef;
  @ViewChild('emaildiv') _Email: ElementRef;
  @ViewChild('xEmpIdDiv') _empId: ElementRef;
  @ViewChild('xAgencyNameDiv') _xAgencyNameDiv: ElementRef;
  @ViewChild('xAgencyCodeDiv') _xAgencyCodeDiv: ElementRef;
  @ViewChild('xAgentLimit') _xAgentLimit: ElementRef;
  @ViewChild('AgencyFilterVisible') _AgencyFilter: ElementRef;

  public cognitoCreds: AWS.CognitoIdentityCredentials;
  public _cognitoidentity: CognitoIdentity;
  public isStateChanged: boolean;
  IsUserTypeVisible: boolean = true;
  IsEmailAddressVisible: boolean = true;
  IsAgencyCodeVisible: boolean = true;
  IsAgencyNameVisible: boolean = true;
  IsEmployeeIdVisible: boolean = true;
  AllowToDisplay: boolean = true;
  msgPanel: HTMLElement = document.getElementById('msgpanel') as HTMLElement;
  emailField: HTMLElement = document.getElementById('divemailid') as HTMLElement;
  validatingForm: FormGroup;
  _getdate = new Common();
  previous: any = [];
  addingUserType = "User";
  sessionKey : string  = sessionStorage.getItem("sessionkey");
  firstItemIndex;
  lastItemIndex;
  public exportAuditLogRecord: Array<Users> = [];

  usertype = [{
    id: '8f8c6e98',
    name: 'Admin',
    code: 'BU'
  },
  {
    id: '3953154c',
    name: 'Super Admin',
    code: 'SA'
  }]

  loginTimeMsg: string
  AgentLimitUpdateMsg: string
  msgResend: any;
  agencyFullNameUpdateMsg: string;
  agencyCodeUpdateMsg: string;
  public objValidator = new validate();
  usersList: Users[];
  headElements = ['User Type', 'Agency Code', 'Agency Name', 'Agent Limit', 'Employee Id', 'Full Name', 'User Name', 'E-mail', 'Login Time', 'Verified', 'Status'];
  columncount: number;
  public usersdetails: Array<Users> = [];
  public Agencydetails: Array<any> = [];
  public AgencyAdminDetails : Array<Users> = [];
  public Agentdetails: Array<Users> = [];
  searchText: string = '';
  usersdata: AWS.Request<CognitoIdentityServiceProvider.ListUsersResponse, AWS.AWSError>;
  _username: string;
  ShowVerifyPnl: boolean = false;
  showalert: boolean = false;
  msgStr: string;
  showprogressbar: boolean;
  isshowprogressbar: boolean;
  enableadduserbutton: boolean = false
  enableverifycodebutton: boolean = false
  LimitMsg: string;
  limitOfAgent: Number;
  totalEnabledAgent: Number;
  somePlaceholder: string = "Full Name Input";
  loggedInUserType: string;



  _userTypeValid: boolean = false;
  _IsAgencyNameValid: boolean = false;
  _IsAgencyFullNameValid: boolean = false;
  _IsAgencyCodeValid: boolean = false;
  _IsAgentEmpIdValid: boolean = false;
  _userFullnameValid: boolean = false;
  _userLoginIdalid: boolean = false;
  _userPasswordValid: boolean = false;
  _userEmailValid: boolean = false;
  _userExpiryDate: boolean = false;
  _verifycodeValid: boolean = false;
  _IsAgentLimitValid: boolean = false;
  _searchQuery: string;

  addTaskValue = null;
  cookieValue = 'UNKNOWN';
   decryptedUserId : string = this.EncrDecr.get(this.sessionKey +  '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
   decryptedUserType : string = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));


   @HostListener('input') oninput() {

    this.searchText = this.el.nativeElement.querySelector('#search').value;
   // this.searchItems();
    this.closeAlert();
  }
 
  clear() {


    var inputs = document.querySelectorAll('#divform input');
    for (let i = 0; i < inputs.length; i++) {
      var c = inputs[i] as HTMLInputElement;
      c.value = "";
    }

    //console.log("clear form");


  }

  constructor(private amplifyService: AmplifyService,  private _globals: globals,private router: Router,
    private renderer: Renderer, private datepipe: DatePipe, private _AwsCognitoUtil: awsCognitoUtil, 
    private logawsesobj: logawses,
    private EncrDecr: EncrDecrService , private cookieService: CookieService, private el: ElementRef, 
    private cdRef: ChangeDetectorRef) {

    this.cookieService.set('expires', new Date().getTime().toString());

    //private tableService: MdbTableService,

  }


  uidelement: HTMLInputElement;
  pwdelement: HTMLInputElement;
  _signoutobj = new SessionUserIdle(this.router);

  ngOnInit() { }

  ngAfterViewInit() {

    /* this.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
//this.mdbTablePagination.lastVisibleItemIndex = 5;
//this.mdbTablePagination.lastItemIndex = 5;

this.firstItemIndex = this.mdbTablePagination.firstItemIndex;
this.lastItemIndex = this.mdbTablePagination.lastItemIndex;

this.mdbTablePagination.calculateFirstItemIndex();
this.mdbTablePagination.calculateLastItemIndex();
this.cdRef.detectChanges();
 */
    
    this.columncount = this.headElements.length;
   
    var type = this.decryptedUserType ;
    this.loggedInUserType = type;
    var userid = this.decryptedUserId;

    //console.log("User type" + type);
    

    if (type == "Agent") {

      this._empId.nativeElement.style.display = 'none';
      this._xAgentLimit.nativeElement.style.display = 'none';
      this._xAgencyCodeDiv.nativeElement.style.display = 'none';
      this._xAgencyNameDiv.nativeElement.style.display = 'none';
      this._AgencyFilter.nativeElement.style.display = 'none';
      this._signoutobj.signout(userid);

    } else if (type == "BU")
    {
     // console.log("applied BU");
      this.IsUserTypeVisible = false;
      this.IsEmployeeIdVisible = false;
      this._empId.nativeElement.style.display = 'none';
      this.IsAgencyCodeVisible = true;
      this.IsAgencyNameVisible = true;

      this._xAgencyCodeDiv.nativeElement.style.display = 'block';
      this._xAgencyNameDiv.nativeElement.style.display = 'block';
      this._xAgentLimit.nativeElement.style.display = 'block';


    } else if (type == "Agency") {

      this._AgencyFilter.nativeElement.style.display = 'none';
      this.IsUserTypeVisible = false;
      this.IsEmployeeIdVisible = true;
      this.IsAgencyCodeVisible = false;
      this.IsAgencyNameVisible = false;
      this._Email.nativeElement.style.display = 'none';
      this._xAgentLimit.nativeElement.style.display = 'none';
      this._xAgencyCodeDiv.nativeElement.style.display = 'none';
      this._xAgencyNameDiv.nativeElement.style.display = 'none';
      this._empId.nativeElement.style.display = 'block';

    } else if (type == "SA" ||  type == "admin") {
      this.IsUserTypeVisible = true;
      this.IsEmployeeIdVisible = false;
      this.IsAgencyCodeVisible = false;
      this.IsAgencyNameVisible = false;
      this._empId.nativeElement.style.display = 'none';
      this._xAgentLimit.nativeElement.style.display = 'none';
      this._xAgencyCodeDiv.nativeElement.style.display = 'none';
      this._xAgencyNameDiv.nativeElement.style.display = 'none';
    } else
    {
      
      this.AllowToDisplay = false;
    }

    setTimeout(() => { 
      this.clear(); 
      this.refreshpage();}, 2000);

    this.cdRef.detectChanges();

  }



  isUserInvalid() {

    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
    var type = decryptedUserType;
    //console.log(type);
    if (type == 'SA' || type == 'admin') {
      this.LimitMsg = "";
      if (this._userTypeValid && this._userEmailValid
        && this._userFullnameValid && this._userLoginIdalid) {


        return true;
      } else {



        return false;
      }

    } else if (type == 'BU') {

      this.LimitMsg = "";
      this.addingUserType = "Agency"
      this.somePlaceholder = "Agency PIC Name Input"

      

      if (this._userEmailValid && this._IsAgentLimitValid
        && this._userFullnameValid && this._userLoginIdalid && this._IsAgencyFullNameValid
        && this._IsAgencyCodeValid) {
        return true;
      } else {
        return false;
      }

    } else if (type == 'Agency') {

      this.addingUserType = "Agent"

      

      if (this._userFullnameValid && this._userLoginIdalid && this._IsAgentEmpIdValid) {
        return true;
      } else {
        return false;
      }



    }

    this.cookieService.set('expires', new Date().getTime().toString());

  }

  isCodeInvalid() {

    if (this._verifycodeValid) {
      return true;

    } else {

      return false;
    }

  }

  UserTypeChanged() {

    var usertype = this.el.nativeElement.querySelector('#selectType').value

    if (usertype.trim() == "") {
      this._userTypeValid = false;

    } else {
      this._userTypeValid = true;

    }

  }

  AgencyNameChanged() {

    var selectAgencyName = this.el.nativeElement.querySelector('#selectAgencyName').value.toString();

    if (selectAgencyName.trim() == "") {
    } else {
      this.loadUserInformation("", "", selectAgencyName);
    }



  }


  validateRequiredFields(val: string, name: string, type: string) {

    

    if (!this.objValidator.RequiredFields(val)) {

      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " <strong><b>" + name + "&nbsp;</b></strong> is required. ";
      this.alert.nativeElement.style.display = 'block';

      if (type == "Full Name") {

        this._userFullnameValid = false;
       // console.log('Full name is not valid')

      } else if (type == "Verification code") {

        this._verifycodeValid = false;

      }
      else if (type == "Employee Id") {

        this._IsAgentEmpIdValid = false;
      } else if (type == "Agency Code") {

        this._IsAgencyCodeValid = false;
       // console.log('Agency Code is not valid')
      }
      else if (type == "Agency Name") {

        this._IsAgencyFullNameValid = false;

       // console.log('Agency name is not valid')
      }
      else if (type == "Agent Limit") {
       // console.log('Agent Limit is not valid')
        this._IsAgentLimitValid = false;
      }
    }
    else {

      if (type == "Full Name") {

        this._userFullnameValid = true;

      } else if (type == "Verification code") {

        this._verifycodeValid = true;

      } else if (type == "Employee Id") {

        this._IsAgentEmpIdValid = true;
      } else if (type == "Agency Code") {

        this._IsAgencyCodeValid = true;
      }
      else if (type == "Agency Name") {


        this._IsAgencyFullNameValid = true;
      } else if (type == "Agent Limit") {

        if (this.objValidator.IsNumber(Number(val))) {
         // console.log('agent limit number is valid')

          this._IsAgentLimitValid = true;

        } else {
         // console.log('agent limit number is not valid' + val)
          this._IsAgentLimitValid = false;
          this.msgStr = " <strong><b>Agent Creation Limit must be numeric without decimal and positive &nbsp;</b></strong>";
        }
      }

      this.closeAlert();

    }

  }

  validateLoginIdFields(val: string) {

    if (!this.objValidator.validateLoginId(val)) {
      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " User <strong><b> Login Id</strong> is required. It must not contain any space ";
      this._userLoginIdalid = false;
    }
    else {
      this._userLoginIdalid = true;
      this.closeAlert();

    }

  }

  validatePasswordFields(val: string) {

    if (!this.objValidator.PasswordCheck(val)) {
      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " User <strong><b> Password </b></strong> is required. <hr> Password must contain at least 1 lowercase alphabetical character,1 uppercase alphabetical character,least 1 numeric character, one special character and 8 characters or longer ";
      this._userPasswordValid = false;
    }
    else {
      this._userPasswordValid = true;
      this.closeAlert();

    }

  }

  validateemailFields(val: string) {



    if (!this.objValidator.validateEmail(val)) {
      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " User <strong><b> Invalid  </b></strong> e-mail id";
      this._userEmailValid = false;
    }
    else {
      this._userEmailValid = true;
      this.closeAlert();

    }


  }




  loadUserInformation(UserId: string = "", AgencyName: string = "", AgencyCode: string = "") {

    // console.log("Local Storage - " + localStorage.length)

    var that = this;

    that.usersdetails = [];


    let _data = this.GetAllUserDetails();

    _data.then(function (resolveOutput) {

      that.usersdetails = [];

      var cognitouserdata = resolveOutput["Users"];
      var decryptedUserType = that.EncrDecr.get(that.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
      var type = decryptedUserType;
      //console.log(type);

      for (let i = 0; i < cognitouserdata.length; i++) {

        let userinfo = new Users();
        userinfo.Id = (i + 1).toString();
        userinfo.UserName = cognitouserdata[i].Username;
        userinfo.IsEnabled = cognitouserdata[i].Enabled;
        userinfo.DateOfCreation = cognitouserdata[i].UserCreateDate;
        userinfo.LastModifiedDate = cognitouserdata[i].UserLastModifiedDate;
        userinfo.UserStatus = cognitouserdata[i].UserStatus;

        for (var j = 0; j < cognitouserdata[i].Attributes.length; j++) {
          var otheratt = cognitouserdata[i].Attributes[j].Name;

          if (otheratt == "email") {

            userinfo.email = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "phone") {
            userinfo.Phone = cognitouserdata[i].Attributes[j].Value;
          }
          else if (otheratt == "custom:usertype") {
            userinfo.UserType = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:logintime") {
            userinfo.logintime = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:loginendtime") {
            userinfo.loginendtime = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:fullname") {
            userinfo.FullName = cognitouserdata[i].Attributes[j].Value;

          } else if (otheratt == "email_verified") {
            userinfo.is_email_verified = Boolean(cognitouserdata[i].Attributes[j].Value);

          } else if (otheratt == "custom:expirydate") {
            userinfo.expirydate = cognitouserdata[i].Attributes[j].Value;

          } else if (otheratt == "custom:agencyname") {
            userinfo.agencyname = cognitouserdata[i].Attributes[j].Value;

          } else if (otheratt == "custom:empid") {
            userinfo.agentId = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:agencycode") {
            userinfo.agencyCode = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:agencyfullname") {
            userinfo.agencyFullName = cognitouserdata[i].Attributes[j].Value;

          } else if (otheratt == "custom:agentlimit") {
            userinfo.agentLimit = cognitouserdata[i].Attributes[j].Value;
            var decryptedUserId = that.EncrDecr.get(that.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
            var loggedinuserid = decryptedUserId;
            if (type == 'Agency' && userinfo.UserName == loggedinuserid) {
             // console.log(UserId + ' - Agent Limit ' + userinfo.agentLimit)
              var encryptedLimitAgentCreation = that.EncrDecr.set(that.sessionKey+'$#@$^@1ERFLMTAGNT',userinfo.agentLimit );
              localStorage.setItem('limitAgentCreation', encryptedLimitAgentCreation);
            }
          }


        }




        if (type == 'Agency') {

          userinfo.isLoginTimeEditable = false;
          userinfo.isAgentLimitEditable = false;
        } else {
          userinfo.isAgentLimitEditable = true;
          userinfo.isLoginTimeEditable = true;
        }

        if (userinfo.UserType == "Agent") {
          userinfo.isLoginTimeEditable = false;
        }

        if (type == 'Agency' && userinfo.UserType == 'Agency') {

          userinfo.isDeactivtionValid = false;

        } else {
          userinfo.isDeactivtionValid = true;
        }

        if (userinfo.UserType == 'Agency' || userinfo.UserType == 'BU' || userinfo.UserType == 'Agent') {
          that.usersdetails.push(userinfo);

        }
      }

      if (UserId != "") {

        that.usersdetails = that.usersdetails.filter((item) => item.UserName == UserId)

      }

      if (AgencyName != "") {

        that.usersdetails = that.usersdetails.filter((item) => item.agencyname == AgencyName)

        that.totalEnabledAgent = Number(that.usersdetails.filter((enabledItem) => enabledItem.IsEnabled == true).length);
        var decryptedAgentCreation = that.EncrDecr.get(that.sessionKey+'$#@$^@1ERFLMTAGNT', localStorage.getItem("limitAgentCreation"));
        that.limitOfAgent = Number(decryptedAgentCreation);

        that.LimitMsg = "Note :- Your Active Agent Limit is " + that.limitOfAgent.toString() + ". Total Enabled Agent - " + that.totalEnabledAgent.toString() + ".";

      } else if (AgencyCode != "") {

        that.usersdetails = that.usersdetails.filter((item) => item.agencyCode == AgencyCode)
        that.totalEnabledAgent = Number(that.usersdetails.filter((enabledItem) => enabledItem.IsEnabled == true).length);
        var decryptedAgentCreationLimit = that.EncrDecr.get(that.sessionKey+'$#@$^@1ERFLMTAGNT', localStorage.getItem("limitAgentCreation"));
       
        that.limitOfAgent = Number(decryptedAgentCreationLimit);
        that.LimitMsg = "Note :- Your Active Agent Limit is " + that.limitOfAgent.toString() + ". Total Enabled Agent - " + that.totalEnabledAgent.toString() + ".";

      }


      if (UserId == "" && AgencyName == "" && AgencyCode == "") {


        that.usersdetails.filter((item) => item.UserType == 'Agency')
          .forEach((item) => {
            that.Agencydetails.push(item.agencyCode);
          })

        const unique = (value, index, self) => {
          return self.indexOf(value) === index
        }

        that.Agencydetails = that.Agencydetails.filter(unique);


      }

     /*  that.tableService.removeRow;
      that.tableService.setDataSource([]);
      that.firstItemIndex = 1;
      that.lastItemIndex = 5;
      that.previous = [];

      that.tableService.setDataSource(that.usersdetails);
      that.usersdetails = that.tableService.getDataSource();
      that.previous = that.tableService.getDataSource();

      that.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
      that.firstItemIndex = that.mdbTablePagination.firstItemIndex;
      that.lastItemIndex = that.mdbTablePagination.lastItemIndex;
      that.mdbTablePagination.calculateFirstItemIndex();
      that.mdbTablePagination.calculateLastItemIndex();
      that.cdRef.detectChanges();
 */
      that.showprogressbar = false;
      that.isshowprogressbar = true;

    }, function (rejectOutput) {
      console.log(rejectOutput);
    });



    that.objHeaderComp.flg = false;

    this.cookieService.set('expires', new Date().getTime().toString());

  }

  ///----------------------
  ///-- Search item of table here
  ///----------------------
  searchItems() {

    /* const prev = this.tableService.getDataSource();

    if (!this.searchText) {
      this.tableService.setDataSource(this.previous);
      this.usersdetails = this.tableService.getDataSource();
    }

    if (this.searchText) {
      this.usersdetails = this.tableService.searchLocalDataBy(this.searchText);
      this.tableService.setDataSource(prev);
    } */


    this.cookieService.set('expires', new Date().getTime().toString());
  }

  onNextPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  onPreviousPageClick(data: any) {
    this.firstItemIndex = data.first;
    this.lastItemIndex = data.last;

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //----------------------------------------------------------------
  // Sign up here 
  //---------------------------------------------------------------
  signUp(loginid: string, pwd: string, emailid: string, name: string, EmployeeId: string
    , agencyName: string, _AgencyCode: string, _AgentCreationLimit: string) {

  loginid = loginid.toLowerCase();
  
    var Signupflg = true;

    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem("userid"));
    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
    var decryptedEmailId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFEMAIL', localStorage.getItem("emailid"));
    
   
    pwd = "Test@123";

    var type = decryptedUserType;
    var userid = decryptedUserId;
    var email = decryptedEmailId;

    var _EmployeeId = "not required";
    var agentlimit = _AgentCreationLimit;
    var logintime = '9:00-20:00';
    var agencyfullname = "not required";
    var agencyCode = "not required";

    if (agentlimit == "0") { agentlimit = "30"; }

    var agencyname = "error";
    var usertype = "error";

    if (type == "BU") {

      var result = this.GetEnabledAgency(_AgencyCode, 'all');
         
      this.eventCaptured("Signup", "created agency admin -" + loginid + " for Agency - " + _AgencyCode);
      
      result.then(item => {
        
        if (item.length <  2) {

          agencyfullname = agencyName;
          agencyCode = _AgencyCode;
          agencyname = loginid;
          usertype = "Agency";

          this.addUsers(loginid, pwd, emailid, name, EmployeeId,
            true, agencyName, _AgencyCode, _AgentCreationLimit, usertype
            , logintime, agencyname, agencyCode, agencyfullname, _EmployeeId, agentlimit);
        } else {
          
          this.alert.nativeElement.style.display = 'block';
          this.alert.nativeElement.classList.add('show');
          this.msgStr = "More than two Agency Admins are not allowed of same agency for accesing DCA portal ";
          
          Signupflg = false;
         // console.log("Agency can not be created");

        }
     


      });
   
       

    }
    else if (type == "Agency") {

      var agencyfullname = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCY', localStorage.getItem("agentfullname"));
      var agencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
     
      this.eventCaptured("Signup", "added agent -" + loginid + " for Agency - " + agencyfullname +"(" + _AgencyCode)+")";
      agencyname = userid;
      usertype = "Agent";
      emailid = email;
      _EmployeeId = EmployeeId;


      if (this.limitOfAgent == this.totalEnabledAgent) {  //console.log("limt not macthing")
        Signupflg = false;
        this.alert.nativeElement.style.display = 'block';
        this.alert.nativeElement.classList.add('show');
        this.msgStr = "Agent can not be added because Agent creation limit has been over. <hr> For creating new agent disable existing agent";

      }else{
        this.addUsers(loginid, pwd, emailid, name, EmployeeId,
          true, agencyName, _AgencyCode, _AgentCreationLimit, usertype
          , logintime, agencyname, agencyCode, agencyfullname, _EmployeeId, agentlimit);

      }

    } if (type == "SA" || type == "admin" ) {
      logintime = "00:00-00:00";
      usertype = this.el.nativeElement.querySelector('#selectType').value;

      if (usertype == "SA") {
        agencyname = "Super Admin"

      } else if (usertype == "BU") {
        agencyname = "Admin"
        agentlimit = _AgentCreationLimit
      }



      this.addUsers(loginid, pwd, emailid, name, EmployeeId,
        true, agencyName, _AgencyCode, _AgentCreationLimit, usertype
        , logintime, agencyname, agencyCode, agencyfullname, _EmployeeId, agentlimit);
        

    }

  
  }


  addUsers(loginid: string, pwd: string, emailid: string, name: string, EmployeeId: string,
    Signupflg:boolean , agencyName: string, _AgencyCode: string, _AgentCreationLimit: string, usertype: string
  ,logintime: string,agencyname:string,agencyCode: string,agencyfullname:string , _EmployeeId:string,agentlimit: string) {
    
    if (Signupflg) {
      
      Auth.signUp({
          username: loginid,
          password: pwd,
          attributes: {
            email: emailid,
            "custom:fullname": name,
            "custom:usertype": usertype,
            "custom:logintime": logintime,
            "custom:expirydate": 'Not required',
            "custom:agencyname": agencyname,
            "custom:agencycode": agencyCode,
            "custom:agencyfullname": agencyfullname,
            "custom:empid": _EmployeeId,
            "custom:agentlimit": agentlimit,
            "custom:isfirsttimelogin": 'Y'
  
          },
          validationData: []  //optional
        })
          .then(data => {
  
            this._username = this._uid.nativeElement.value;
            this.alert.nativeElement.style.display = 'block';
            this.alert.nativeElement.classList.add('show');
            this.msgStr = " User <strong><b>" + loginid + "&nbsp;</b></strong> has been added successfully ,Confirmation will be required during first time login .Please check your mailbox for credentials";
             try {
               this._globals.sendTemporaryCredntials(loginid, pwd, emailid, "naveen_kumar@astro.com.my");
            } catch (ex) { console.log(ex.message) } 
            AWS.config.update({ region: 'ap-southeast-1' });
            this.loadUserInformation(this._username);
  
          })
          .catch(err => {
            AWS.config.update({ region: 'ap-southeast-1' });
            this.alert.nativeElement.style.display = 'block';
            this.alert.nativeElement.classList.add('show');
            this.msgStr = "User <strong><b>" + loginid + "&nbsp;</b></strong> has not been added " + err.message;
            console.log(err["message"])
  
          });
  
      }
  
      this.cookieService.set('expires', new Date().getTime().toString());
  }

  //----------------------------------------------
  // Verify code of user login
  //----------------------------------------------
  verfiyCode(username: string, code: string) {

    Auth.confirmSignUp(username, code, {
      forceAliasCreation: true
    }).then(data => {
      // console.log(data)
      this.ShowVerifyPnl = false;
      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = "User <strong><b>" + username + "&nbsp;</b></strong>  has been verified";
    })
      .catch(err => {
        console.log(err)
        this.alert.nativeElement.style.display = 'block';
        this.alert.nativeElement.classList.add('show');
        this.msgStr = " User <strong><b>" + username + "&nbsp;</b></strong>  has not been verified " + err;
      });


  }


  //-----------------update login time ----------------------------------
  _logintime: HTMLInputElement
  logintimeUserupdated: HTMLElement

  UpdateLoginTime(userid: string, usertype: string, agencyname: string, agencycode: string) {


    this.eventCaptured("Login Time", "Updated login time of Agnecy - " + agencyname +"("+ agencycode+")");

    if (usertype == 'Agency') {

      this.GetEnabledAgentCount(agencycode, 'all').then(result => {

        this.Agentdetails = result;

        for (let i = 0; i < result.length; i++) {

          this.updateAgentLoginTime(result[i].UserName, agencyname);

        }

      });
    }
    else {
      this.updateAgentLoginTime(userid);
    }
  }

  updateAgentLoginTime(userid: string, agencyid: string = "") {

    if (agencyid == "") {

      this._logintime = document.getElementById("logintimeUser_" + userid) as HTMLInputElement;


    } else {

      this._logintime = document.getElementById("logintimeUser_" + agencyid) as HTMLInputElement;


    }

    var _this = this;
    let timestr = this._logintime.value;
    console.log(' User id :- ' + userid + ' agency id :- ' + agencyid)

    if (this.validatelogintimeDate(timestr)) {


      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var params = {
        UserAttributes: [ /* required */
          {
            Name: 'custom:logintime', /* required */
            Value: timestr
          },
          /* more items */
        ],
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, //'us-east-1_rWWNFO1Xi', /* required */
        Username: userid /* required */
      };
      cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
        if (err) {

          if (agencyid == "") {
            _this.logintimeUserupdated = document.getElementById("logintimeUserupdated_" + userid) as HTMLElement;
            _this.logintimeUserupdated.className = "far fa-times-circle"
            _this.logintimeUserupdated.style.display = "block"
            _this.loginTimeMsg = err.message;
          } else {
            _this.logintimeUserupdated = document.getElementById("logintimeUserupdated_" + agencyid) as HTMLElement;
            _this.logintimeUserupdated.className = "far fa-times-circle"
            _this.logintimeUserupdated.style.display = "block"
            _this.loginTimeMsg = err.message;
          }

          this.cookieService.set('expires', new Date().getTime().toString());
          console.log(err, err.stack); // an error occurred
        }
        else {

          try {
            var spnLoginTime = document.getElementById("spnLoginTime_" + userid) as HTMLElement;
            spnLoginTime.innerText = timestr;
          } catch{ }

          if (agencyid == "") {
            _this.logintimeUserupdated = document.getElementById("logintimeUserupdated_" + userid) as HTMLElement;
            _this.logintimeUserupdated.className = "far fa-check-circle"
            _this.logintimeUserupdated.style.display = "block"
            _this.loginTimeMsg = "Time updated"
          } else {
            _this.logintimeUserupdated = document.getElementById("logintimeUserupdated_" + agencyid) as HTMLElement;
            _this.logintimeUserupdated.className = "far fa-check-circle"
            _this.logintimeUserupdated.style.display = "block"
            _this.loginTimeMsg = "Time updated"
          }

          _this.cookieService.set('expires', new Date().getTime().toString());

        }      // successful response
      });



    } else {

      this.cookieService.set('expires', new Date().getTime().toString());
      if (agencyid == "") {

        var logintimeUserupdated = document.getElementById("logintimeUserupdated_" + userid) as HTMLElement;

        logintimeUserupdated.style.display = "block"

        logintimeUserupdated.className = "far fa-times-circle"

        this.loginTimeMsg = "Invalid Format"

      } else {

        var logintimeUserupdated = document.getElementById("logintimeUserupdated_" + agencyid) as HTMLElement;


        logintimeUserupdated.style.display = "block"

        logintimeUserupdated.className = "far fa-times-circle"

        this.loginTimeMsg = "Invalid Format"

      }

    }


  }


  //--------------------------------------
  // update email id
  //-------------------------------------
  emailIdUpdateMsg: any;
  _emailid: HTMLInputElement
  _emailidUserupdated: HTMLElement
  //
  updateUsersEmailId(userid: string, usertype: string, agencyname: string, agencycode: string) {

    this.eventCaptured("Email Id", "Updated email id of Agency - " + agencyname +"("+ agencycode+")");

    if (usertype == 'Agency') {

      this.GetEnabledAgentCount(agencycode, 'all').then(result => {
        // Process result
        this.Agentdetails = result;

        for (let i = 0; i < result.length; i++) {

          this.updateEmailId(result[i].UserName, agencyname);

        }

      })

    } else {

      this.updateEmailId(userid);

    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }


  spnEmailId: HTMLElement;
  updateEmailId(userid: string, agencyid: string = "") {

    if (agencyid == "") {
      this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

      this._emailid = document.getElementById("useremailid_" + userid) as HTMLInputElement;

      this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;
    } else {

      this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

      this._emailid = document.getElementById("useremailid_" + agencyid) as HTMLInputElement;

      this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;

    }

    var _this = this;
    let strEmailId = this._emailid.value;

    if (this.objValidator.validateEmail(strEmailId)) {

      let user = Auth.currentAuthenticatedUser();

      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var params = {
        UserAttributes: [ /* required */
          {
            Name: 'email', /* required */
            Value: strEmailId
          }, {
            Name: 'email_verified',
            Value: 'true'
          }
          /* more items */
        ],
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, //'us-east-1_rWWNFO1Xi', /* required */
        Username: userid /* required */
      };
      cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
        if (err) {

          _this.emailIdUpdateMsg = err.message;
          console.log(err, err.stack); // an error occurred
        }
        else {
          _this.ShowEditableField('email', userid, 'read')

        }      // successful response
      });


      if (agencyid == "") {
        this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

        this._emailid = document.getElementById("useremailid_" + userid) as HTMLInputElement;

        this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;
      } else {

        this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

        this._emailid = document.getElementById("useremailid_" + agencyid) as HTMLInputElement;

        this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;

      }

      this.spnEmailId.innerText = strEmailId;

      _this._emailidUserupdated.style.display = "block"

      _this._emailidUserupdated.className = "far fa-check-circle"

      this.emailIdUpdateMsg = "Email Id updated"

    } else {


      if (agencyid == "") {
        this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

        this._emailid = document.getElementById("useremailid_" + userid) as HTMLInputElement;

        this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;
      } else {

        this.spnEmailId = document.getElementById("spnuseremailid_" + userid) as HTMLElement;

        this._emailid = document.getElementById("useremailid_" + agencyid) as HTMLInputElement;

        this._emailidUserupdated = document.getElementById("emailiduserupdated_" + userid) as HTMLElement;

      }


      _this._emailidUserupdated.style.display = "block"

      _this._emailidUserupdated.className = "far fa-times-circle"

      this.emailIdUpdateMsg = "Invalid Format"

    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }


  updateAgencyCode(userid: string, usertype: string, agencyname: string, agencycode: string) {

    this.eventCaptured("Agency Code", "Updated agency code of Agency - " + agencyname +"("+ agencycode+")");

    if (usertype == 'Agency') {

      var txtAgencyCode = document.getElementById("agencycode_" + agencyname) as HTMLInputElement;

      if (this.objValidator.RequiredFields(txtAgencyCode.value)) {

        this.GetEnabledAgentCount(agencycode, 'all').then(result => {
          // Process result
          this.Agentdetails = result;


          for (let i = 0; i < result.length; i++) {



            var _this = this;
            let strAgencyCode = txtAgencyCode.value;

            let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
            var params = {
              UserAttributes: [ /* required */
                {
                  Name: 'custom:agencycode', /* required */
                  Value: strAgencyCode
                }
              ],
              UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, //'us-east-1_rWWNFO1Xi', /* required */
              Username: result[i].UserName /* required */
            };
            cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
              if (err) {

                var spnAgencyCode = document.getElementById("spnagencycode_" + result[i].UserName) as HTMLElement;

                var msgAgencyCodeupdated = document.getElementById("agencycodeupdated_" + result[i].UserName) as HTMLElement;


                _this.agencyCodeUpdateMsg = err.message;

                msgAgencyCodeupdated.style.display = "block"

                msgAgencyCodeupdated.className = "far fa-times-circle"

                _this.showprogressbar = false;

                console.log(err, err.stack); // an error occurred
              }
              else {

                var spnAgencyCode = document.getElementById("spnagencycode_" + result[i].UserName) as HTMLElement;

                var msgAgencyCodeupdated = document.getElementById("agencycodeupdated_" + result[i].UserName) as HTMLElement;

                _this.ShowEditableField('agencycode', userid, 'read')

                spnAgencyCode.innerText = txtAgencyCode.value;

                msgAgencyCodeupdated.style.display = "block"

                msgAgencyCodeupdated.className = "far fa-check-circle"

                _this.agencyCodeUpdateMsg = "Agency Code updated"

              }      // successful response
            });



          }

        })
      }
      else {

        var spnAgencyCode = document.getElementById("spnagencycode_" + userid) as HTMLElement;

        var msgAgencyCodeupdated = document.getElementById("agencycodeupdated_" + userid) as HTMLElement;

        this.agencyCodeUpdateMsg = "Agency Code is required";

        msgAgencyCodeupdated.style.display = "block"

        msgAgencyCodeupdated.className = "far fa-times-circle"

        this.showprogressbar = false;
      }

    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }

  updateAgencyFullName(userid: string, usertype: string, agencyname: string, agencycode: string) {

    this.eventCaptured("Agency Name", "Updated agency name of Agency - " + agencyname +"("+ agencycode+")");

    if (usertype == 'Agency') {

      var txtAgencyFullName = document.getElementById("agencyfullname_" + agencyname) as HTMLInputElement;

      if (this.objValidator.RequiredFields(txtAgencyFullName.value)) {

        this.GetEnabledAgentCount(agencycode, 'all').then(result => {

          this.Agentdetails = result;

        
          for (let i = 0; i < result.length; i++) {



            var _this = this;
            let strAgencyFullName = txtAgencyFullName.value;

            let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
            var params = {
              UserAttributes: [ /* required */
                {
                  Name: 'custom:agencyfullname', /* required */
                  Value: strAgencyFullName
                }
              ],
              UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, //'us-east-1_rWWNFO1Xi', /* required */
              Username: result[i].UserName /* required */
            };
            cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
              if (err) {

                var spnAgencyFullName = document.getElementById("spnagencyfullname_" + result[i].UserName) as HTMLElement;

                var msgAgencyFullNameupdated = document.getElementById("agencyfullnameupdated_" + result[i].UserName) as HTMLElement;

                _this.agencyFullNameUpdateMsg = err.message;

                msgAgencyFullNameupdated.style.display = "block"

                msgAgencyFullNameupdated.className = "far fa-times-circle"

                _this.showprogressbar = false;

                console.log(err, err.stack); // an error occurred
              }
              else {

                var spnAgencyFullName = document.getElementById("spnagencyfullname_" + result[i].UserName) as HTMLElement;

                var msgAgencyFullNameupdated = document.getElementById("agencyfullnameupdated_" + result[i].UserName) as HTMLElement;

                _this.ShowEditableField('agencyfullname', userid, 'read')

                spnAgencyFullName.innerText = txtAgencyFullName.value;

                msgAgencyFullNameupdated.style.display = "block"

                msgAgencyFullNameupdated.className = "far fa-check-circle"

                _this.agencyFullNameUpdateMsg = "Agency Name updated"

              }      // successful response
            });



          }

        })

      }
    } else {

      var spnAgencyFullName = document.getElementById("spnagencyfullname_" + userid) as HTMLElement;

      var msgAgencyFullNameupdated = document.getElementById("agencyfullnameupdated_" + userid) as HTMLElement;

      this.agencyFullNameUpdateMsg = "Agency name is required";

      msgAgencyFullNameupdated.style.display = "block"

      msgAgencyFullNameupdated.className = "far fa-times-circle"

      this.showprogressbar = false;


    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //--------------------------------------
  // update email id
  //-------------------------------------
  FullNameUpdateMsg: any;
  _FullName: HTMLInputElement
  _FullNameUserupdated: HTMLElement

  updateUsersFullName(userid: string) {



    this._FullName = document.getElementById("userfullname_" + userid) as HTMLInputElement;

    this._FullNameUserupdated = document.getElementById("fullnameuserupdated_" + userid) as HTMLElement;


    var _this = this;
    let strFullName = this._FullName.value;

    if (this.objValidator.RequiredFields(strFullName)) {

      let user = Auth.currentAuthenticatedUser();

      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var params = {
        UserAttributes: [ /* required */
          {
            Name: 'custom:fullname', /* required */
            Value: strFullName
          },
          /* more items */
        ],
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,
        Username: userid /* required */
      };
      cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
        if (err) {

          _this._FullNameUserupdated.className = "far fa-times-circle"
          _this.FullNameUpdateMsg = err.message;
          console.log(err, err.stack); // an error occurred

        }
        else {
          console.log(data);
          _this.ShowEditableField('Fullname', userid, 'read')
        }      // successful response
      });

      _this._FullNameUserupdated.style.display = "block"

      _this._FullNameUserupdated.className = "far fa-check-circle"

      this.FullNameUpdateMsg = "Full Name updated"

      this.showprogressbar = false;

    } else {

      _this._FullNameUserupdated.style.display = "block"

      _this._FullNameUserupdated.className = "far fa-times-circle"

      this.FullNameUpdateMsg = "Invalid full name"

      this.showprogressbar = false;
    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //---------------------------------------
  // update Agent Limit
  //--------------------------------------

  updateAgentLimit(agencyCode: string, userid: string) {
    
    this.eventCaptured("Agent Limit", "Updated agent limit of Agency - " +  agencyCode);

    var _this = this;
    var elementAgentLimit = <HTMLInputElement>document.getElementById('agentLimit_' + userid);

    this.GetEnabledAgency(agencyCode,'all').then(result => { 

      console.log(result);
     
     
        result.forEach(function (item, index, array) {
        
         
          (async () => {

            await _this.updateAgentLimitOfAgency(item.UserName, elementAgentLimit.value);
        
         })();
        
        
        });
      
    });

   
    
   


  }


 async updateAgentLimitOfAgency(userid: string, elementAgentLimit: string ) {
   
 
   
  var agentLimitupdated = document.getElementById("agentLimitUpdated_" + userid) as HTMLElement;


    var _this = this;
    let strAgentLimit = parseInt(elementAgentLimit);

    if (this.objValidator.IsNumber(Number(strAgentLimit))) {

     

      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var params = {
        UserAttributes: [ /* required */
          {
            Name: 'custom:agentlimit', /* required */
            Value: strAgentLimit.toString()
          },
          /* more items */
        ],
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,
        Username: userid /* required */
      };


   await  cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {

     if (err) {
          
          agentLimitupdated.className = "far fa-times-circle"
          _this.AgentLimitUpdateMsg = err.message;
          _this.showprogressbar = false;
          console.log(err, err.stack); // an error occurred

        }
     else {
       
       var spnAgentLimit = document.getElementById("spnAgentLimit_" + userid) as HTMLElement;       
       spnAgentLimit.innerHTML = elementAgentLimit;

       var txtelementAgentLimit = <HTMLInputElement>document.getElementById('agentLimit_' + userid);
       txtelementAgentLimit.value = elementAgentLimit;       

          _this.ShowEditableField('AgentLimit', userid, 'read')

          agentLimitupdated.style.display = "block"

          agentLimitupdated.className = "far fa-check-circle"

          _this.AgentLimitUpdateMsg = "Agent Limit updated"

          _this.showprogressbar = false;

        }      // successful response
      });



    } else {

      agentLimitupdated.style.display = "block"

      agentLimitupdated.className = "far fa-times-circle"

      this.AgentLimitUpdateMsg = "Invalid Value"

      this.showprogressbar = false;
    }

    this.cookieService.set('expires', new Date().getTime().toString());
  }

  //---------------------------------
  //https://docs.aws.amazon.com/AWSJavaScriptSDK/latest/AWS/CognitoIdentityServiceProvider.html#listUsers-property
  //-----------------------------------
  public GetAllUserDetails() {

    var _this = this;
    this.showprogressbar = true;
    this.isshowprogressbar = false;
    var poolData = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, // 
      ClientId: this._AwsCognitoUtil._CLIENT_ID  //
    };

    var params = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,

    };

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();

    return new Promise((resolve, reject) => {
      cognitoidentityserviceprovider.listUsers(params, function (err, data) {
        if (err) {
          console.log(err, err.stack); // an error occurred
          _this.showprogressbar = false;
          reject(err)
        }
        else {
          resolve(data)
          _this.showprogressbar = false;
        }


      })


    });


    this.cookieService.set('expires', new Date().getTime().toString());

  }


  element: HTMLElement;
  hdnelement: HTMLInputElement;
  spnElement: HTMLSpanElement;
  type: string;
  ///--------------------------------------
  //--- Disable and Enable user
  ///-------------------------------------
  ChangeUserActiveState(_UserId: string, _UserType: string, _AgencyName: string, _AgencyFullName: string = "", _AgencyCode: string = "") {

    

    this.hdnelement = document.getElementById("hdn_" + _UserId) as HTMLInputElement;
    this.type = this.hdnelement.value;

    // console.log(_UserId + " " + _UserType);
    // console.log("Enabled - " + this.type);

    if (_UserType == "Agency" && this.type == "true") {

      var deactive = confirm("Are you sure you want to disable " + _AgencyFullName + " agency, this will disable all agents of this agency also?");

      if (deactive == true) {

        if (_AgencyCode == "") {

          this.eventCaptured("Enabled", "Enabled to User -" + _UserId);
    
        } else {
    

          this.eventCaptured("Enabled", "Enabled to "+ _UserId +" of Agency - " + _AgencyFullName + "(" + _AgencyCode + ")");
        }
        this.GetEnabledAgentCount(_AgencyCode).then(result => {
          // Process result
          this.Agentdetails = result;

          for (let i = 0; i < result.length; i++) {

            console.log("User id" + result[i].UserName + " type- " + result[i].UserType);


            // if (this.Agentdetails[i].UserName) 
            // {
            this.disableEnableUser(result[i].UserName, result[i].UserType, _AgencyName, _AgencyCode);
            // }
          }

        })



      }

    }
    else {

      this.disableEnableUser(_UserId, _UserType, _AgencyName, _AgencyCode);

    }


    this.cookieService.set('expires', new Date().getTime().toString());
  }


  disableEnableUser(UserName: string, UserType: string, agencyname: string, agencycode: string) {

    if (this.type == "true") {

      if (agencycode == "") {

        this.eventCaptured("Disabled", "Disabled to User - " + UserName);

      } else {

        this.eventCaptured("Disabled", "Disabled to User - " + UserName + " of Agency - " + agencyname + "(" + agencycode + ")");
      }
    } else
      {
        if (agencycode == "") {

          this.eventCaptured("Enabled", "Enabled to User - " + UserName);
  
        } else {
  
          this.eventCaptured("Enabled", "Enabled to User - " + UserName + " of Agency - " + agencyname + "(" + agencycode + ")");
        }
      
      }
    
    var _this = this;
    this.element = document.getElementById(UserName) as HTMLElement;
    this.element.className = "fas fa-spinner fa-spin";
    this.hdnelement = document.getElementById("hdn_" + UserName) as HTMLInputElement;
    this.spnElement = document.getElementById("hdn_" + UserName) as HTMLSpanElement;
    this.type = this.hdnelement.value;

    /* console.log('user disabled' + _this.element.innerHTML);
    console.log("User name " + UserName + " hidden field status : -" + _this.hdnelement.innerText); */


    var _csp = new CognitoIdentityServiceProvider();
    let isStateChanged: boolean;

    console.log("Processing - " + UserName)

    var params = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,// 'us-east-1_rWWNFO1Xi', /* required */
      Username: UserName /* required */
    };

    if (this.type == "true") {

      console.log("eneterd for disabling - " + UserName)

      _csp.adminDisableUser(params, function (err, data) {
        if (err) {

          isStateChanged = false;
          alert(err.message);
          console.log(err, err.stack); // an error occurred
        }
        else {
          setTimeout(() => { /*Your Code*/
            isStateChanged = true;
            _this.element = document.getElementById(UserName) as HTMLElement;
            _this.hdnelement = document.getElementById("hdn_" + UserName) as HTMLInputElement;
            _this.element.className = 'fas fa-user-times';
            _this.hdnelement.value = "false";
            var row = <HTMLElement>document.getElementById('row_' + UserName);
            row.className = 'bg-light'
            console.log('user disabled' + _this.element.innerHTML);
            console.log("User name " + UserName + " hidden field status : -" + _this.hdnelement.innerText);

            if (UserType == "Agent") {
              _this.GetEnabledAgentCount(agencycode);

            }
          }, 300);

        }
      });
    }


    if (this.type == "false") {

      console.log("enabling to be Processed - " + UserName)

      _csp.adminEnableUser(params, function (err, data) {
        if (err) {
          console.log(err, err.stack); // an error occurred
          isStateChanged = false;

        }
        else {

          
          isStateChanged = true;
          _this.element.className = 'fas fa-user-check';
          _this.hdnelement.value = "true";
          var row = <HTMLElement>document.getElementById('row_' + UserName);
          row.className = ''

          _this.updateLoginCount("0", UserName);

          if (UserType == "Agency") {

          } else if (UserType == "Agent") {

            _this.GetEnabledAgentCount(agencycode);

          }

        }
      });
    }


    var element = <HTMLElement>document.getElementById('selectAgencyName');
    element.nodeValue = agencyname;

  }

  ///---------------------------------------
  //-------Fetch user details
  ///---------------------------------------
  ReloadUserDetails(userFilter: string) {


    var that = this;
    let _data = this.GetAllUserDetails();

    _data.then(function (resolveOutput) {


      var cognitouserdata = resolveOutput["Users"];

      for (let i = 0; i < cognitouserdata.length; i++) {

        let userinfo = new Users();
        userinfo.Id = (i + 1).toString();
        userinfo.UserName = cognitouserdata[i].Username;
        userinfo.IsEnabled = cognitouserdata[i].Enabled;
        userinfo.DateOfCreation = cognitouserdata[i].UserCreateDate;
        userinfo.LastModifiedDate = cognitouserdata[i].UserLastModifiedDate;
        userinfo.UserStatus = cognitouserdata[i].UserStatus;

        for (var j = 0; j < cognitouserdata[i].Attributes.length; j++) {
          var otheratt = cognitouserdata[i].Attributes[j].Name;

          if (otheratt == "email") {

            userinfo.email = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "phone") {
            userinfo.Phone = cognitouserdata[i].Attributes[j].Value;
          }
          else if (otheratt == "custom:usertype") {
            userinfo.UserType = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:logintime") {
            userinfo.logintime = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:loginendtime") {
            userinfo.loginendtime = cognitouserdata[i].Attributes[j].Value;

          }
          else if (otheratt == "custom:fullname") {
            userinfo.FullName = cognitouserdata[i].Attributes[j].Value;

          } else if (otheratt == "email_verified") {
            userinfo.is_email_verified = Boolean(cognitouserdata[i].Attributes[j].Value);

          } else if (otheratt == "custom:agencyname") {
            userinfo.agencyname = cognitouserdata[i].Attributes[j].Value;

          }


        }

        // if (userinfo.UserType != "admin") {
        //   that.usersdetails.push(userinfo);
        // }

      }

      /* that.tableService.setDataSource(that.usersdetails);
      that.usersdetails = that.tableService.getDataSource();
      that.previous = that.tableService.getDataSource();

      that.mdbTablePagination.setMaxVisibleItemsNumberTo(5);
      that.firstItemIndex = that.mdbTablePagination.firstItemIndex;
      that.lastItemIndex = that.mdbTablePagination.lastItemIndex;
      that.mdbTablePagination.calculateFirstItemIndex();
      that.mdbTablePagination.calculateLastItemIndex();
      that.cdRef.detectChanges();
 */



    }, function (rejectOutput) {
      console.log(rejectOutput);
    });

  }

  resendCode(username: string) {
    Auth.resendSignUp(username).then(() => {
      this.msgResend = 'code resent successfully';
    }).catch(e => {
      this.msgResend = 'error : ' + e.Message;
    });
  }

  verfyuserelement: HTMLInputElement;
  confrimUser(username: string) {
    this.verfyuserelement = document.getElementById("verfyUser_" + username) as HTMLInputElement;

    let code = this.verfyuserelement.value;

    Auth.confirmSignUp(username, code, {
      // Optional. Force user confirmation irrespective of existing alias. By default set to True.
      forceAliasCreation: true
    }).then(data => {
      this.msgResend = username + ' confirmed successfully';
    }
    )
      .catch(err => {
        console.log(err)
        this.msgResend = username + ' was not confirmed ' + err.message;
      });

  }
  closeAlert() {

    this.alert.nativeElement.classList.remove('show');
    this.alert.nativeElement.style.display = 'none';
    //this.alert.nativeElement.style.display='none';

  }

  getdate() {

    return this._getdate.getCurrentDate();

  }

  verifydivelement: HTMLElement
  showCodeWindow(username: string, type: string) {
    this.verifydivelement = document.getElementById("divverfy_" + username) as HTMLElement;

    if (type.toLowerCase() == "unconfirmed") {

      this.verifydivelement.style.display = "block"
      //this.verifydivelement.style.position = "fixed"

    } else {

      this.verifydivelement.style.display = "none"
    }

  }


  _loginexpirydate: HTMLInputElement
  loginexpiryUserupdated: HTMLElement

  updateExpiryDate(userid: string) {


    this._loginexpirydate = document.getElementById("loginexpirydateUser_" + userid) as HTMLInputElement;

    this.loginexpiryUserupdated = document.getElementById("loginexpiryUserupdated_" + userid) as HTMLElement;

    var _this = this;
    let expirydate = this._loginexpirydate.value;
    let user = Auth.currentAuthenticatedUser();

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var params = {
      UserAttributes: [ /* required */
        {
          Name: 'custom:expirydate', /* required */
          Value: expirydate
        },
        /* more items */
      ],
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, // 'us-east-1_rWWNFO1Xi', /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else { console.log("attribute updated"); console.log(data); }      // successful response
    });

    _this.loginexpiryUserupdated.style.display = "block"

  }


  refreshpage() {

    /* const prev = this.tableService.getDataSource();

    if (!this.searchText) {
      this.tableService.setDataSource(this.previous);
      this.usersdetails = this.tableService.getDataSource();
    }
 */
    var type = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));
    if (localStorage.getItem("agencycode") != null) {
      
      var agencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
    }
  
     

    if (type == "Agency") {

      this.loadUserInformation("", "", agencyCode);

    } else {
      this.loadUserInformation();
      this.el.nativeElement.querySelector('#selectAgencyName').value = "";
      console.log("reloaded dropdown");
    }

  }


  validateExpiryDate(expirydate: string) {

    var q = new Date();
    var m = q.getMonth();
    var d = q.getDay();
    var y = q.getFullYear();

    var today = new Date();
    var todaydate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    var tdate = new Date(y, m, today.getDate());

    var expirtdt = new Date(this.datepipe.transform(new Date(expirydate), 'yyyy') + "-" +
      this.datepipe.transform(new Date(expirydate), 'MM') + '-' + this.datepipe.transform(new Date(expirydate), 'dd'));
    /* console.log(tdate);
    console.log(expirtdt) */


    // alert(expirtdt + "smaller" + today.getDate())
    if (expirtdt < tdate) {
      this.alert.nativeElement.style.display = 'block';
      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> expiry date must be greater than today ";

      this._userExpiryDate = false;

    } else {
      this._userExpiryDate = true;
      this.closeAlert()
    }

  }


  validatelogintimeDate(logintimestr: string): boolean {

    var regexp = new RegExp('^[0-9:-]*$'), //^[ A-Za-z0-9_@./#&+-]*$
      isPatternValid = regexp.test(logintimestr);

    if (isPatternValid) {

      try {

        var arr = logintimestr.split('-')
        var postionstarttime = arr[0].indexOf(":");
        var postionendtime = arr[1].indexOf(":");

        var arrstarttime = arr[0].split(':')
        var arrendtime = arr[1].split(':')

        if (arr.length < 2 || postionendtime < 0 || postionstarttime < 0 || arrstarttime[0].length < 1 ||
          arrstarttime[1].length < 1 || arrendtime[0].length < 1 ||
          arrendtime[1].length < 1 || !Number.isInteger(Number(arrstarttime[0])) || !Number.isInteger(Number(arrendtime[0]))) {
          this.alert.nativeElement.style.display = 'block';
          this.alert.nativeElement.classList.add('show');
          this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> Invalid login time format ";

          return false;
        }
        var a = arr[0] + ":00";
        var b = arr[1] + ":00";

        var aa1 = a.split(":");
        var aa2 = b.split(":");

        var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
        var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
        var dd1 = d1.valueOf();
        var dd2 = d2.valueOf();

        var today = new Date();
        var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
        var ttm = time.split(":");
        var todaytimepars = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(ttm[0], 10), parseInt(ttm[1], 10), parseInt(ttm[2], 10));
        var todaytime = todaytimepars.valueOf();

        if (dd1 > dd2) {
          this.alert.nativeElement.style.display = 'block';
          this.alert.nativeElement.classList.add('show');
          this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> Login Time must be 09:00-18:00 format and Login start time must be less than end time  ";
          return false;
        } else {
          this.closeAlert();
          return true;
        }


      } catch{
        this.alert.nativeElement.style.display = 'block';
        this.alert.nativeElement.classList.add('show');
        this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> Login Time must be 09:00-18:00 format and Login start time must be less than end time  ";
        return false;

      }
    } else {

      console.log('invalid format');
      return isPatternValid;

    }

    // if (todaytime < dd1 && todaytime > dd2) {

    //   this.alert.nativeElement.classList.add('show');
    //   this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> you are only allowed to login during ";

    // }else{

    //  // this.closeAlert();
    // }

  }


  GetEnabledAgentCount(agencycode: string, type: string = "") {


    var that = this;
    var enabledAgentList: Users[] = [];
    let _data = this.GetAllUserDetails();

    that.Agentdetails = [];

    return _data.then(function (resolveOutput) {

      var cognitouserdata = resolveOutput["Users"];

      var count = 0;

      for (let i = 0; i < cognitouserdata.length; i++) {

        let agentInfo = new Users();

        agentInfo.UserName = cognitouserdata[i].Username;
        agentInfo.IsEnabled = cognitouserdata[i].Enabled;


        for (var j = 0; j < cognitouserdata[i].Attributes.length; j++) {

          var otheratt = cognitouserdata[i].Attributes[j].Name;


          if (otheratt == "custom:agencycode") {

            if (cognitouserdata[i].Attributes[j].Value == agencycode) {

              if (type == "all") {

                agentInfo.agencyname = cognitouserdata[i].Attributes[j].Value
                count = count + 1;


              } else {

                if (cognitouserdata[i].Enabled) {
                  agentInfo.agencyname = cognitouserdata[i].Attributes[j].Value
                  count = count + 1;

                }
              }

            }

          }
          else if (otheratt == "custom:usertype") {
            agentInfo.UserType = cognitouserdata[i].Attributes[j].Value;

          }

        }
        if (agentInfo.agencyname == agencycode) {
          that.Agentdetails.push(agentInfo);
        }
      }

      if (type == "all") { } else {
        that.totalEnabledAgent = count;
        
        var decryptedAgentCreationLimit = that.EncrDecr.get(that.sessionKey+'$#@$^@1ERFLMTAGNT', localStorage.getItem("limitAgentCreation"));       
        that.limitOfAgent = Number(decryptedAgentCreationLimit);
        that.LimitMsg = "Note :- Your Active Agent Limit is " + that.limitOfAgent.toString() + ". Total Enabled Agent - " + that.totalEnabledAgent.toString() + ".";

        that.showprogressbar = false;
      }



      enabledAgentList = that.Agentdetails;

      return enabledAgentList;

    });



  }


  ShowEditableField(val: string, id: string, type: string, usertype: string = "") {

    var CurrentUserType =   this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

    if (val == 'Fullname') {



      if (usertype == 'Agent' && CurrentUserType == "BU") { return false; }


      if (type == 'edit') {

        var element = <HTMLElement>document.getElementById('spnuserfullname_' + id);
        element.style.display = 'none';

        var elementUserFullName = <HTMLInputElement>document.getElementById('userfullname_' + id);
        elementUserFullName.style.display = 'block';

      } else if (type == 'read') {

        var element = <HTMLElement>document.getElementById('spnuserfullname_' + id);
        element.style.display = 'block';

        var elementUserFullName = <HTMLInputElement>document.getElementById('userfullname_' + id);
        elementUserFullName.style.display = 'none';


      }


    }
    else if (val == 'email') {

      if (CurrentUserType == "BU") {

        if (usertype != 'Agent') {

          if (type == 'edit') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnuseremailid_' + id);
            elementSpnEmailid.style.display = 'none';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('useremailid_' + id);
            elementUserEmailid.style.display = 'block';
          }
          else if (type == 'read') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnuseremailid_' + id);
            elementSpnEmailid.style.display = 'block';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('useremailid_' + id);
            elementUserEmailid.style.display = 'none';

          }
        }
      }
    } else if (val == 'AgentLimit') {

      if (usertype == 'Agency') {
        if (type == 'edit') {
          var elementSpnEmailid = <HTMLElement>document.getElementById('spnAgentLimit_' + id);
          elementSpnEmailid.style.display = 'none';

          var elementUserEmailid = <HTMLInputElement>document.getElementById('agentLimit_' + id);
          elementUserEmailid.style.display = 'block';
        }
        else if (type == 'read') {
          var elementSpnEmailid = <HTMLElement>document.getElementById('spnAgentLimit_' + id);
          elementSpnEmailid.style.display = 'block';

          var elementUserEmailid = <HTMLInputElement>document.getElementById('agentLimit_' + id);
          elementUserEmailid.style.display = 'none';

        }
      }
    } else if (val == 'agencycode') {
      if (CurrentUserType == "BU") {
        if (usertype != 'Agent') {
          if (type == 'edit') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnagencycode_' + id);
            elementSpnEmailid.style.display = 'none';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('agencycode_' + id);
            elementUserEmailid.style.display = 'block';
          }
          else if (type == 'read') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnagencycode_' + id);
            elementSpnEmailid.style.display = 'block';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('agencycode_' + id);
            elementUserEmailid.style.display = 'none';

          }
        }
      }
    } else if (val == 'agencyfullname') {
      if (CurrentUserType == "BU") {
        if (usertype != 'Agent') {
          if (type == 'edit') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnagencyfullname_' + id);
            elementSpnEmailid.style.display = 'none';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('agencyfullname_' + id);
            elementUserEmailid.style.display = 'block';
          }
          else if (type == 'read') {
            var elementSpnEmailid = <HTMLElement>document.getElementById('spnagencyfullname_' + id);
            elementSpnEmailid.style.display = 'block';

            var elementUserEmailid = <HTMLInputElement>document.getElementById('agencyfullname_' + id);
            elementUserEmailid.style.display = 'none';

          }
        }
      }
    }
    this.cookieService.set('expires', new Date().getTime().toString());
  }



  GetEnabledAgency(agencycode: string = "", type: string = "") {


    var that = this;
    var enabledAgencyList: Users[] = [];
    let _data = this.GetAllUserDetails();

    that.AgencyAdminDetails = [];

   return  _data.then(function (resolveOutput) {

      var cognitouserdata = resolveOutput["Users"];

      var count = 0;

      for (let i = 0; i < cognitouserdata.length; i++) {

        let agencyInfo = new Users();

        agencyInfo.UserName = cognitouserdata[i].Username;
        agencyInfo.IsEnabled = cognitouserdata[i].Enabled;


        for (var j = 0; j < cognitouserdata[i].Attributes.length; j++) {

          var otheratt = cognitouserdata[i].Attributes[j].Name;


          if (otheratt == "custom:agencycode") {

            if (cognitouserdata[i].Attributes[j].Value == agencycode) {

              if (type == "all") {

                agencyInfo.agencyname = cognitouserdata[i].Attributes[j].Value
                count = count + 1;


              } else {

                if (cognitouserdata[i].Enabled) {
                  agencyInfo.agencyname = cognitouserdata[i].Attributes[j].Value
                  count = count + 1;

                }
              }

            }

          }
          else if (otheratt == "custom:usertype") {
            agencyInfo.UserType = cognitouserdata[i].Attributes[j].Value;

          }

        }


        if (agencyInfo.agencyname == agencycode && agencyInfo.UserType == "Agency") {
         
          that.AgencyAdminDetails.push(agencyInfo);
        } else if (agencycode == "" && agencyInfo.UserType == "Agency") {
          that.AgencyAdminDetails.push(agencyInfo);
        }

      }

      if (type == "all") { } else {

        that.totalEnabledAgent = count;
        var decryptedAgentCreationLimit = that.EncrDecr.get(that.sessionKey+'$#@$^@1ERFLMTAGNT', localStorage.getItem("limitAgentCreation"));
        that.limitOfAgent = Number(decryptedAgentCreationLimit);
        that.LimitMsg = "Note :- Your Active Agent Limit is " + that.limitOfAgent.toString() + ". Total Enabled Agent - " + that.totalEnabledAgent.toString() + ".";

        that.showprogressbar = false;
      }



     enabledAgencyList = that.AgencyAdminDetails;
     console.log(enabledAgencyList);

      return enabledAgencyList;

    });



  }

  
  eventCaptured(event, desc) {
    
    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

    var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));

    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

    var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
    
    var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));

    if(decryptedUserType =="BU"){ decryptedAgencyCode ="Admin"}

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", event, decryptedFullName, decryptedUserType, decryptedUserId + " performed " + desc , new Date().toISOString(),decryptedAgencyName,decryptedAgencyCode);
    console.log("Audit log saved");

  }

  updateLoginCount(getFailedCount: string, userid: string) {
    
    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var _this = this;
    var params = {
      UserAttributes: [ /* required */
        {
          Name: 'custom:failedattemptcount', /* required */
          Value: getFailedCount
        },
        /* more items */
      ],
      UserPoolId: _this._AwsCognitoUtil.USER_POOL_ID, /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
      if (err) console.log(err.message); // an error occurred
      else {
       // console.log(data);

      }      // successful response
    });


}


}
