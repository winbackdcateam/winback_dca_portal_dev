import { Component, Input, OnInit, ViewChild, ElementRef, AfterViewInit, HostListener,NgZone } from '@angular/core';
import { AmplifyService } from 'aws-amplify-angular';
import { Router } from '@angular/router';
import { Auth } from 'aws-amplify';
//import { getToken } from '@angular/router/src/utils/preactivation';
import { NgxCaptchaModule } from 'ngx-captcha';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { AppComponent } from '../app.component';
import { UserIdleService } from 'angular-user-idle';
import * as CognitoIdentity from "aws-sdk/clients/cognitoidentity";
import { CognitoIdentityServiceProvider } from 'aws-sdk/clients/all';
import { Alert } from 'selenium-webdriver';
import { DatePipe } from '@angular/common';
import { SessionUserIdle, PasswordValidator, logRecord, awsCognitoUtil, validate,EncrDecrService } from '../_models';
import { DataService } from '../data.service';
import { jsonpCallbackContext } from '@angular/common/http/src/module';
import { HttpClient } from '@angular/common/http';
import { CookieService } from 'ngx-cookie-service';
import * as CryptoJS from 'crypto-js';
import { logawses } from '../_models/logawses';
import * as AWSGlobal from "aws-sdk/global";
import AWS from 'aws-sdk';
import { CognitoUser } from '@aws-amplify/auth';
import { AuthenticationDetails, CognitoUserPool } from 'amazon-cognito-identity-js';


export interface FormModel {
  captcha?: string;
}

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss']
})
export class AuthComponent implements OnInit, AfterViewInit {

  @HostListener('click', ['$event']) onclick(event: Event) {

    /* console.profile();
    console.profileEnd();
    if (console.clear) console.clear();
    ////sessionStorage.clear();
    localStorage.clear();
 */
  }

 /*  @HostListener('mouseover') onMouseOver() {
    console.profile();
    console.profileEnd();
    if (console.clear) console.clear();
  } */

  @Input() userName: string;

  readonly googlePlayLink: string;
  readonly appStoreLink: string;
  public formModel: FormModel = {};
  public agencyCode :string;
  public agencyName :string;
  protected aFormGroup: FormGroup;

  @ViewChild('alert') alert: ElementRef;

  signedIn: boolean;
  public _jwtsesssion: any;
  IsLoginRequired: boolean = true;
  IsPwdResetRequired: boolean = false;
  IsPwdConfirmationRequired: boolean = false;
  IsUserConfirmationRequired: boolean = false;
  _isAgentFirstTimeLogin: boolean = false;
  _IsUserCodeValid: boolean = false;
  _IsUserNameValid: boolean = false;
  _IsAgentEmpIdValid: boolean = false;
  isReEnteredPassowrd: boolean = false;
  _userPasswordValid: boolean = false;
  _userOldPasswordValid: boolean = false;
  validatingForm: FormGroup;
  public objValidator = new validate();
  user: any;
  greeting: string;
  awsPersonalCreds: any;
  
  msgStr: string;
  isInformationRequired: boolean = false;
  isConfrimingUser: boolean = false;
  issign: boolean = false;
  isResetPassword: boolean = true;
  isfgcode: boolean = false;
  isconfriming: boolean = false;
  popup: HTMLElement;
  public captchaResponse: string = '';
  _killlogin = new SessionUserIdle(this.router);
  _getIdToken: any
  _hiddentoekn: HTMLInputElement;
  AuthenticatedUser: string
  ipAddress: any;
  sitekey: string;
  cookieValue = 'UNKNOWN';
  logawsesobj = new logawses();
 


  resolved(captchaResponse: string) {

    const newResponse = captchaResponse
      ? captchaResponse.substr(0, 7) + "...." + captchaResponse.substr(-7)
      : captchaResponse;
    this.captchaResponse += JSON.stringify(newResponse);

  }


  constructor(private amplifyService: AmplifyService, public router: Router, private _data: DataService, private cookieService: CookieService,private zone: NgZone,
    private EncrDecr: EncrDecrService,private datePipe: DatePipe, public myapp: AppComponent, private  _AwsCognitoUtil: awsCognitoUtil, private http: HttpClient) {

     localStorage.clear();
     sessionStorage.clear();

     

      this.http.get<{ip:string}>('https://api.ipify.org/?format=json')
      .subscribe( data => {
       
        this.ipAddress = JSON.stringify(data.ip);
  
        sessionStorage.setItem("ClientIP",this.ipAddress + " - " + this.getBrowserName());
  
      })
    
    this.checkSignInStatus(false, "auto");

  }
  ngOnDestroy(): void {
    
  }
  ngOnInit() {

    if (window.location.href.toString().includes( "http://localhost:")) {

      this.sitekey = "6Ldp0xgUAAAAAF_iIss_hpFaVrjLbPGjwyfJwebB";

    } else {

      this.sitekey = this._AwsCognitoUtil.CAPTCHA_SITE_KEY;
    }

    this.myapp.disableTimer();
    this.myapp.stopWatching();

  }




  ngAfterViewInit() {
    this.myapp.showtimeoutpopup = false;
    this.myapp.stop();


  }

  //-----------------------
  //--Login here 
  //--------------------
  signIn(username: string, password: string) {

   
   

    username = username.toLowerCase();
    
    sessionStorage.clear();
    localStorage.clear();

    this.http.get<{ip:string}>('https://api.ipify.org/?format=json')
    .subscribe( data => {
     
      this.ipAddress = JSON.stringify(data.ip);

      
      
      sessionStorage.setItem("ClientIP",this.ipAddress + " - " + this.getBrowserName());

    })

    if (username.trim() != "" && password.trim() != "") {
      var _this = this;
      this.userName = username;


     if (this.captchaResponse != "") 
     {
       
      //---------------------------


        try {

          _this.issign = true;
          this.isconfriming = false;
          this.isfgcode = false;

          const user = Auth.signIn(username, password)
            .then(result => {

                 this._getIdToken = result.signInUserSession.accessToken.jwtToken

              
          
                 console.log(result);
                  sessionStorage["token"] =this._getIdToken;

                
             
               

              let dateTime = new Date()

              this.cookieService.set('expires', new Date().getTime().toString());
              
              
              _this.checkSignInStatus(true,"",this._getIdToken,username);

              

            })
            .catch(ex => {
              
              console.log(ex)

              this.alert.nativeElement.classList.add('show');
             
              this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>error !</b></strong>  " + ex.message;
             
              _this.issign = false;

              console.log(ex.code)

              if (ex.code == "NotAuthorizedException") {
               
                this.CountFailedAttempt(username);
                
              }

             
             
            });
         
        } catch (err) {

          _this.issign = false;
          this.alert.nativeElement.classList.add('show');
          this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>error ! Login Failed </b></strong> " + err.message;

          if (err.code === 'UserNotConfirmedException') {
            // The error happens if the user didn't finish the confirmation step when signing up
            // In this case you need to resend the code and confirm the user
            // About how to resend the code and confirm the user, please check the signUp part
          } else if (err.code === 'PasswordResetRequiredException') {
            // The error happens when the password is reset in the Cognito console
            // In this case you need to call forgotPassword to reset the password
            // Please check the Forgot Password part.
          } else if (err.code === 'NotAuthorizedException') {
            // The error happens when the incorrect password is provided
          } else if (err.code === 'UserNotFoundException') {
            // The error happens when the supplied username/email does not exist in the Cognito user pool
          } else {
            console.log(err);
          }

          _this.alert.nativeElement.classList.add('show');
          _this.msgStr = "<strong><b>error !</b></strong> " + err.message;
          _this.issign = false;
        }

        
      }
      else {

        this.alert.nativeElement.classList.add('show');
        this.msgStr = "<strong><b>error !</b></strong> You need to click captcha";

      } 
    } else {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<strong><b>error !</b></strong> User Id and Password are required ";

    }

  }

  getParameter(name :string){

//dcaPartnerKey
   
    const ssm = new AWS.SSM();
    const params = {
    Name: name, 
    
    };

  const request =  ssm.getParameter(params);
  


  request.on('success', function(response) {
    
    var ssmjson = JSON.stringify(response.data);
    var obj =JSON.parse(ssmjson);
    //alert(obj.Parameter.Value)
  });
  
  request.on('error', function(err) {
    alert("AWS SSM got error");
    throw err;
  });
  
  return request.send();

   /*  AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: 'ap-southeast-1:e4ad475f-9f52-4c57-8760-edaae74b7456',
      Logins: {
        'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': accessToken
      }
    });
    
    AWS.config.update({
      region: 'ap-southeast-1',
      credentials: AWS.config.credentials
    }); 

    AWSGlobal.config.update({region: 'ap-southeast-1',
    credentials: AWS.config.credentials}); */
   

    



  }

  checkSignInStatus(isLoadRequired: boolean, type: string = "",jwtIdToken: string ="",user: string="") {
    

    var _this = this;
    let flg = false;

    try {

      this.amplifyService.authStateChange$
        .subscribe(authState => {
          this.signedIn = authState.state === 'signedIn';
          if (!authState.user) {
            this.user = null;
            this.router.navigateByUrl('/auth');
            _this.issign = false;
          } else {

            this.user = authState.user;
          //  console.log("current User details -" + JSON.stringify(this.user));

            if (type == "auto") {

              console.log('Auto logging In...')
              if (localStorage.getItem('lsk') == null) {
                sessionStorage.setItem("sessionkey", this.EncrDecr.getRandomSessionKey(10).toUpperCase());
                localStorage.setItem("localsession",sessionStorage.getItem("sessionkey"));
              }
              const cookieExists: boolean = this.cookieService.check('expires');

              if (cookieExists) {

                const cookieValue: string = this.cookieService.get('expires');
              //  console.log('loading time - ' + cookieValue)
                var res = Math.abs(new Date().getTime() - parseInt(this.cookieValue)) / 1000;
                var minutes = Math.floor(res / 60) % 60;

                if (minutes < 20) {
                  this.CheckLoginExpiry(this.user.username, isLoadRequired);
                } else if (!isNaN(minutes)) {
                 // console.log('Loading Time diff - ' + minutes);
                //  console.log('Cookie got expired');
                  _this._killlogin.signout(this.user.username);

                } else {
                 // console.log('Loading Time diff - ' + minutes);
                  this.CheckLoginExpiry(this.user.username, isLoadRequired);
                }


              } else {

               // console.log('Logging In...')
                if (localStorage.getItem('lsk') == null) {
                  sessionStorage.setItem("sessionkey", this.EncrDecr.getRandomSessionKey(10));
                  localStorage.setItem("localsession",sessionStorage.getItem("sessionkey"));
                }
                this.CheckLoginExpiry(this.user.username, isLoadRequired);

              }



            } else {

             // console.log('Logging In...')
              this.CheckLoginExpiry(this.user.username, isLoadRequired);
            }

          }
        });


    } catch (errmg) {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<strong><b>error !</b></strong> " + errmg.message;
    }


               
  }


  CheckLoginExpiry(userid: string, isLoadRequired: boolean) {

   
    let sessionKey = sessionStorage.getItem("sessionkey");
    localStorage.setItem("lsk", sessionKey);
    var existingjwttoken = "";
    var UserFullName = "";
    var userlogintime = "";
    var usertype = "";
    
    var _this = this;
    var isFirstTimeLogin = "N";
    var  agentEmpId = "";
    var emailAddress = "";
    var limitAgentCreation = "";
    this.AuthenticatedUser = userid;
    let flg = false;

  



    Auth.currentSession()
    .then(data => {
      console.log("current session")
      

      AWS.config.credentials = new AWS.CognitoIdentityCredentials({
        IdentityPoolId: this._AwsCognitoUtil._IDENTITY_POOL_ID,
        Logins: {
          'cognito-idp.ap-southeast-1.amazonaws.com/ap-southeast-1_2uyy4kb5L': data.getIdToken().getJwtToken()
        }
      });
  
      AWS.config.update({
        region: 'ap-southeast-1',
        credentials: AWS.config.credentials
      }); 
  
      AWSGlobal.config.update({region: 'ap-southeast-1',
      credentials: AWS.config.credentials});
    
      


    
 
    var paramsData = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, // 'us-east-1_rWWNFO1Xi', /* required */
      Username: userid /* required */
    };

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    cognitoidentityserviceprovider.adminGetUser(paramsData, function (err, data) {
      if (err) { console.log(err.message); } // an error occurred
      else {

        //console.log(data)
        try {

          UserFullName = data.UserAttributes.find((a) => a.Name === 'custom:fullname').Value
          usertype = data.UserAttributes.find((a) => a.Name === 'custom:usertype').Value
          emailAddress = data.UserAttributes.find((a) => a.Name === 'email').Value
          isFirstTimeLogin = data.UserAttributes.find((a) => a.Name === 'custom:isfirsttimelogin').Value
          userlogintime = data.UserAttributes.find((a) => a.Name === 'custom:logintime').Value
          _this.agencyName = data.UserAttributes.find((a) => a.Name === 'custom:agencyname').Value
          existingjwttoken = data.UserAttributes.find((a) => a.Name === 'custom:jwttoken').Value
          limitAgentCreation = data.UserAttributes.find((a) => a.Name === 'custom:agentlimit').Value


        } catch
        {
          //_this.router.navigateByUrl('/home?' + userid);
        }
       
       
        try {

         

          _this.agencyCode = data.UserAttributes.find((a) => a.Name === 'custom:agencycode').Value

          
         
          var encryptedAgencyCode =  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFACODE', _this.agencyCode);
         
          localStorage.setItem("agencycode", encryptedAgencyCode);

        }
        catch(e)
        {
          //_this.router.navigateByUrl('/home?' + userid);
        }

        try {
          let agentFullName = data.UserAttributes.find((a) => a.Name === 'custom:agencyfullname').Value
          var encryptedAgentFullName =  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFAGENTFNAME', agentFullName);
          localStorage.setItem("agentfullname", encryptedAgentFullName);

        } catch
        {
          //_this.router.navigateByUrl('/home?' + userid);
        }

        try {
          agentEmpId = data.UserAttributes.find((a) => a.Name === 'custom:empid').Value
          var encryptedAgentEmpId =  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFEMPID', agentEmpId);
          localStorage.setItem("empid", encryptedAgentEmpId);

        } catch
        {

        }finally{

         
          _this.updateLoginCount("0", userid);
              

            if(usertype =='BU')
            {
              _this.agencyCode = 'Admin';
            }

            console.log("Audit log to be saved");
            _this.logawsesobj.logIntoAWSES(UserFullName, "DCA", "Login", userid, usertype, userid + " signed In", new Date().toISOString(),_this.agencyName,_this.agencyCode);
            console.log("Audit log saved");

        }




        if (usertype == 'Agent' || usertype == 'Agency') {

          try {

            var passwordExpiryDate = data.UserAttributes.find((a) => a.Name === 'custom:expirydate').Value
           // console.log("password expiry date : -" + passwordExpiryDate)
            if (passwordExpiryDate.length > 1) {
              if (!_this.validateExpiryDate(passwordExpiryDate)) {


                _this._killlogin.signout(userid);
                _this.issign = false;
                _this.alert.nativeElement.classList.add('show');
                _this.msgStr = "<b>Your password expired on " + passwordExpiryDate + ". So, please change your dca login password as soon as possible.</b> <hr> Password must contain at least 1 lowercase alphabetical character,1 uppercase alphabetical character,least 1 numeric character, one special character and 8 characters or longer ";

                _this.router.navigateByUrl('/auth');
                flg = false;

                if (usertype == 'Agent') {
                  _this.ShowConfrimUserScreen();
                  _this._isAgentFirstTimeLogin = true;
                } else if (usertype == 'Agency') {
                  _this.ShowPasswordResetWindow();
                }

                return false;

              }
            }

          } catch
          {
            //_this.router.navigateByUrl('/home?' + userid);
          }
        }






        if (isFirstTimeLogin === "Y") {

          flg = false;
          _this.issign = false;

          if (usertype == 'Agent') {
            //console.log(usertype);
            _this._isAgentFirstTimeLogin = true;
            _this.IsLoginRequired = false;
          }
          else {

            _this._killlogin.signout(userid);
            _this.ShowPasswordResetWindow();
          }
          _this.alert.nativeElement.classList.add('show');
          _this.msgStr = "<b>Password reset is required for new user.</b> <hr> Password must contain at least 1 lowercase alphabetical character,1 uppercase alphabetical character,least 1 numeric character, one special character and 8 characters or longer ";


        } else {
          if (userlogintime != "00:00-00:00") {

            /// alert(userlogintime);

            if (!_this.validatelogintimeDate(userlogintime,userid)) {

              flg = false;
              _this._killlogin.signout(userid);
              _this.issign = false;

            } else {

              flg = true;

            }
          } else {
            flg = true;
          }
        }





        if (flg)
        {
          

          (async ()=>{ 
          var encryptedIsLoggedIn = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFISLOGIN', 'true');
            localStorage.setItem('isLoggedIn', encryptedIsLoggedIn);    
           
          var encryptedexistingjwttoken = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFJWT', existingjwttoken);
          localStorage.setItem('token', encryptedexistingjwttoken);
          
          var encrypteduserid = await _this.EncrDecr.set(sessionKey+'$#@$^@1ERFUSRID', userid);
            localStorage.setItem('userid', encrypteduserid);

          var encryptedUserFullName = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFFNAME',UserFullName );
            localStorage.setItem('fullname', encryptedUserFullName);
           
          var encryptedUserType =  await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFTYP',usertype );
            localStorage.setItem('type', encryptedUserType);
          
          var encryptedAgencyName = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFAGENCY',_this.agencyName );
            localStorage.setItem('agencyname', encryptedAgencyName);
           
          var encryptedEmailAddress = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFEMAIL',emailAddress );
            localStorage.setItem('emailid', encryptedEmailAddress);
           
          var encryptedLimitAgentCreation = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFLMTAGNT',limitAgentCreation );
          localStorage.setItem('limitAgentCreation', encryptedLimitAgentCreation);

          var encryptedAgencyLoginTime = await  _this.EncrDecr.set(sessionKey+'$#@$^@1ERFAGENCYTM',userlogintime );
            localStorage.setItem('agencylogintime', encryptedAgencyLoginTime);

            _this.getParameterValue("dcaPartnerKey","DPK");
            _this.getParameterValue("dcaapipartnerpassword","DAPP");
          
           
        })();

          _this.myapp.CheckPageIdle();

          _this.router.navigateByUrl('/home');

        }

      }



    });

   //
//---------------------------------
})
.catch(err => console.log(err)); 
    //-------

  }

  ForgotPassword(username: string) {


    Auth.configure({
      Auth: {
        // REQUIRED - Amazon Cognito Identity Pool ID
        identityPoolId: this._AwsCognitoUtil._IDENTITY_POOL_ID,
        // REQUIRED - Amazon Cognito Region
        region: this._AwsCognitoUtil._REGION,
        // OPTIONAL - Amazon Cognito User Pool ID
        userPoolId: this._AwsCognitoUtil.USER_POOL_ID,
        // OPTIONAL - Amazon Cognito Web Client ID
        userPoolWebClientId: this._AwsCognitoUtil._CLIENT_ID
      }
    });

    this.closeAlert();

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(username, "DCA", "Forgot Password", "", "", username + " performed forgot password", new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved");

    if (username.trim().length > 0) {

      Auth.forgotPassword(username)
        .then(data => {
          // console.log(data)
          this.IsPwdResetRequired = false;
          this.IsPwdConfirmationRequired = true;

          this.isconfriming = false;
          this.isfgcode = true;
          this.issign = false;
        })
        .catch(err => {
          console.log(err)
          this.alert.nativeElement.classList.add('show');
          this.msgStr = "<strong><b>error !</b></strong> " + err.message;
        });
    } else {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<strong><b>error !</b> User name is required</strong> ";

    }
  }

  confrimNewPassword(username: string, code: string, new_password: string, confrimPassword: string) {

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(username, "DCA", "Change PAssword", "", "", username + " change password", new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved");

    if (confrimPassword == new_password) {
      if (username.trim().length > 0 && new_password.trim().length > 0 && code.trim().length > 0) {
        // Collect confirmation code and new password, then
        Auth.forgotPasswordSubmit(username, code, new_password)
          .then(data => {

            this.IsLoginRequired = true;
            this.IsPwdConfirmationRequired = false;
            this.isconfriming = true;
            this.isfgcode = false;
            this.issign = false;
            this.alert.nativeElement.classList.add('show');
            this.msgStr = "<i class='fas fa-thumbs-up'></i><strong>&nbsp;&nbsp;<b>Password has been reset successfully</b></strong> ";

            this.updateFirstTimeLogin(username);
            this.updatePasswordExpiryDate(username);
            
          })
          .catch(err => {

            this.alert.nativeElement.classList.add('show');
            this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>error !</b></strong> " + err.message;
            console.log(JSON.stringify(err))
          });
      }
      else {

        this.alert.nativeElement.classList.add('show');
        this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>error !</b>All fields are required</strong> ";

      }
    } else {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>error !</b>Password is not matching</strong> ";


    }

  }

  updateFirstTimeLogin(userid: string) {

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(userid, "DCA", "First Time Change Password", "", "", userid + " performed change password", new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved");

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();

    var params = {
      UserAttributes: [ /* required */
        {
          Name: 'custom:isfirsttimelogin', /* required */
          Value: 'N'
        },
        /* more items */
      ],
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
      if (err) console.log("Error - " + err.message); // an error occurred
      else { //console.log(data);

      }      // successful response
    });


  }

  hideSignin() {
    this.isResetPassword = false;

  }


  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
    //this.alert.nativeElement.style.display = 'none';
  }



  ShowPasswordResetWindow() {

    this.IsPwdResetRequired = true;
    this.IsLoginRequired = false;
    this.alert.nativeElement.classList.remove('show');
  }

  ShowConfrimUserScreen() {
    this.IsUserConfirmationRequired = true;
    this.IsLoginRequired = false;
  }

  resetscreen() {

    this.closeAlert();

    this.IsLoginRequired = true;
    this.IsPwdResetRequired = false;
    this.IsPwdConfirmationRequired = false;
    this.IsUserConfirmationRequired = false;

    this.isconfriming = false;
    this.isfgcode = false;
    this.issign = false;
    this.isConfrimingUser = false;
    this._isAgentFirstTimeLogin = false;
  }


  updateUsersJWTToken(userid: string) {

    Auth.currentSession()
      .then(data => {
        //console.log(data)
        this._getIdToken = data.getIdToken().getJwtToken();
        this.passtoken(userid, this._getIdToken);

      })
      .catch(err => console.log(err));

  }

  passtoken(userid: string, currenttoken: string) {

    let user = Auth.currentAuthenticatedUser();
    let existingjwttoken = "";
    let expirydate = "", userlogintime = "";
    let usertype = ""
    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var _this = this;
    var awsparams = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,//'us-east-1_rWWNFO1Xi', /* required */
      Username: userid /* required */
    };


    console.log("AWS credentials -" + AWS.config.credentials);

    cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
      if (err) console.log(err.message); // an error occurred
      else {
        existingjwttoken = data.UserAttributes.find((a) => a.Name === 'custom:jwttoken').Value
        expirydate = data.UserAttributes.find((a) => a.Name === 'custom:expirydate').Value
        userlogintime = data.UserAttributes.find((a) => a.Name === 'custom:logintime').Value
        //usertype = data.UserAttributes.find((a) => a.Name === 'custom:usertype').Value
        var flg = false;

        if (userlogintime != "00:00-00:00") {


          if (!_this.validatelogintimeDate(userlogintime, userid)) {

            _this._killlogin.signout(userid);

          } else {


            _this.checkSignInStatus(true);

          }
        } else {

          _this.checkSignInStatus(true);
        }

        //}








      }    // successful response
    });







  }

  disableUser(userid: string) {

    var _this = this;

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(userid, "DCA", "Login Failure Exceeded", "", "", userid + " got disabled due to failure attempt", new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 

    var _csp = new CognitoIdentityServiceProvider();

    var params = {
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, // 'us-east-1_rWWNFO1Xi', /* required */
      Username: userid /* required */
    };
    _csp.adminDisableUser(params, function (err, data) {
      if (err) {
        console.log(err, err.stack); // an error occurred   

      }
      else {

        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = _this.msgStr +  "<br> It seems that your login id has been blocked or disabled after exceeding failed login attempts. ";

      }

    });

  }


  validatelogintimeDate(logintimestr: string,userid : string =""): boolean {


    var arr = logintimestr.split('-')

    var a = arr[0] + ":00";
    var b = arr[1] + ":00";

    var aa1 = a.split(":");
    var aa2 = b.split(":");

    var d1 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa1[0], 10), parseInt(aa1[1], 10), parseInt(aa1[2], 10));
    var d2 = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(aa2[0], 10), parseInt(aa2[1], 10), parseInt(aa2[2], 10));
    var dd1 = d1.valueOf();
    var dd2 = d2.valueOf();

    var today = new Date();
    var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    var ttm = time.split(":");
    var todaytimepars = new Date(parseInt("2001", 10), (parseInt("01", 10)) - 1, parseInt("01", 10), parseInt(ttm[0], 10), parseInt(ttm[1], 10), parseInt(ttm[2], 10));
    var todaytime = todaytimepars.valueOf();

    // console.log(" checking here starttime -" + dd1 + "endtime  -" + dd2 + "Today time - " + todaytime)
    if (todaytime < dd1 || todaytime > dd2) {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = "<i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> you are only allowed to login during " + logintimestr;
      
    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(userid, "DCA", "Odd Time Login Attempt", "", "", userid + " tried to login in odd time " + logintimestr , new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 
      
      return false;
    }

    return true;

  }

  validateExpiryDate(expirydate: string): boolean {



    var q = new Date();
    var m = q.getMonth();
    var d = q.getDay();
    var y = q.getFullYear();

    var today = new Date();
    var todaydate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    var tdate = new Date(y, m, today.getDate());

    var expirtdt = new Date(this.datePipe.transform(new Date(expirydate), 'yyyy') + "-" +
      this.datePipe.transform(new Date(expirydate), 'MM') + '-' + this.datePipe.transform(new Date(expirydate), 'dd'));



    if (expirtdt < tdate) {
      // alert("login expired")
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " <i class='fas fa-exclamation-circle'></i><strong>&nbsp;&nbsp;<b>Warning !</b></strong> your login expired on " + expirydate;
      return false;

    } else {

      this.closeAlert()

      return true;
    }

  }

 

  // this code is not being used 
  confrimNewUser(username: string, code: string, old_password: string, new_password: string) {


    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(username, "DCA", "Agent Confirmation", "", "", username + " performed to first time change password " , new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var _this = this;


    var params = {
      ClientId: this._AwsCognitoUtil._CLIENT_ID, // 'us-east-1_rWWNFO1Xi', /* required */
      ConfirmationCode: code,
      Username: username /* required */
    };


    cognitoidentityserviceprovider.confirmSignUp(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else {  // console.log(data);           // successful response

        Auth.signIn(username, old_password)
          .then(user => {         

            _this.UpdatePassword(username, old_password, new_password);

          });





      }
    });


  }


  UpdatePassword(username: string, old_password: string, new_password: string) {

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(username, "DCA", "Change Password", "", "", username + " performed Change password "  , new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 

    Auth.currentAuthenticatedUser()
      .then(user => {
        return Auth.changePassword(username, old_password, new_password);
      })
      .then(data => { //console.log(data)

       // console.log(username + " New User's Password has been changed");
        this.updatePasswordExpiryDate(username);
      })
      .catch(err => console.log(err));

  }


  validatePasswordFields(val: string) {


    if (!this.objValidator.PasswordCheck(val)) {
      this.alert.nativeElement.classList.add('show');
      this.msgStr = " User <strong><b> Password </b></strong> is required. <hr> Password must contain at least 1 lowercase alphabetical character,1 uppercase alphabetical character,least 1 numeric character, one special character and 8 characters or longer ";
      this._userPasswordValid = false;
    }
    else {
      this._userPasswordValid = true;
      this.closeAlert();

    }

  }

  validateRequiredFields(val: string, type: string) {

    if (!this.objValidator.RequiredFields(val)) {

      this.alert.nativeElement.classList.add('show');
      this.msgStr = " <strong><b>" + type + "&nbsp;</b></strong> is required. ";

      if (type == "User Name") {
        this._IsUserNameValid = false;
      } else if (type == "Verification code") {

        this._IsUserCodeValid = false;
      } else if (type == "Old Password") {

        this._userOldPasswordValid = false;
      } else if (type == "New Password ") {

        this._userPasswordValid = false;
      } else if (type == "Employee Id") {

        this._IsAgentEmpIdValid = false;

      } else if (type == "Re-enter Password") {

        this.isReEnteredPassowrd = false;
      }

    }
    else {

      if (type == "User Name") {

        this._IsUserNameValid = true;

      } else if (type == "Verification code") {

        this._IsUserCodeValid = true;
      }
      else if (type == "Old Password") {

        this._userOldPasswordValid = true;
      }
      else if (type == "New Password") {

        this._userPasswordValid = true;

      } else if (type == "Employee Id") {

        this._IsAgentEmpIdValid = true;

      } else if (type == "Re-enter Password") {

        this.isReEnteredPassowrd = true;
      }


    }

  }


  msgConfirmUser: string
  confirmAgent(employeeId: string, defaultPassword: string, newPassword: string, reenterPassword: string) {

    this.closeAlert();

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(employeeId, "DCA", "Agent Confirmation", "", "", employeeId + " performed to first time change password " , new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 

    this.isConfrimingUser = true

    var accessKey = "", accessTokenstring = "";

    if (newPassword == reenterPassword) {

      // console.log('user id  - ' + this.AuthenticatedUser)

      Auth.currentSession()
        .then(data => {

          this._getIdToken = data.getAccessToken().getJwtToken();
          this.ChangeDefaultPassword(this.AuthenticatedUser, this._getIdToken, defaultPassword, newPassword);
          this.isConfrimingUser = false
        })
        .catch(err => {
          console.log(err)

          this.isConfrimingUser = false
        });
    }
    else {
      this.isConfrimingUser = false
      this.msgConfirmUser = "Password is not matching";
    }

  }

  ChangeDefaultPassword(username: string, accesstoken: string, defaultPassword: string, newPassword: string) {

    var _this = this;

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(username, "DCA", "Change Password", "", "", username + " performed to change password " , new Date().toISOString(),this.agencyName,this.agencyCode);
    console.log("Audit log saved"); 

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var params = {
      AccessToken: accesstoken, /* required */
      PreviousPassword: defaultPassword, /* required */
      ProposedPassword: newPassword /* required */
    };
    cognitoidentityserviceprovider.changePassword(params, function (err, data) {
      if (err) {
        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='fas fa-times'></i><strong>&nbsp;&nbsp;<b>Password has not been reset</b></strong> <hr>" + JSON.stringify(err).toString();
        console.log(err, err.stack);
      }// an error occurred
      else {

        _this.IsLoginRequired = true;
        _this._isAgentFirstTimeLogin = false;
        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='fas fa-thumbs-up'></i><strong>&nbsp;&nbsp;<b>Password has been reset successfully</b></strong> ";
        _this.isConfrimingUser = false;

        _this.updateFirstTimeLogin(username);
        _this.updatePasswordExpiryDate(username);

      }           // successful respons
    });




  }


  IsUserValidated: boolean
  hasNewUserFilledInformation(EmployeeId: string, defaultPassword: string, newPassword: string, reenterPassword: string): boolean {
    var _this = this;
    var flg = true;
    let sessionKey  = sessionStorage.getItem("sessionkey");
    this.validateRequiredFields(reenterPassword, "Re-enter Password")
    this.validateRequiredFields(newPassword, "New Password")
    this.validateRequiredFields(defaultPassword, "Old Password")
    this.validateRequiredFields(EmployeeId, "Employee Id")


    if (this._IsAgentEmpIdValid) {

      

      var decryptedEmpId = this.EncrDecr.get( sessionKey +'$#@$^@1ERFEMPID', localStorage.getItem("empid"));

      
      
      if (EmployeeId.trim() != decryptedEmpId) {
        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='far fa-times-circle'></i><strong>&nbsp;&nbsp;<b>Invalid Employee Id</b></strong> ";
        _this.isConfrimingUser = false;

        flg = false;

        return flg;

      }


    }

    if (this._userPasswordValid && this.isReEnteredPassowrd) {
      this.validatePasswordFields(newPassword),
        this.validatePasswordFields(reenterPassword);

    }



    if (this._userOldPasswordValid &&
      this._IsAgentEmpIdValid &&
      this._userPasswordValid && this.isReEnteredPassowrd) {



      if (EmployeeId.trim() == "") {

        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='far fa-times-circle'></i><strong>&nbsp;&nbsp;<b>Employee Id is required</b></strong> ";
        _this.isConfrimingUser = false;

        flg = false;

      } else if (defaultPassword == newPassword) {
        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='fas fa-times-circle'></i><strong>&nbsp;&nbsp;<b>Old Password and New Password can not be same</b></strong> ";
        _this.isConfrimingUser = false;
        flg = false;

      } else if (newPassword != reenterPassword) {
        _this.alert.nativeElement.classList.add('show');
        _this.msgStr = "<i class='fas fa-times-circle'></i><strong>&nbsp;&nbsp;<b>Password is not matching</b></strong> ";
        _this.isConfrimingUser = false;
        flg = false;

      }
    } else {
     // console.log("validation false")
      flg = false;

    }

    if (flg) {

      this.IsUserValidated = true;
      this.closeAlert();


    } else {

      this.IsUserValidated = false;
    }

    return flg;
  }

  isUserInvalid() {

    // if (this._IsUserCodeValid && this._IsUserNameValid
    //   && this._userPasswordValid) {
    //   console.log("true")
    //   return true;
    // } else {
    //   console.log("false")
    //   return false;
    // }

    return this.IsUserValidated;

  }


  CountFailedAttempt(userid: string) {

    try {
      let userlogintime = "";
      let getFailedCount = "0" ;
      let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
      var _this = this;
      var awsparams = {
        UserPoolId: this._AwsCognitoUtil.USER_POOL_ID,//'us-east-1_rWWNFO1Xi', /* required */
        Username: userid /* required */
      };
      console.log(AWS.config.credentials);
      cognitoidentityserviceprovider.adminGetUser(awsparams, function (err, data) {
        if (err) console.log(err.message, err.stack); // an error occurred
        else {
        if(data.Enabled)
          {   
      
          try {

            getFailedCount = data.UserAttributes.find((a) => a.Name === 'custom:failedattemptcount').Value
          
          } catch (err_msg) { }
         
          var flg = false;

          if (getFailedCount == "") {
            
            getFailedCount = "0";

          }
         

          if (Number(getFailedCount) < 3)
          {

            _this.msgStr =  " Total " + String(3 - Number(getFailedCount) ) + " attempts are remaining"

            getFailedCount = String(Number(getFailedCount) + 1);

            _this.updateLoginCount(getFailedCount, userid);
           
          } else {

            _this.disableUser(userid);
          }

        }
        else
        {

          _this.msgStr =  "User Id - "+userid+" has been blocked or disabled, Please contact to admin";


        }

      }    // successful response
    });


  } catch (e) { console.log(e.message) }

}


  updateLoginCount(getFailedCount: string, userid: string) {
    
    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();
    var _this = this;
    var params = {
      UserAttributes: [ /* required */
        {
          Name: 'custom:failedattemptcount', /* required */
          Value: getFailedCount
        },
        /* more items */
      ],
      UserPoolId: _this._AwsCognitoUtil.USER_POOL_ID, /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
      if (err) console.log(err.message); // an error occurred
      else {
       // console.log(data);

      }      // successful response
    });


}

  updatePasswordExpiryDate(userid: string) {


    var date = new Date();
    date.setDate(date.getDate() + 30);
    var dateString = date.toISOString().split('T')[0];

    let cognitoidentityserviceprovider = new CognitoIdentityServiceProvider();

    var params = {
      UserAttributes: [ /* required */
        {
          Name: 'custom:expirydate', /* required */
          Value: dateString
        },
        /* more items */
      ],
      UserPoolId: this._AwsCognitoUtil.USER_POOL_ID, /* required */
      Username: userid /* required */
    };
    cognitoidentityserviceprovider.adminUpdateUserAttributes(params, function (err, data) {
      if (err) console.log(err, err.stack); // an error occurred
      else { //console.log(data);

      }      // successful response
    });


  }

  private ipRegex = new RegExp(/([0-9]{1,3}(\.[0-9]{1,3}){3}|[a-f0-9]{1,4}(:[a-f0-9]{1,4}){7})/);
  localIp = sessionStorage.getItem('LOCAL_IP');
 
    
    /*
  private determineLocalIp() {
    window.RTCPeerConnection = this.getRTCPeerConnection();

    const pc = new RTCPeerConnection({ iceServers: [] });
    pc.createDataChannel('');
    pc.createOffer().then(pc.setLocalDescription.bind(pc));

    pc.onicecandidate = (ice) => {
      this.zone.run(() => {
        if (!ice || !ice.candidate || !ice.candidate.candidate) {
          return;
        }
        console.log('ip' + JSON.stringify(pc));
        console.log('ip' + JSON.stringify( pc.onicecandidate));
       
        this.localIp = JSON.stringify( this.ipRegex.exec(ice.candidate.candidate));
        sessionStorage.setItem('LOCAL_IP', this.localIp);
        console.log('ip' + this.localIp);
        pc.onicecandidate = () => {};
        pc.close();
      });
    };
  }*/

  private getRTCPeerConnection() {
    return window.RTCPeerConnection ||
      window.mozRTCPeerConnection ||
      window.webkitRTCPeerConnection;
  }
  
  getBrowserName() {
    const agent = window.navigator.userAgent.toLowerCase()
    switch (true) {
      case agent.indexOf('edge') > -1:
        return 'edge';
      case agent.indexOf('opr') > -1 && !!(<any>window).opr:
        return 'opera';
      case agent.indexOf('chrome') > -1 && !!(<any>window).chrome:
        return 'chrome';
      case agent.indexOf('trident') > -1:
        return 'ie';
      case agent.indexOf('firefox') > -1:
        return 'firefox';
      case agent.indexOf('safari') > -1:
        return 'safari';
      default:
        return 'other';
    }

  }

  public  getParameterValue(name :string,pkey : string){
    var _this =this;
    const ssm = new AWS.SSM();
     
       var params = {
           Name: name, 
           WithDecryption: true
       };
       
       var request =  ssm.getParameter(params);
       
       let sessionKey  = sessionStorage.getItem("sessionkey");

  request.on('success', function(response) {
    
    var ssmjson = JSON.stringify(response.data);
    var obj =JSON.parse(ssmjson);
    var encryptedPartnerkey =  _this.EncrDecr.set(sessionKey+'$#@$^@1'+ pkey,obj.Parameter.Value);
    localStorage.setItem(name, encryptedPartnerkey);
    //console.log(obj.Parameter.Value)
    //return obj.Parameter.Value;
  });
  
  request.on('error', function(err) {
    alert("AWS SSM got error");
    throw err;
  });
  
   request.send();
   



  }

}


