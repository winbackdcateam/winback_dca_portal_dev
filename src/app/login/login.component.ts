import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder,FormControl, Validators  } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { ContainerEvents, FileObject, FileObjectStatus,  } from '../_models/types';
import { Router } from '@angular/router';

import { Auth } from 'aws-amplify';
import { AmplifyService } from 'aws-amplify-angular';
import { User } from '../_models';
 

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  files: FileObject[] = [];
  
  signedInUser: User;
  uploadStarted = false;
 
  title = 'Angular Form Validation Tutorial';
  angForm: FormGroup;
  constructor(private authService: AmplifyService,
    private router: Router,
    ) {
   
 } 



ngOnInit(): void {
    
Auth.currentAuthenticatedUser()
  .then(user => {
    //return Auth.changePassword(user, 'oldPassword', 'newPassword');

    this.signedInUser = user;
    //console.log(JSON.stringify( this.signedInUser));
    
  
    if (!this.signedInUser || !this.signedInUser.signedIn) {
      // this.authService.redirectToSignin(this.router.routerState.snapshot.root.queryParams);
     // this.router.navigate(['signin']);
     // return;
    }
  })
  // .then(data => console.log(data))
  // .catch(err => console.log(err));


    // Auth.getCurrentUser((err, user: User) => {
    //   this.signedInUser = user;
    //   this.uploadService.setSignedInUser(this.signedInUser);
    //   if (!this.signedInUser || !this.signedInUser.signedIn) {
    //     // this.authService.redirectToSignin(this.router.routerState.snapshot.root.queryParams);
    //     this.router.navigate(['signin']);
    //     return;
    //   }
    // });
  
  }
  

 

  
  

}
