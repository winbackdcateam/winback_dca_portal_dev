import { Component, ViewChild, HostListener, OnInit,ElementRef, AfterViewInit, ChangeDetectorRef } from '@angular/core';
import { logRecord, awsCognitoUtil, EncrDecrService } from '../_models';
import { saveAs } from 'file-saver'
import { DatePipe } from "@angular/common";
import { MdbTablePaginationComponent, MdbTableService, MdbTableDirective } from 'angular-bootstrap-md';
import * as elasticsearch from 'elasticsearch-browser'
import { logawses } from '../_models/logawses';
import { HeaderComponent } from '../header/header.component';
import { XlsService } from '../xls.service';


@Component({
  selector: 'app-auditlogs',
  templateUrl: './auditlogs.component.html',
  styleUrls: ['./auditlogs.component.scss']
})
export class AuditlogsComponent implements OnInit, AfterViewInit {

  auditLogRecord: AuditLogRecord[];
  public exportAuditLogRecord: Array<logRecord> = [];
  headElements = ['User Id', 'Full Name', 'User Role', 'Agency Code', 'Event Type', 'Log Time', 'Event Desc', 'User IP Address']
  searchdate: any;
  searchAuditText: any;
  searchAuditLogsText: any;
  astroagnecycode : Array<string>=[];
  msgStr: string;
  dcaloginCount :number;
  public filteredLogRecord: Array<logRecord> = [];
  public filteredDCARecord: Array<logRecord> = [];
  py:any;
  pya:any;
  

  @ViewChild('alert') alert: ElementRef;
  @ViewChild(HeaderComponent) objHeaderComp: HeaderComponent;
  sessionKey: string = sessionStorage.getItem("sessionkey");
  
  

  constructor(private _AwsCongnitoUtil: awsCognitoUtil, private EncrDecr: EncrDecrService,private el: ElementRef,
    private excelService: XlsService, private logawsesobj: logawses, private cdr: ChangeDetectorRef) { }

  ngOnInit() { }

  ngAfterViewInit() {
    this.objHeaderComp.flg = false;

    let curr_dt = new Date();
    let publishdate = curr_dt.toISOString().substring(0, 10);
    this.searchAuditLogsAWSES(publishdate);
    this.cdr.detectChanges();
  }

  searchAuditLogsAWSES(txtdate: string) {

    this.closeAlert();

    this.auditLogRecord = [];
    
    let eventdt = txtdate;
    
    this.searchdate = txtdate;
    
   

    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

    var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));

    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

    var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));

    var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
    

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", "Audit Log Search", decryptedFullName, decryptedUserType, decryptedUserId + " searched logs of " + txtdate, new Date().toISOString(),decryptedAgencyName, decryptedAgencyCode);
    console.log("Audit log saved");


    var elasticSearchClient = new elasticsearch.Client({
      host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com',
      size: 100,
      /*   log:'trace', */
      /*  connectionClass: HttpAmazonESConnector, */
      amazonES: {
        
        region: this._AwsCongnitoUtil._REGION
      },
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    
    elasticSearchClient.search({
      index: 'audit-logs-dca',
      size: 100,
      // type: 'doc',
      filterPath: ['hits.hits._source', 'hits.total'],
      body: {
        query: {
          "match": {
            "published": txtdate
          }
          
        },
        "sort": [
          { "eventtime":   { "order": "desc" }},
          { "_score": { "order": "desc" }}
         ]
      }

    }).then(response => {

      try {
        // console.log(response["hits"]);
        this.exportAuditLogRecord = [];

        if (response["hits"].total > 0) {

          this.auditLogRecord = response.hits.hits;

          this.auditLogRecord.forEach(obj => {

            let LogRecord = new logRecord();
            
            LogRecord.loggeduser=  obj["_source"].loggeduser
            LogRecord.username = obj["_source"].username
            LogRecord.roletype = obj["_source"].roletype
            LogRecord.agnecyCode = obj["_source"].agnecycode
            LogRecord.eventname = obj["_source"].eventname
            LogRecord.eventtime = obj["_source"].eventtime
            LogRecord.desc = obj["_source"].desc
            LogRecord.clientip = obj["_source"].clientip
            this.exportAuditLogRecord.push(LogRecord);
           })

        

         this.getCountOfItems(this.exportAuditLogRecord);

        } else {

          this.showmsg("No Record found..");
        }
      } catch (e) { console.log(e); }

    }, error => {
      this.showmsg("No Record found , Please make selection of valid date");
      console.error(error);
    }).then(() => {
      console.log('Audit Log searched!');
    });


  

    /*   elasticSearchClient.search({
        index: 'audit-logs-dca',
        // type: 'doc',
        filterPath: ['hits.hits._source', 'hits.total'],
        body: {
          query: {
            "match_all": {}
          },
        }
  
      }).then(response => {
  
        try {
          console.log(response["hits"]);
          if (response["hits"].total > 0) {
  
            this.auditLogRecord = response.hits.hits;
  
            console.log(this.auditLogRecord)
  
          }
        } catch (e) { console.log(e); }
  
      }, error => {
        console.error(error);
      }).then(() => {
        console.log('Audit Log searched!');
      });
  
   */




  }

  @HostListener('input') oninput() {
    
    this.searchAuditText = this.el.nativeElement.querySelector('#txtSearchAudit').value;

    this.searchAuditLogsText = this.el.nativeElement.querySelector('#txtSearchAuditLogs').value; 

  }

  showmsg(msg: any) {
    this.alert.nativeElement.classList.add('show');
    this.msgStr = " <strong><b>" + msg + "&nbsp;</b></strong>  ";
    this.alert.nativeElement.style.display = 'block';


    this.alert.nativeElement.style.display = 'block';
    this.alert.nativeElement.classList.add('show');

  }

  closeAlert() {


    
    this.alert.nativeElement.classList.remove('show');
    this.alert.nativeElement.style.display = 'none';
    //this.alert.nativeElement.style.display='none';

  }

  async exportAsXLSX(searcheddate: any) {
    
    this.closeAlert();
    await this.exportAuditLogsAWSES(searcheddate);

  }




  exportAuditLogsAWSES(txtdate: string) {

    this.auditLogRecord = [];

    this.exportAuditLogRecord = [];
    
    let eventdt = txtdate;
    
    this.searchdate = txtdate;    

    var decryptedUserId = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFUSRID', localStorage.getItem('userid'));

    var decryptedFullName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFFNAME', localStorage.getItem('fullname'));

    var decryptedUserType = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFTYP', localStorage.getItem("type"));

    var decryptedAgencyName = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFAGENCYNAME', localStorage.getItem("agencyname"));
    
    var decryptedAgencyCode = this.EncrDecr.get(this.sessionKey + '$#@$^@1ERFACODE', localStorage.getItem("agencycode"));
      

    console.log("Audit log to be saved");
    this.logawsesobj.logIntoAWSES(decryptedUserId, "DCA", "Audit Log Search", decryptedFullName, decryptedUserType, decryptedUserId + " searched logs of " + txtdate , new Date().toISOString(),decryptedAgencyName,decryptedAgencyCode);
    console.log("Audit log saved");


    var elasticSearchClient = new elasticsearch.Client({
      host: 'https://search-odsdata-xvm6bwlsl63gcxvfbfxrtr7wz4.ap-southeast-1.es.amazonaws.com',
      size: 100,
      /*   log:'trace', */
      /*  connectionClass: HttpAmazonESConnector, */
      amazonES: {
       
        region: this._AwsCongnitoUtil._REGION
      },
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    });

    
 return   elasticSearchClient.search({
      index: 'audit-logs-dca',
      size:100,
      // type: 'doc',
      filterPath: ['hits.hits._source', 'hits.total'],
      body: {
        query: {
          "match": {
            "published":  txtdate
          }
          
        },
      }

    }).then(response => {

      try {
      
        if (response["hits"].total > 0) {

          this.auditLogRecord = response.hits.hits;

          this.auditLogRecord.forEach(obj => {

            let LogRecord = new logRecord();
            
            LogRecord.loggeduser=  obj["_source"].loggeduser
            LogRecord.username = obj["_source"].username
            LogRecord.roletype = obj["_source"].roletype
            LogRecord.agnecyCode = obj["_source"].agnecycode
            LogRecord.eventname = obj["_source"].eventname
            LogRecord.eventtime = obj["_source"].eventtime
            LogRecord.desc = obj["_source"].desc
            LogRecord.clientip = obj["_source"].clientip
            this.exportAuditLogRecord.push(LogRecord);
           })

         this.excelService.exportAsExcelFile(this.exportAuditLogRecord, 'dcaauditlogs');

         

        }else{

          this.showmsg("No Record found..");
        }
      } catch (e) { console.log(e); }

    }, error => {
      this.showmsg("No Record found , Please make selection of valid date");
      console.error(error);
    }).then(() => {
      console.log('Audit Log searched!');
    });



  }

  getCountOfItems(_exportAuditLogRecord :any)
  {
    //exportAuditLogRecord.map(x => x.roletype );

    this.filteredLogRecord = _exportAuditLogRecord.filter((item) => item.roletype != "DCA" && item.eventname =='Login');
    this.dcaloginCount = this.filteredLogRecord.length;

    //let count = this.filteredLogRecord.filter(e => e.name === "foo").length;
    
    this.astroagnecycode = this.filteredLogRecord.map(item => item.agnecyCode)
    .filter((value, index, self) => self.indexOf(value) === index)
 
    
    

  }

  getDCAWiseItems(_dcacode :any)
  {
    this.filteredDCARecord = [];
    this.filteredDCARecord =  this.exportAuditLogRecord.filter((item) =>  item.agnecyCode == _dcacode);
  }

}

export interface AuditLogRecord {
  source: logRecord;
}
