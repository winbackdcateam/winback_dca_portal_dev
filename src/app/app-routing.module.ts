import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AuthComponent } from './auth/auth.component';
import { SecureComponent } from './secure/secure.component';
import { LoginComponent } from './login/login.component';
import { UserManagementComponent } from './auth/user-management/user-management.component';
import { LoadMappingFileComponent } from './load-mapping-file/load-mapping-file.component';
import { ErrorPageComponent } from './error-page/error-page.component';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { AuditlogsComponent } from './auditlogs/auditlogs.component';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent
},
{ path: '', component: AuthComponent, pathMatch: 'full' },
{ path: 'auth', component: AuthComponent, pathMatch: 'full' },
{ path: 'checkMapping', component: UploadfileComponent , pathMatch: 'full' },
{ path: 'error', component: ErrorPageComponent, pathMatch: 'full' },
{ path: 'userManagement', component: UserManagementComponent, pathMatch: 'full' },
{ path: 'auditlogs', component: AuditlogsComponent, pathMatch: 'full' },
{ path: 'campaign', component: LoadMappingFileComponent, pathMatch: 'full' },  

];

@NgModule({
  //imports: [RouterModule.forRoot(routes)],
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
