import { Injectable } from '@angular/core';
import { HttpClient,HttpEvent, HttpRequest, HttpHandler, HttpInterceptor, HttpResponse  } from '@angular/common/http';
import { stringType } from 'aws-sdk/clients/iam';
import * as CryptoJS from 'crypto-js';
import * as _promise from 'bluebird';
import { Observable } from 'rxjs';
import { forkJoin } from 'rxjs';  // RxJS 6 syntax
import { environment } from 'src/environments/environment';
import { tap, shareReplay } from 'rxjs/operators';
import { awsCognitoUtil } from './_models';
import AWS from 'aws-sdk';

@Injectable({
  providedIn: 'root'
})
export class DataService  implements HttpInterceptor  {

  private cache = new Map<string, any>();

  _Partnerkey = this._AwsCognitoUtil.partnerkey;
  skey:string
   
   PostUrl = "api/applog";

  apiUrl = 'http://172.18.108.52:8080/wbapi/wb/FETCHWBAPIINFO/';

  acIntrxnurl = "http://172.18.108.52:8080/wbapi/wb/fetchintrxninfo/";

  acbillinfosurl = "http://172.18.108.52:8080/wbapi/wb/fetchbillinfo/";

  acbilldetailurl = "http://172.18.108.52:8080/wbapi/wb/fetchbilldetails/";

  acntdisconturl = " http://172.18.108.52:8080/wbapi/wb/FETCHDISCONNECTINFO/";

  acntfreepreviewurl = "http://172.18.108.52:8080/wbapi/wb/FETCHFPINFO/";

  acntflashdetailsurl = "http://172.18.108.52:8080/wbapi/wb/fetchflashdetails/";



  constructor(private _http: HttpClient, private _AwsCognitoUtil: awsCognitoUtil) { 


  }

 
//------------------------------------------------
  //-- Rest api call for Winback
  //------------------------------------------------
  public getWinback(XID: string) {
    return this._http.get(this.apiUrl + XID)
  }

  getTimeStamp(){
    var now = new Date();
    var currentTime = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    var random = Math.random().toString(36).substring(7);
    var month = (currentTime.getMonth() + 1).toString();
    if (Number(month) < 10) { month = '0' + month }
    var day = currentTime.getDate().toString();
    if (Number(day) < 10) { day = '0' + day }
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours().toString();
    if (Number(hours) < 10) { hours = 0 + hours }
    var minutes = currentTime.getMinutes().toString();
    if (Number(minutes) < 10) { minutes = '0' + minutes }
    var seconds = currentTime.getSeconds().toString();
    if (Number(seconds) < 10) { seconds = '0' + seconds }
    var datetime = year + "" + month + "" + day + "" + hours + "" + minutes + "" + seconds + "";
    var utcdatetime = year + "-" + month + "-" + day + "T" + hours + ":" + minutes + ":" + seconds + "Z";
   

return utcdatetime;
  }
   public generateSignature():string {
    // logic for signature 

    

    var now = new Date();
    var currentTime = new Date(now.getTime() + now.getTimezoneOffset() * 60000);
    var random = Math.random().toString(36).substring(7);
    var month = (currentTime.getMonth() + 1).toString();
    if (Number(month) < 10) { month = '0' + month }
    var day = currentTime.getDate().toString();
    if (Number(day) < 10) { day = '0' + day }
    var year = currentTime.getFullYear();
    var hours = currentTime.getHours().toString();
    if (Number(hours) < 10) { hours = 0 + hours }
    var minutes = currentTime.getMinutes().toString();
    if (Number(minutes) < 10) { minutes = '0' + minutes }
    var seconds = currentTime.getSeconds().toString();
    if (Number(seconds) < 10) { seconds = '0' + seconds }
    var datetime = year + "" + month + "" + day + "" + hours + "" + minutes + "" + seconds + "";

    var partnerkey = this._Partnerkey;
    var partnerpassword = this.skey;
     var getconcat= this.getTimeStamp() + partnerkey + partnerpassword;
     var gethash = CryptoJS.SHA256(getconcat);
     var getSig = CryptoJS.enc.Base64.stringify(gethash);

    var APISignature = getSig;

  
    return APISignature;
   
  }

  public requestCustomerData(accountnumber: string,partnerkey: string,secretkey:string): Observable<any[]> {

     this._Partnerkey = partnerkey ;
     this.skey = secretkey;
    
    var sign =this.generateSignature();

  

    let responseFetchCustInfo = this._http.post('https://api-ods.astro.com.my/api/FETCHCUSTINFO', {
      siginfo: {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      body: {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });

     // console.log(responseFetchCustInfo)

    let responseFetchAccountinfo = this._http.post('https://api-ods.astro.com.my/api/FETCHACCOUNTINFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });
    let responseFetchAddress = this._http.post('https://api-ods.astro.com.my/api/FETCHADDRESSINFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });

    let responseFetchCollection = this._http.post('https://api-ods.astro.com.my/api/FETCHCOLLECTIONINFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });



    let responseFetchSMCInfo = this._http.post('https://api-ods.astro.com.my/api/FETCHSMCINFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });

    let responseFetchPaymentInfo = this._http.post('https://api-ods.astro.com.my/api/FETCHPAYMENTINFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });

    let responseFetchFinanceInfo = this._http.post('https://api-ods.astro.com.my/api/FETCHFININFO', {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      "body": {
        "partnerkey": partnerkey,
        "accountid": accountnumber
      }
    });

    // Observable.forkJoin (RxJS 5) changes to just forkJoin() in RxJS 6
    return forkJoin([responseFetchCustInfo, responseFetchAccountinfo, responseFetchAddress,
      responseFetchCollection, responseFetchFinanceInfo, responseFetchPaymentInfo, responseFetchSMCInfo]);
  }



//------------------------------------------------
  //-- Rest api call for Interaction
  //------------------------------------------------
  public requestInteractionDetails(_AccountNumber: string) :Observable<any[]> {
  
    return  forkJoin(this._http.post("https://api-ods.astro.com.my/api/fetchintrxninfo" , {"siginfo":{
      "sig":this.generateSignature(),
      "timestamp":this.getTimeStamp()
      },
      "body":{
      "partnerkey":this._Partnerkey,
      "accountid":_AccountNumber
      }
      }));
  }

  //------------------------------------------------
  //-- Rest api call for Bill Information
  //------------------------------------------------
  public requestBillInformation(_AccountNumber: string):Observable<any[]> {
    return forkJoin(this._http.post('https://api-ods.astro.com.my/api/fetchbillinfo' , {"siginfo":{
      "sig":this.generateSignature(),
      "timestamp":this.getTimeStamp()
      },
      "body":{
      "partnerkey":this._Partnerkey,
      "accountid":_AccountNumber
      }
      }));
  }

  //-------------------------------------------------
  //-- Rest api call for Bill Details
  //-------------------------------------------------
  public requestBillDetails(_AccountNumber: string, seq: string):Observable<any[]> {
    
    return forkJoin(this._http.post("https://api-ods.astro.com.my/api/fetchbilldetails" , {
      "siginfo": {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()      
      },
      "body": {
        "partnerkey":this._Partnerkey,
                "accountid":_AccountNumber,
                "seq":seq
      }}
    ));
  }


  //-------------------------------------------------
  //-- Rest api call for Disconnection Details
  //-------------------------------------------------
  public requestDisconnectionDetails(_AccountNumber: string):Observable<any[]> {
    return forkJoin( this._http.post('https://api-ods.astro.com.my/api/FETCHDISCONNECTINFO' , {
      siginfo: {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      body: {
        "partnerkey":this._Partnerkey,
      "accountid":_AccountNumber
      }
    }));
  }


  //-------------------------------------------------
  //-- Rest api call for free preview Details
  //-------------------------------------------------
  public requestFreePreviewDetails(_AccountNumber: string):Observable<any[]> {
    return forkJoin(this._http.post('https://api-ods.astro.com.my/api/FETCHFPINFO' , {
      siginfo: {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      body: {
        "partnerkey":this._Partnerkey,
      "accountid":_AccountNumber
      }
    }));
  }


  //-------------------------------------------------
  //-- Rest api call for flash Details
  //-------------------------------------------------
  public requestFlashDetails(_AccountNumber: string) {
    return forkJoin( this._http.post('https://api-ods.astro.com.my/api/fetchflashdetails' , {
      siginfo: {
        "sig": this.generateSignature(),
        "timestamp": this.getTimeStamp()
      },
      body: {
        "partnerkey":this._Partnerkey,
      "accountid":_AccountNumber
      }
    }));
  }




  

  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    /* if (request.method !== 'GET') {
      return next.handle(request);
    } */

    const cachedResponse = this.cache.get(request.url);
    if (cachedResponse) {
      return (cachedResponse);
    }

    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          this.cache.set(request.url, event);
        }
      })
    );
  }


    //---------------------------------------------------
  //-- Read csv file
  //--------------------------------------------------
  public readCSVfile(csvpath: string) {
    //'assets/file.csv'
    return this._http.get(csvpath, { responseType: 'text' })

  }

  
}
