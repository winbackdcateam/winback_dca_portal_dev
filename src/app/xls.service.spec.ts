import { TestBed } from '@angular/core/testing';

import { XlsService } from './xls.service';

describe('XlsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: XlsService = TestBed.get(XlsService);
    expect(service).toBeTruthy();
  });
});
