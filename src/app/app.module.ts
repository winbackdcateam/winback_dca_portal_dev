import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule ,NO_ERRORS_SCHEMA} from '@angular/core';
import { NgxCaptchaModule } from 'ngx-captcha';
import { RecaptchaModule } from 'ng-recaptcha';
import { PapaParseModule } from 'ngx-papaparse';
import {MDBBootstrapModule,MdbTablePaginationComponent, MdbTableService} from 'angular-bootstrap-md';
import {HttpClientModule,HttpInterceptor, HttpClientXsrfModule } from '@angular/common/http';
import { UserIdleModule } from 'angular-user-idle';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgxPopper } from 'angular-popper';
import { ClipboardModule } from 'ngx-clipboard';
import { CookieService } from 'ngx-cookie-service';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { DataService } from './data.service';
import { CustomerInformation,IntrxnInformation, UploadFileService, awsCognitoUtil, validate, globals,logRecord, EncrDecrService } from './_models';
import {AtomSpinnerModule} from 'angular-epic-spinners';
import { FooterComponent } from './footer/footer.component';
import { AmplifyAngularModule, AmplifyService, AmplifyIonicModule } from 'aws-amplify-angular';
import { AuthComponent } from './auth/auth.component';
import { SecureComponent } from './secure/secure.component';
import { UserManagementComponent } from './auth/user-management/user-management.component';
import { DisallowcopyDirective } from './disallowcopy.directive';
import { CognitoUserSession } from 'amazon-cognito-identity-js';
import { DatePipe,HashLocationStrategy, LocationStrategy  } from '@angular/common';
import { UploadfileComponent } from './uploadfile/uploadfile.component';
import { RouterModule,Routes } from '@angular/router';
import { AngularFileUploaderModule } from "angular-file-uploader";
import { AuthGuard } from './auth.guard';
import { LoadMappingFileComponent } from './load-mapping-file/load-mapping-file.component';
import { ApplicationStateServiceService } from './application-state-service.service';
import { ErrorPageComponent } from './error-page/error-page.component';
import { NgxPaginationModule } from 'ngx-pagination';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { DeviceDetectorModule  } from 'ngx-device-detector';
import { FlatpickrModule } from 'angularx-flatpickr';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FilterPipeModule } from 'ngx-filter-pipe';
import { AuditlogsComponent } from './auditlogs/auditlogs.component';
import {NgxFsModule} from 'ngx-fs';
import { ConfigService } from 'aws-sdk';
import { logawses } from './_models/logawses';
import {AuthorizationCheckService} from './authorization-check.service';
import { SafePipe } from './safe.pipe'




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    HomeComponent,
    LoginComponent,
    FooterComponent,
    AuthComponent,
    SecureComponent,
    UserManagementComponent,
    DisallowcopyDirective,
    UploadfileComponent,
    LoadMappingFileComponent,
    ErrorPageComponent,
    AuditlogsComponent,
    SafePipe
  ],
  imports: [ 
    HttpClientModule,AtomSpinnerModule,NgxCaptchaModule,RecaptchaModule,PapaParseModule, AngularFileUploaderModule,
    BrowserModule,BrowserAnimationsModule,  UserIdleModule.forRoot({idle:100, timeout: 100, ping: 120})
    , AppRoutingModule,NgxFsModule, MDBBootstrapModule.forRoot(), AmplifyAngularModule, AmplifyIonicModule, NgxPaginationModule,
    DeviceDetectorModule.forRoot(),FlatpickrModule.forRoot(), ClipboardModule,Ng2SearchPipeModule,FilterPipeModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    RouterModule.forRoot([

      {
        path: 'home',
        component: HomeComponent
      },
      { path: '', component: AuthComponent, pathMatch: 'full' , canActivate: [AuthorizationCheckService] },
      { path: 'auth', component: AuthComponent, pathMatch: 'full', canActivate: [AuthorizationCheckService]  },
      { path: 'checkMapping', component: UploadfileComponent , pathMatch: 'full' , canActivate: [AuthorizationCheckService] },
      { path: 'error', component: ErrorPageComponent, pathMatch: 'full', canActivate: [AuthorizationCheckService]  },
        { path: 'userManagement', component: UserManagementComponent, pathMatch: 'full' , canActivate: [AuthorizationCheckService] },
        { path: 'auditlogs', component: AuditlogsComponent, pathMatch: 'full' , canActivate: [AuthorizationCheckService] }
     ]),
     HttpClientModule,
      HttpClientXsrfModule.withOptions({
      cookieName: 'My-Xsrf-Cookie',
      headerName: 'My-Xsrf-Header',
      })
  ],
  schemas: [ NO_ERRORS_SCHEMA ],
  providers: [
    {provide : LocationStrategy , useClass: HashLocationStrategy},
    AuthGuard, DataService, CustomerInformation, IntrxnInformation, ApplicationStateServiceService, ConfigService, logRecord,
   EncrDecrService,logawses,  AmplifyService,AppComponent,DatePipe,UploadFileService,awsCognitoUtil,CookieService,validate,globals],
  bootstrap: [AppComponent]
})
export class AppModule {  }
