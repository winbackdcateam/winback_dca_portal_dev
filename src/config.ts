import * as loadedConfig from "!val-loader!../config-loader";
import * as CryptoJS from 'crypto-js';

export interface IConfig {
  env: "production" | "development" | "test" | "local" | "stage";
  accesskey: any;  
  secrectkey: any;
  poolid: any;
  webclientid: any;
  userpoolsid: any;
  clientip: any;
  CAPTCHA_SITE_KEY: any;
  apipartnerpassword: any;
  partnerkey: any;
}

class GetkeysClass {
  constructor() { }
  
  decryptData(data) {

    try {
      const bytes = CryptoJS.AES.decrypt(data, "encryptdata");
      if (bytes.toString()) {
        let decrptdata = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
        
        return decrptdata;
      }
      return data;
    } catch (e) {
      console.log(e);
    }
  }
}
export const config = new GetkeysClass().decryptData(loadedConfig) as IConfig;