const webpack = require('webpack');
const dtenv = require('dotenv').config();

module.exports = {
   plugins: [
        new webpack.DefinePlugin({
            $ENV: {
                dcaaccesskey : JSON.stringify(process.env.dcaaccesskey),
                dcasecretkey : JSON.stringify(process.env.dcasecretkey),
                dcapoolid: JSON.stringify(process.env.dcapoolid),
                region: JSON.stringify(dtenv)
            }
        })
]
}