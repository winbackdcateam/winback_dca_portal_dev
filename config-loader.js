const convict = require("convict");
const dotenv = require("dotenv");
var dotenvExpand = require('dotenv-expand');
const CryptoJS = require('crypto-js');
const os = require('os');

var myEnv = dotenv.config()
dotenvExpand(myEnv)

dotenv.config();

 
var interfaces = os.networkInterfaces();
var addresses = [];
for (var k in interfaces) {
    for (var k2 in interfaces[k]) {
        var address = interfaces[k][k2];
        if (address.family === 'IPv4' && !address.internal) {
            addresses.push(address.address);
        }
    }
}


const config = convict({
  env: {
    doc: "DCA application environment.",
    format: ["production", "development", "test", "local", "stage"],
    default: "local",
    env: "NODE_ENV"   
  },
  accesskey: process.env.dcaaccesskey,  
  secrectkey :   process.env.dcasecretkey,
  poolid: process.env.dcapoolid,
  webclientid: process.env.dcawebclientid,
  userpoolsid: process.env.dcauserpoolsid,
  clientip: JSON.stringify(addresses),
  CAPTCHA_SITE_KEY:process.env.CAPTCHA_SITE_KEY,
  apipartnerpassword: process.env.apipartnerpassword,
  partnerkey : process.env.partnerkey
});

config.validate({ allowed: "strict" });

function encryptData(data) {

  try {
    
    return CryptoJS.AES.encrypt(JSON.stringify(data), "encryptdata").toString();
  } catch (e) {
    console.log(e);
  }
}



module.exports = () => {
  //return { code: config.getProperties() }
  return { code: "module.exports = " + JSON.stringify(encryptData(config.getProperties())) };
};