'use strict';
module.exports = function (app) {
    //Controllers
    let apiController = require('../../controllers/applog/applog.controller.server'); 
    app.route("/api/applog").post(apiController.create); 
};