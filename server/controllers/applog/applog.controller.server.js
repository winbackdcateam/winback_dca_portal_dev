'use strict';

// Service
let apiService = require('../../services/applog/applog.service.server'); 
exports.create = function (request, response) {
    let objData = request.body;
    apiService.create(objData);
    response.status(200).json(objData);
};
 