let express = require('express'),
    path = require('path'),
    app = express(),
    port = process.env.PORT || 3000, 
    cors = require('cors');
    bodyParser = require('body-parser'),
    cookieParser = require('cookie-parser'),
        session = require('express-session'),
        fs = require("fs"),
        stream = require("stream"),
        abortStream=false,  // When user click on delete, update this flag to true
        util = require("util"),    
        awskeys = require("./config.js"),
    passport = require('passport');
    var morgan = require('morgan');
    require('dotenv').config({path: '.env.dev'})
    console.log('environment code '+ process.env.ACCESS_HEADER)
 
 
//Adding cookie parser and session
app.use(cookieParser());
app.use(session({
    secret: process.env.SESSION_SECRET || 'this is the secret',
    resave: true,
    saveUninitialized: true
}));

//Passport.js
app.use(passport.initialize());
app.use(passport.session());

//Adding body parser for handling request and response objects.
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Point static path to dist -- For building -- REMOVE
app.use(express.static(path.join(__dirname, 'dist/winback-ECAP-proj')));
app.use(morgan('dev'));
//app.use(express.static('../accounting-client/dist'));
//Enabling CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", process.env.FRONT_END_URL || "http://localhost:3000");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
    res.header("Access-Control-Allow-Credentials", "true");
    next();
});

//Loading server side file
require('./server/app')(app);

// For Build: Catch all other routes and return the index file -- BUILDING
app.get('*', function (req, res) {
    res.sendFile(path.join(__dirname, 'dist/winback-ECAP-proj/index.html'));
});

app.listen(port);

console.log(' application running on: ' + port);
console.log(' awskeys application : ' + awskeys);

