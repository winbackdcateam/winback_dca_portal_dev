### STAGE 1: Build ###
FROM node:12.13.0 AS build

# SET ENVIRONMENT VARIABLES

ENV CAPTCHA_SITE_KEY=6Le9zKUUAAAAALDazbJmaOG9SuG6o31qPb2kEe_d 
ENV partnerkey=dca
ENV apipartnerpassword=dca@k19!

WORKDIR /app
COPY package.json ./
RUN npm install -g @angular/cli@8.3.15 -g npm-install-peers
RUN npm update 
COPY . .




RUN npm rebuild node-sass --force
RUN npm run ng-high-memory --prod

#CMD npm run build-serve --host 0.0.0.0

#### STAGE 2
#### Deploying the application

FROM nginx:alpine

VOLUME  /var/cache/nginx

# Copy the build files from the project
# replace "angular-docker-environment-variables" with your angular project name
COPY --from=build /app/dist/winback-ECAP-proj /usr/share/nginx/html

# Copy Nginx Files
COPY --from=build /app/nginx/nginx.conf /etc/nginx/conf.d/default.conf

# EXPOSE Port 80
EXPOSE 80


ENV CAPTCHA_SITE_KEY=6Le9zKUUAAAAALDazbJmaOG9SuG6o31qPb2kEe_d 
ENV partnerkey=dca
ENV apipartnerpassword=dca@k19!